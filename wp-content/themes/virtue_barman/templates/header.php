<?php
if (is_single()) {
    $categorias = get_the_category();
    foreach ($categorias as $categoria) {
        $id_categoria = $categoria->cat_ID;
        $nombre_cat = $categoria->cat_name;
    }
}
?>
<header class="banner headerclass <?php
               if (is_single()) {
                   if (isset($id_categoria))
                       echo 'cat-' . $id_categoria;
               }
               ?>"
        role="banner">
    <?php
    global $virtue;
    if (isset($virtue['logo_layout'])) {
        if ($virtue['logo_layout'] == 'logocenter') {
            $logocclass = 'col-md-12';
            $menulclass = 'col-md-12';
        } else if ($virtue['logo_layout'] == 'logohalf') {
            $logocclass = 'col-md-6';
            $menulclass = 'col-md-6';
        } else {
            $logocclass = 'col-md-4';
            $menulclass = 'col-md-9';
        }
    } else {
        $logocclass = 'col-md-4';
        $menulclass = 'col-md-9';
    }
    ?>
    <div class="container">
        <div class="row">
            <div class="<?php echo $logocclass; ?>  clearfix kad-header-left">
                <div id="logo" class="logocase">
                    <a class="brand logofont" href="<?php echo home_url(); ?>/">
                        <?php
                        global $virtue;
                        if (!empty($virtue['x1_virtue_logo_upload']['url'])) {
                        ?>
                        <div id="thelogo">
                            <img src="<?php echo $virtue['x1_virtue_logo_upload']['url']; ?>" alt="<?php bloginfo('name'); ?>" class="kad-standard-logo" />
                            <?php if (!empty($virtue['x2_virtue_logo_upload']['url'])) { ?>
                            <img src="<?php echo $virtue['x2_virtue_logo_upload']['url']; ?>" class="kad-retina-logo" style="max-height:<?php echo $virtue['x1_virtue_logo_upload']['height']; ?>px" /> <?php } ?>
                        </div> <?php
                        } else {
                            bloginfo('name');
                        }
                        ?>
                    </a>
                    <?php if (isset($virtue['logo_below_text'])) { ?> <p class="kad_tagline belowlogo-text"><?php echo $virtue['logo_below_text']; ?></p> <?php } ?>
                </div> <!-- Close #logo -->
            </div><!-- close logo span -->

            <div class="col-md-9 col-md-offset-3 kad-header-right">

                <?php if (is_mobile()) : ?>
                <nav id="header-top-mobile" class="ui text menu">
                    <div class="item" data-form="searchform">
                        <i class="icon search"></i>
                    </div>
                    <div class="item" data-form="seleccion-ciudades">
                        <i class="marker icon"></i>
                    </div>
                </nav>
                    <?php if (is_user_logged_in()):global $current_user;//get_currentuserinfo();?>
                        <div class="log-user-name" style="color:white;">Bienvenido, <?php echo $current_user->user_firstname ?></div>
                    <?php endif; ?>
                <?php endif; ?>

                <nav id="nav-main" class="clearfix" role="navigation">


                    <?php if (is_mobile()) : ?>
                    <div id="cerrar-nav-main">
                        <i class="remove icon"></i>
                    </div>
                    <?php endif; ?>

                    <?php echo do_shortcode("[mocion-ciudades-dropdown reload='true']"); ?>
                    <?php get_search_form(); ?>

                    <?php if (is_user_logged_in() && !is_mobile()):global $current_user;//get_currentuserinfo();?>
                    <div class="log-user-name">Bienvenido, <?php echo $current_user->user_firstname ?></div>
                    <?php endif; ?>

                    <?php if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'sf-menu'));
                    endif;?>

                </nav>
                <?php if (has_nav_menu('secondary_navigation')) : ?>

                <div class="wrap-menu-barman">
                    <nav id="nav-second" class="clearfix" role="navigation">
                        <?php wp_nav_menu(array('theme_location' => 'secondary_navigation', 'menu_class' => 'sf-menu')); ?>
                    </nav>
                </div><!--close container-->

                <?php endif; ?>
            </div> <!-- Close span7 -->
        </div> <!-- Close Row -->
        <?php if (has_nav_menu('mobile_navigation')) : ?>
        <div id="mobile-nav-trigger" class="nav-trigger">
            <a class="nav-trigger-case mobileclass collapsed" rel="nofollow" data-toggle="collapse" data-target=".kad-nav-collapse">
                <div class="kad-navbtn"><i class="icon-reorder"></i></div>
                <div class="kad-menu-name"><?php echo __('Menu', 'virtue'); ?></div>
            </a>
        </div>
        <div id="kad-mobile-nav" class="kad-mobile-nav">
            <div class="kad-nav-inner mobileclass">
                <div class="kad-nav-collapse">
                    <?php wp_nav_menu(array('theme_location' => 'mobile_navigation', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_class' => 'kad-mnav')); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if (kadence_display_topbar()) : ?>
        <section id="topbar" class="topclass">
            <div class="container">
                <div class="row">
                    <div class="kad-topbar-right">
                        <div id="topbar-search" class="topbar-widget">
                            <?php
                            if (kadence_display_topbar_widget()) {
                                if (is_active_sidebar('topbarright')) {
                                    dynamic_sidebar(__('Topbar Widget', 'virtue'));
                                }
                            } else {
                                if (kadence_display_top_search()) {
                                    get_search_form();
                                }
                            }
                            ?>
                        </div>
                    </div> <!-- close col-md-6-->
                </div> <!-- Close Row -->
            </div> <!-- Close Container -->
        </section>

        <?php endif; ?>
    </div> <!-- Close Container -->

    <?php
    global $virtue;
    if (!empty($virtue['virtue_banner_upload']['url'])) {
    ?> <div class="container"><div class="virtue_banner"><img src="<?php echo $virtue['virtue_banner_upload']['url']; ?>" /></div></div> <?php } ?>
</header>
<nav class="topbardos container">
    <div class="contenedor-info">

        <div class="popup-info compra-segura">
            <span>COMPRA SEGURA</span>
            <div class="despliegue-info">
                El pago es contra entrega, ya sea en tarjeta de crédito, débito o efectivo, lo que garantiza la seguridad en la transacción. Para realizar el pedido en la página no es necesario entregar la información de la tarjeta.
            </div>
        </div>

        <div class="popup-info productos-exclusivos">
            <span>PRODUCTOS EXCLUSIVOS</span>
            <div class="despliegue-info">
                En esta tienda siempre encontrará productos exclusivos y de las mejores marcas de licores, y  muchas veces productos y promociones que no encontrará en ninguna otra tienda del país.
            </div>
        </div>

    </div>
</nav>
