<?php

add_action('init', 'create_promocion_cupon_taxonomy');

function create_promocion_cupon_taxonomy() {
    $labels = array(
        'name' => 'Promociones',
        'singular_name' => 'Promocion',
        'search_items' => 'Buscar Promociones',
        'all_items' => 'Todas las Promociones',
        'edit_item' => 'Editar Promocion',
        'update_item' => 'Actualizar Promocion',
        'add_new_item' => 'Agregar nueva Promocion',
        'new_item_name' => 'Nuevo Promocion Nombre',
        'menu_name' => 'Promocion',
        'view_item' => 'Ver Promocion',
        'popular_items' => 'Popular Promocion',
        'separate_items_with_commas' => 'Separar Promociones con comas',
        'add_or_remove_items' => 'Agregar o eliminar Promociones',
        'choose_from_most_used' => 'Choose from the most used Promociones',
        'not_found' => 'No Promociones found'
    );

    register_taxonomy(
            'Promocion', array(
        'label' => __('Promocion'),
        'hierarchical' => false,
            //'labels' => $labels
            )
    );
}

// Exclude coupons from being applied when products on sale
// validamos si ya se aplico el descuento de 3x4 si si
// no permite aplicar algun cupon
add_filter('woocommerce_coupon_is_valid', 'woocommerce_coupon_check_sale_items', 10, 2);

function woocommerce_coupon_check_sale_items($valid, $coupon) {
    global $woocommerce;

    $valid_for_cart = $valid;

    if (sizeof($woocommerce->cart->get_cart()) > 0) :
        foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) :
            if (isset($cart_item['rp_wcdpd'])) {
                $valid_for_cart = false;
            }

        endforeach;

    endif;
    if (!$valid_for_cart)
        $valid = false;

    return $valid;
}

