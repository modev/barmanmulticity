<?php

function my_connection_types() {
    p2p_register_connection_type(array(
        'name' => 'products_to_posts',
        'from' => 'product',
        'to' => 'post'
    ));
}

/*  funcion para agregar relacion post to post  */

function post_post_register() {
    p2p_register_connection_type(array(
        'name' => 'posts_to_posts',
        'from' => 'post',
        'to' => 'post',
        'reciprocal' => true,
        'title' => 'Artículos Relacionados  '
    ));
}

add_action('p2p_init', 'my_connection_types');
add_action('p2p_init', 'post_post_register');

/* hook para alterar el formulario antes de guardarse */
add_filter('wp_insert_post_data', 'alter_related_posts', '99', 2);

function alter_related_posts($data, $postarr) {
    if ($data['post_type'] == 'post') {

$post_list = array();

//load relateds
        $connected = new WP_Query(array(
            'connected_type' => 'posts_to_posts',
            'connected_items' => $postarr["ID"],
            'nopaging' => true,
                ));





        $categories = wp_get_post_categories($postarr["ID"]);

        $destacados = count($connected);
        $numero_destacados = 2;

        if ($numero_destacados < 2) {
            
            $args = "";
            
            if(isset($categories[0])) {
                $args = array('cat' => $categories[0], 'posts_per_page' => ($numero_destacados));
            }
// The Query
            $the_query = new WP_Query($args);

// The Loop
            if ($the_query->have_posts()) {
                $contador = 0;
                while ($the_query->have_posts() && $destacados <= $numero_destacados) {
                    $the_query->the_post();

                    $contador++;
   
                    if ($postarr["ID"] != $the_query->post->ID) {

                        $destacados++;  
                        if (!in_array($the_query->post->ID, $post_list)) {
                         
                        //var_dump("IDS: " . $postarr["ID"] ." ". $the_query->post->ID);
                        (p2p_type('posts_to_posts')->connect($postarr["ID"], $the_query->post->ID, array(
                                    'date' => current_time('mysql')
                                )));

                        array_push($post_list, $the_query->post->ID);
                        }
                    }

                    if ($contador > 2) {

                        break;
                    }
                }
            }
        }
    }
    return $data;
}


