<?php

function mobile_shortcode($atts, $content = '') {
    if (wp_is_mobile() === false) {
        $content = '';
    }
    return do_shortcode($content);
}
add_shortcode('mobile', 'mobile_shortcode');

function not_mobile_shortcode($atts, $content = '') {
    if (wp_is_mobile() === true) {
        $content = '';
    }
    return do_shortcode($content);
}
add_shortcode('not_mobile', 'not_mobile_shortcode');
