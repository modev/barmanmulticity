<?php
	/*-----------------------------------------------------------------------------------*/
	/* AGREGA MENSAJE EN CARRITO */
	/*-----------------------------------------------------------------------------------*/
	/**
	 * Add a content block after all notices, such as the login and coupon notices.
	 *
	 * Reference: https://github.com/woothemes/woocommerce/blob/master/templates/checkout/form-checkout.php
	 */
	//add_action( 'woocommerce_before_checkout_form', 'skyverge_add_checkout_content', 12 );
	function skyverge_add_checkout_content() {
	    $format = "Y-m-d H:i:s";
		$fecha_inicial  = DateTime::createFromFormat($format, "2015-05-01 00:00:00");
		$fecha_final    = DateTime::createFromFormat($format, "2015-06-02 11:59:59");
		$now = new DateTime();


		$estamos_en_la_franja = ($now > $fecha_inicial && $now < $fecha_final);
		$es_administrador = current_user_can( 'manage_options' );


		if ($estamos_en_la_franja || ($es_administrador && ($now < $fecha_final))):

			wc_print_notice( __( 'Regístrate para hacer efectiva la promoción de CyberMonday. ¡Lleva la 4ta Gratis! Envío únicamente en Bogotá.', 'woocommerce' ), 'notice' );

		endif;
	}
	
	/*-----------------------------------------------------------------------------------*/
	/* hook para mostar mensajes en el archive page */
	/*-----------------------------------------------------------------------------------*/

	add_action( 'woocommerce_before_my_page', 'wc_print_notices', 10 );

	/*-----------------------------------------------------------------------------------*/
	/* WooCommerce cambia texto boton añadir al carrito */
	/*-----------------------------------------------------------------------------------*/
	add_filter( 'woocommerce_product_single_add_to_cart_text', 'wc_single_texto_add_to_cart' );    // 2.1 +
	 
	function wc_single_texto_add_to_cart() {
	 
	  return __( 'AÑADIR AL CARRITO', 'woocommerce' );
	 
	}

	/*-----------------------------------------------------------------------------------*/
	/* WooCommerce cambia texto tab description  */
	/*-----------------------------------------------------------------------------------*/
	add_filter( 'woocommerce_product_tabs', 'wc_descripcion_producto_header', 10, 1 );

	function wc_descripcion_producto_header( $tabs ) {

	  if ( isset( $tabs['description']['title'] ) )
	    $tabs['description']['title'] = 'Descripción';
	  return $tabs;

	}

	// Quitando encabezado de tab de descripcion
	add_filter( 'woocommerce_product_description_heading', 'wc_notexto_product_description_tab_heading', 10, 1 );

	function wc_notexto_product_description_tab_heading() {
	  return __( '', 'woocommerce' );
	}

	/*-----------------------------------------------------------------------------------*/
	/* Agregar campo de mililitros  */
	/*-----------------------------------------------------------------------------------*/

	//se crea el campo en la pestaña general
	add_action( 'woocommerce_product_options_pricing', 'wc_mililitro_product_field' );
	function wc_mililitro_product_field() {
    	woocommerce_wp_text_input( array( 
    		'id' => 'mililitros', 
    		'class' => 'wc_input_mililitros short', 
    		'label' => __( 'Valor ', 'woocommerce' )
    		) 
    	);
	}

	//guardamos los datos ingresados en el campo
	add_action( 'save_post', 'wc_mililitro_save_product' );
	function wc_mililitro_save_product( $product_id ) {
	    // si tiene autoguardado no ahcer nada, solo se guarda cuando hacemos click en update
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;
		if ( isset( $_POST['mililitros'] ) ) {
			if ( is_numeric( $_POST['mililitros'] ) )
				update_post_meta( $product_id, 'mililitros', $_POST['mililitros'] );
		} else delete_post_meta( $product_id, 'mililitros' );
	}

	//mostramos la informacion del campo en el front-end
	//unidad
	add_action( 'woocommerce_single_product_summary', 'wc_mililitros_show', 5 );
	function wc_mililitros_show() {
	    global $product;

		$mililitros = get_post_meta( $product->id, 'mililitros', true );
		$unidad = get_post_meta( $product->id, 'unidad', true );
		
		if ($unidad == 'mililitros' || $unidad == 'ml') {
			echo '<div class="woocommerce_mililitros_wrap">';
			_e( '', 'woocommerce' );
			echo '<span class="woocommerce-mililitros"><span>Botella </span>' . $mililitros . ' ML.</span>';
			echo '</div>';
		}else{
			echo '<div class="woocommerce_mililitros_wrap">';
			_e( '', 'woocommerce' );
			echo '<span class="woocommerce-mililitros">'. $mililitros . ' '. $unidad .'.</span>';
			echo '</div>';
		}
	}

	// Opcional: mostrar en los archives
	//add_action( 'woocommerce_after_shop_loop_item_title', 'wc_mililitros_show' );
?>