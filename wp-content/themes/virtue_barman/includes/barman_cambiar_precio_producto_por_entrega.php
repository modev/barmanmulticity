<?php
/*
//Cambiamos los precios de los productos dependiendo si es entrega inmediata o entrega normal, cada valor del producto
//es un campo separado en la administracion, el número 90 es para ejecutar esto antes de los cambios del modulo pricing and discounts
add_action('woocommerce_cart_loaded_from_session', 'barman_apply_discounts', 90);
add_action('woocommerce_ajax_added_to_cart', 'barman_apply_discounts', 90);

function barman_apply_discounts($cart_object) {

    //global $woocommerce;
    //$woocommerce->cart->cart_contents;
    //Si la entrega es inmediata o no

    if (isset($_POST['payment_method'])) {
        WC()->session->chosen_payment_method = $_POST['payment_method'];
    }
    $payment_method = WC()->session->chosen_payment_method;


    $es_entrega_imediata = ($payment_method == 'cod');

    if (isset($cart_object->cart_contents)) {
        foreach ($cart_object->cart_contents as $key => $value) {

            if ($es_entrega_imediata) {
                $precio = get_post_meta($value['data']->id, 'precio_inmediato', true);
            } else {
                $precio = get_post_meta($value['data']->id, '_regular_price', true);
            }

            $precio = intval(($precio) ? $precio : 0);

            $value['data']->set_price($precio);
        }
    }
}


add_action('woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields');

// Save Fields

function woo_add_custom_general_fields() {
    global $woocommerce, $post;
    echo '<div class="options_group">';
    woocommerce_wp_text_input(
            array(
                'id' => 'precio_inmediato',
                'label' => __('Precio entrega inmediata', 'woocommerce'),
                'placeholder' => '',
                'description' => __('Precio para entrega en menos de una hora', 'woocommerce'),
                'type' => 'number',
                'custom_attributes' => array(
                    'step' => 'any',
                    'min' => '0'
                )
            )
    );
    echo '</div>';
}


add_action('woocommerce_process_product_meta', 'woo_add_custom_general_fields_save');

function woo_add_custom_general_fields_save($post_id) {

// Number Field
    $woocommerce_number_field = $_POST['precio_inmediato'];
    if (!empty($woocommerce_number_field)) {
        update_post_meta($post_id, 'precio_inmediato', esc_attr($woocommerce_number_field));
    }
}

*/