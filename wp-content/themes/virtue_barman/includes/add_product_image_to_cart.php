<?php

add_action('woocommerce_cart_item_name', 'barman_displays_cart_products_feature_image',10,3);
function barman_displays_cart_products_feature_image($title, $cart_item, $cart_item_key) {
	
     if (!isset($cart_item['product_id']) || !isset($cart_item_key)) return $title;
     
	 $product = new WC_product($cart_item['product_id']);
	 
	return $product->get_image()."<span class='item-title'>".$title."</span>";
	
}
