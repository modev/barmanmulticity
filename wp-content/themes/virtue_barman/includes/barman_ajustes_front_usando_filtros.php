<?php

//Ocultamos el SKU no se esta usando
add_filter('wc_product_sku_enabled', '__return_false');

add_filter('woocommerce_review_order_before_submit', 'agregar_disclaimer_menoresdeedad');

add_filter( 'woocommerce_thankyou_order_received_text', 'add_message_to_thankyou_page' );



function agregar_disclaimer_menoresdeedad(){
    echo '<p class="row form-row menores">
		No vendemos a menores de edad, es necesario presentar la cédula de la persona que recibe el pedido.
	  </p>';
}


function add_message_to_thankyou_page ( $thank_you_msg ) {
	$nuevo_mensaje = "<p>Tu pedido ha sido recibido y ya está siendo procesado.</p>
	<p>Recibirás una llamada en los próximos 10 minutos para programar la hora de entrega de tu producto. 
	Si no la recibes llama al número "
	.apply_filters( 'telefono_distribuidor', '4926307')
    ." en ".apply_filters( 'ciudad_distribuidor', 'Bogotá').".</p>
	<p>Los detalles para la entrega son los siguientes:</p>
	";
	$thank_you_msg =  $nuevo_mensaje; //$thank_you_msg.'This is your new thank you message';
    return $thank_you_msg;
}


