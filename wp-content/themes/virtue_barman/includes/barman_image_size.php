<?php
add_action( 'do_meta_boxes', 'my_do_meta_boxes' );
function my_do_meta_boxes( $post_type ) {
	if($post_type == 'product'){
	global $wp_meta_boxes;
	if ( ! current_theme_supports( 'post-thumbnails', $post_type ) || ! post_type_supports( $post_type, 'thumbnail' ) )
		return;

	foreach ( $wp_meta_boxes[ $post_type ] as $context => $priorities )
	foreach ( $priorities as $priority => $meta_boxes )
	foreach ( $meta_boxes as $id => $box )
	if ( 'postimagediv' == $id )
		$wp_meta_boxes[ $post_type ][ $context ][ $priority ][ $id ]['title'] .= 'PNG, Ancho: 243, Alto: 675';
	remove_action( 'do_meta_boxes', 'my_do_meta_boxes' );
	}
}