'use strict';
module.exports = function(grunt) {
    // cargamos todas las tareas
    require('load-grunt-tasks')(grunt);

    // Configuracion del proyecto.
    grunt.initConfig({
        less: {
            dev: {
                files: {
                    "assets/css/main.css": [
                        "assets/less/master.less"
                    ]
                },
                options: {
                    compress: false,
                    //source maps
                    sourceMap: true,
                    sourceMapFilename: 'main.css.map'
                }
            },
            build: {
                files: {
                    "assets/css/main.css": [
                        "assets/less/master.less"
                    ]
                },
                options: {
                    compress: true
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
            },
            dev: {
                src: 'assets/css/main.css'
            },
            build: {
                src: 'assets/css/main.css'
            }
        },
        watch: {
            less: {
                files: [
                    'assets/less/**.less'
                ],
                tasks: ['less:dev']
            }
        }
    });

    // resgistrar las tareas
    grunt.registerTask('default', [
        'dev'
    ]);

    grunt.registerTask('dev', [
        'less:dev',
        //'autoprefixer:dev'
    ]);

    grunt.registerTask('build', [
        'less:build',
        'autoprefixer:build'
    ]);
};
