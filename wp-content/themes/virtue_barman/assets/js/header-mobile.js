(function ($, window){
    $(document).on('ready', function() {

        var NavMain = $('#nav-main');
        var ContIconos = $('#header-top-mobile');
        var BotonCerrarNavMain = $('#cerrar-nav-main');
        var forms = {};

        BotonCerrarNavMain.on('click', function (e) {
            e.preventDefault();
            NavMain.removeClass('active');
            $('#' + forms.active).removeClass('active');
            forms.active = null;
        });

        ContIconos.on('click', '.item', function () {
            var form = $(this).data('form');
            forms.active = form;

            NavMain.addClass('active');
            $('#' + form).addClass('active');
        });

    });//END: document on ready
})(jQuery, window);
