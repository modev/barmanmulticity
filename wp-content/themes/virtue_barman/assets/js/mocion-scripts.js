(function ($, window){
    $(document).on('ready', function() {
        /*
        .woocommerce-checkout .woocommerce-message,
        .woocommerce-checkout .woocommerce-error
        */
        var checkout = $('.woocommerce-checkout');
        if (checkout) {
            checkout.on('click', '.woocommerce-message, .woocommerce-error', function () {
               $(this).hide();
            });
        }

        /*
        * Checkout Loader
        --------------------------------------------------------- */
        var terms = $('#terms');

        if (terms) {
            $('body').on('change', '#terms', function () {
                
                if ($(this).is(':checked')) {
                    $('input#place_order').prop('disabled', false);
                    $('input#place_order').removeClass('disabled');
                } else {
                    $('input#place_order').prop('disabled', true);
                    $('input#place_order').addClass('disabled');
                }
            });

            $('body').on('click', 'input#place_order', function () {
                $('.cargador-checkout').addClass('active');
            });
        }

    });

})(jQuery, window);
