/* 
 * Para enviar datos desde PHP al cliente sin afectar la respuesta del ajax
 */

jQuery(document).ajaxSuccess(function( event, xhr, settings ) {
    
  var debuginfo = xhr.getResponseHeader('mociondebug');
  if (!debuginfo) return;
  
  try{
    debuginfo = jQuery.parseJSON( debuginfo );
  } catch(e){}
  
  console.log(debuginfo);

});