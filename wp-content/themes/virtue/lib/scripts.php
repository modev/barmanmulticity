<?php
/**
 * Enqueue scripts and stylesheets
 *
 */
function kadence_scripts() {
  $jquery_css_base = '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/blitzer/jquery-ui.min.css';
  wp_enqueue_style( 'jquery-ui-standard-css', $jquery_css_base );
  wp_enqueue_style('kadence_bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css', false, filemtime( dirname( __FILE__ )) );
  wp_enqueue_style('kadence_theme', get_template_directory_uri() . '/assets/css/virtue.css', false, filemtime( dirname( __FILE__ )) );
global $virtue; if(isset($virtue['skin_stylesheet']) || !empty($virtue['skin_stylesheet'])) {$skin = $virtue['skin_stylesheet'];} else { $skin = 'default.css';} 
 wp_enqueue_style('virtue_skin', get_template_directory_uri() . '/assets/css/skins/'.$skin.'', false, filemtime( dirname( __FILE__ )) );

  if (is_child_theme()) {
   wp_enqueue_style('roots_child', get_stylesheet_uri(), false, filemtime( dirname( __FILE__ )) );
  } 

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
  wp_enqueue_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', filemtime( dirname( __FILE__ )) );
 
  wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.6.2.min.js', false, filemtime( dirname( __FILE__ )) , false);
  wp_register_script('kadence_plugins', get_template_directory_uri() . '/assets/js/plugins.js', false, filemtime( dirname( __FILE__ )), true);
  wp_register_script('kadence_main', get_template_directory_uri() . '/assets/js/main.js', false, filemtime( dirname( __FILE__ )), true);
  //wp_enqueue_script('jquery');
  wp_enqueue_script('modernizr');
  wp_enqueue_script('masonry');
  wp_enqueue_script('kadence_plugins');
  wp_enqueue_script('kadence_main');

   if(class_exists('woocommerce')) {
  wp_deregister_script('wc-add-to-cart-variation');
  wp_register_script( 'wc-add-to-cart-variation', get_template_directory_uri() . '/assets/js/add-to-cart-variation-ck.js' , array( 'jquery' ), false, '200', true );
    wp_localize_script( 'wc-add-to-cart-variation', 'wc_add_to_cart_variation_params', apply_filters( 'wc_add_to_cart_variation_params', array(
      'i18n_no_matching_variations_text' => esc_attr__( 'Sorry, no products matched your selection. Please choose a different combination.', 'woocommerce' ),
      'i18n_unavailable_text'            => esc_attr__( 'Sorry, this product is unavailable. Please choose a different combination.', 'woocommerce' ),
    ) ) );
  wp_enqueue_script( 'wc-add-to-cart-variation');
  }


}
add_action('wp_enqueue_scripts', 'kadence_scripts', 100);

// Carga de librería jQuery UI en todo el sitio

//add_action( 'edit_user_profile_update', 'kad_save_extra_profile_fields' );
