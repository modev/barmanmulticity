<?php

if (kadence_display_sidebar()) {
    $slide_sidebar = 848;
} else {
    $slide_sidebar = 1140;
}
global $post, $virtue;
$headcontent = get_post_meta($post->ID, '_kad_blog_head', true);
if (empty($headcontent) || $headcontent == 'default') {
    if (!empty($virtue['post_head_default'])) {
        $headcontent = $virtue['post_head_default'];
    } else {
        $headcontent = 'none';
    }
}
$height = get_post_meta($post->ID, '_kad_posthead_height', true);
if (!empty($height))
    $slideheight = $height;
else
    $slideheight = 400;
$swidth = get_post_meta($post->ID, '_kad_posthead_width', true);
if (!empty($swidth))
    $slidewidth = $swidth;
else
    $slidewidth = $slide_sidebar;
$connected = new WP_Query(array(
    'connected_type' => 'products_to_posts',
    'connected_items' => get_queried_object(),
    'nopaging' => true,
));
?>
<?php $catid = get_the_category();
if(isset($catid[0])) {
    if ($catid[0]->cat_ID == 88) {
        $catname = $catid[1]->cat_name;
    } else {
        $catname = $catid[0]->cat_name;
    };
}

?>
<?php while (have_posts()) : the_post(); ?>
<article <?php post_class(); ?>>
    <!-- SINGLE POST - header ================================================================== -->
    <?php if ($connected->posts) : $pr = $connected->posts[0]; ?>
    <div class="producto-head">
        <div class="container">

            <div class="row">

                <div class="col-xs-12 col-sm-12">

                    <header class="textos-head">
                        <div class="title-post"><?php the_title(); ?></div>
                        <div class="grupo-comprar">
                            <div class="title-product">
                                Mejor con <span><?php echo $pr->post_title ?></span>
                                <div class="comprar"> <a href="<?php echo get_permalink($pr->ID); ?>" data-jckqvpid="<?php echo $pr->ID; ?>">Comprar</a></div>
                            </div>
                        </div>
                    </header>
                    <div class="imagen-producto"><?php echo get_the_post_thumbnail($pr->ID, 'full'); ?></div>

                </div>

            </div>

        </div><!-- header wrap / para centrar contenido -->
    </div>
    <?php endif; ?>
    <div id="content" class="container">

        <nav class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
            <div class="bc-contenedor">
                <?php
if (function_exists('bcn_display')) {
    bcn_display();
}
                ?>
            </div>
        </nav>

        <div class="row single-article-header">
            <div class="col-sm-8">
                <header class="el-titulo">
                    <h1><?php the_title(); ?></h1>
                </header>
            </div>
        </div><!-- row end -->

        <div class="row single-article">

            <div class="main col-sm-<?php echo (kadence_display_sidebar())?8:12;?>" role="main">

                <?php if ($headcontent == 'flex') { ?>
                <section class="postfeat">
                    <div class="flexslider" style="max-width:<?php echo $slidewidth; ?>px;">
                        <ul class="slides">
                            <?php
                                                   $image_gallery = get_post_meta($post->ID, '_kad_image_gallery', true);
                                                   if (!empty($image_gallery)) {
                                                       $attachments = array_filter(explode(',', $image_gallery));
                                                       if ($attachments) {
                                                           foreach ($attachments as $attachment) {
                                                               $attachment_url = wp_get_attachment_url($attachment, 'full');
                                                               $image = aq_resize($attachment_url, $slidewidth, $slideheight, true);
                                                               if (empty($image)) {
                                                                   $image = $attachment_url;
                                                               }
                                                               echo '<li><img src="' . $image . '"/></li>';
                                                           }
                                                       }
                                                   } else {
                                                       $attach_args = array('order' => 'ASC', 'post_type' => 'attachment', 'post_parent' => $post->ID, 'post_mime_type' => 'image', 'post_status' => null, 'orderby' => 'menu_order', 'numberposts' => -1);
                                                       $attachments = get_posts($attach_args);
                                                       if ($attachments) {
                                                           foreach ($attachments as $attachment) {
                                                               $attachment_url = wp_get_attachment_url($attachment->ID, 'full');
                                                               $image = aq_resize($attachment_url, $slidewidth, $slideheight, true);
                                                               if (empty($image)) {
                                                                   $image = $attachment_url;
                                                               }
                                                               echo '<li><img src="' . $image . '"/></li>';
                                                           }
                                                       }
                                                   }
                            ?>
                        </ul>
                    </div>

                    <!--Flex Slides-->
                    <script type="text/javascript">
                        jQuery(window).load(function() {
                            jQuery('.flexslider').flexslider({
                                animation: "fade",
                                animationSpeed: 400,
                                slideshow: true,
                                slideshowSpeed: 7000,
                                before: function(slider) {
                                    slider.removeClass('loading');
                                }
                            });
                        });
                    </script>
                </section>
                <?php } else if ($headcontent == 'video') { ?>
                <section class="postfeat">
                    <div class="videofit">
                        <?php $video = get_post_meta($post->ID, '_kad_post_video', true);
                                                           echo $video; ?>
                    </div>
                </section>
                <?php } else if ($headcontent == 'image') { ?>
                <?php
                    $height = get_post_meta($post->ID, '_kad_posthead_height', true);
                    if ($height != '')
                        $slideheight = $height;
                    else
                        $slideheight = 350;
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url($thumb, 'home-post'); //get full URL to image (use "large" or "medium" if the images too big)
                    $image = aq_resize($img_url, $slidewidth, $slideheight, true); //resize & crop the image
                    if (empty($image)) {
                        $image = $img_url;
                    }
                ?>
                <?php if ($image) : ?>
                <section class="postfeat">
                    <div class="post-single-img">
                        <img src="<?php echo $image ?>" alt="<?php the_title(); ?>" />
                    </div>
                    <?php
                    $pie_foto = get_post_meta($post->ID, 'piedefoto', true);
                    if (empty($pie_foto)):
                    else:
                    ?><div class="pie-foto">
                    <?php echo get_post_meta($post->ID, 'piedefoto', true); ?>
                    </div><?php endif ?>

                    <div class="sharebe"><p>Compartir</p><?php
                        echo do_shortcode('[ssba]');
                        ?></div>


                    <?php if ($connected->posts) :

                    $contador = 0;

                    foreach ($connected->posts as $pr) :

                    if ($contador++ >= 1) {
                        break;
                    }



                    ?>
                    <div class="compraWrap">
                        <div class="imagen_product_post">
                            <?php  print get_the_post_thumbnail( $pr->ID, 'shop_thumbnail'); ?>
                        </div>

                        <div class="attached-product">
                            <div class="preparalo">
                                <p>Mejor con</p>
                                <div class="titulo_producto"><?php echo $pr->post_title; ?></div>
                            </div>
                            <div class="comprar">
                                <a data-interaccion-id="<?php echo HorusConf::get_var('interaccion_agregado_al_carrito_id') ?>"
                                   data-interaccion-agregado_desde="interna de articulo call to action superior"
                                   data-interaccion-productid="<?php echo $pr->ID ?>"
                                   href="?p=<?php echo $pr->ID ?>">
                                    Comprar
                                </a>
                            </div>
                        </div>
                    </div>

                    <?php endforeach; endif;?>

                    <!-- RENDER LINK PARA AGREGAR POST COMO FAVORITO -->
                    <div class="favoritos open-login-form"><?php wpfp_link() ?> </div>
                </section>
                <?php endif; ?>

                <?php } ?>
                <?php // get_template_part('templates/post', 'date');  ?>

                <!-- SINGLE POST - THE POST CONTENT ================================================================== -->
                <div class="entry-content">
                    <?php

$categoria = get_the_category();

if($categoria[0]->name == "Video"){ ?>

                    <div class="contenedor_video" id="contenedor_<?php echo get("video_youtube_id"); ?>" data-id-yt="<?php echo get("video_youtube_id"); ?>">
                        <?php
                                   set_query_var('video_youtube', $post);
                                   get_template_part('templates/yt', 'imagen');
                        ?>

                        <div class="video" id="video_<?php echo get("video_youtube_id"); ?>">

                        </div>

                    </div>
                    <?php } ?>
                    <?php
                    the_content();
                    ?>
                </div>
                <div class="responsabilidad"><p>Contenido para adultos. No compartir con menores de edad.</p></div>
                <div class="tags">
                    <?php $tags = get_the_tags();
if ($tags) { ?>
                    <span class="posttags"><i class="icon-tag"></i>
                        <?php the_tags('', ', ', ''); ?>
                    </span>
                    <?php } ?>
                </div>

                <!-- SINGLE POST - en la tienda ================================================================== -->
                <?php if ($connected->posts) : ?>
                <div class="cat-title"><div class="titulo_en_tienda">En la tienda</div></div>
                <section class="en-tienda">
                    <?php foreach ($connected->posts as $pr) :; ?>
                    <article class="en-tienda-producto col-xs-12 col-sm-4">
                        <?php echo do_shortcode('[product id="' . $pr->ID . '"]'); ?>
                        <?php //echo '<div class="comprar"><a href="'.get_permalink($pr->ID).'" data-jckqvpid="'.$pr->ID.'">Comprar</a></div>'; ?>
                    </article>
                    <?php endforeach; ?>
                </section>
                <?php endif; ?>


                <?php // comments_template('/templates/comments.php');  ?>

                <footer class="single-footer">
                    <?php if (get_post_meta($post->ID, '_kad_blog_author', true) == 'yes') {
    virtue_author_box();
} ?>

                    <?php
$prevpt = get_previous_post('true');
$id = isset($prevpt->ID) ? $prevpt->ID : 0;
if (class_exists('Dynamic_Featured_Image')) {
    global $dynamic_featured_image;

    $featured_image = $dynamic_featured_image->get_featured_images($id);
    $imgsrc = isset($featured_image[0]) ? $featured_image[0] : null;
    if (!is_null($imgsrc)) {
        $prevthumb = '<img src="' . $imgsrc['full'] . '"/>';
    }
}
$pprlink = get_post_permalink($id);
                    ?>

                    <?php
$nextpt = get_next_post('true');

$idnext = isset($nextpt->ID) ? $nextpt->ID : null;

if (class_exists('Dynamic_Featured_Image')) {
    global $dynamic_featured_image;
    $featured_image = $dynamic_featured_image->get_featured_images($idnext);
    $imgsrc = isset($featured_image[0]) ? $featured_image[0] : null;
    if (!is_null($imgsrc)) {
        $nextthumb = '<img src="' . $imgsrc['full'] . '"/>';
    }
}
$pstlink = get_post_permalink($idnext);
                    ?>
                    <div>

                        <?php if (isset($prevpt->ID)) { ?>
                        <div class="prev-post-barman">
                            <div class="linkpost-anterior-imagen">
                                <a href="<?php echo $pprlink; ?>">
                                    <?php echo $prevthumb; ?>
                                </a>
                            </div>
                            <div class="linkpost-anterior-titulo">
                                <a href="<?php echo $pprlink; ?>"><?php echo $prevpt->post_title ?></a>
                            </div>
                            <div class="linkpost-anterior">
                                <a href="<?php echo $pprlink; ?>">
                                    Anterior en <?php echo $catname; ?>
                                </a>
                            </div>
                        </div>

                        <?php } ?>
                        <?php if (isset($nextpt->ID)) { ?>

                        <div class="next-post-barman">
                            <div class="linkpost-siguiente-imagen">
                                <a href="<?php echo $pstlink; ?>">
                                    <?php echo $nextthumb; ?>
                                </a>
                            </div>
                            <div class="linkpost-siguiente-titulo">
                                <a href="<?php echo $pstlink; ?>"><?php echo $nextpt->post_title ?></a>
                            </div>
                            <div class="linkpost-siguiente">
                                <a href="<?php echo $pstlink; ?>">
                                    Siguiente en <?php echo $catname; ?>
                                </a>
                            </div>
                        </div>

                        <?php } ?>

                    </div>
                </footer>

            </div>


            <?php if (kadence_display_sidebar()):?>
            <div class="related-sidebar col-sm-<?php echo (kadence_display_sidebar())?4:12;?>">
                <div class="cat-title"><h2>RELACIONADOS</h2></div>
                <div class="relacionados-sidebar">
                    <?php
$args = array(
    'numberposts' => 2,
    'offset' => 0,
    'category' => isset($catid[0]->cat_ID) ? $catid[0]->cat_ID : null,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => false);

$recent_posts = wp_get_recent_posts($args, OBJECT);

$tit = ($recent_posts[0]->ID == $post->ID) ? $recent_posts[1]->ID : $recent_posts[0]->ID;
$title = ($recent_posts[0]->ID == $post->ID) ? $recent_posts[1]->post_title : $recent_posts[0]->post_title;
$prm = get_post_permalink($tit);
if (class_exists('Dynamic_Featured_Image')) {
    global $dynamic_featured_image;
    $featured_image = $dynamic_featured_image->get_featured_images($tit);
    $image = '<a href="' . $prm . '"><img src="' . $featured_image[0]['full'] . '"/></a>';
}
                    ?>
                    <?php echo $image; ?>
                    <div class="titlesr">
                        <a href="<?php echo $prm; ?>"><h3 class="title"><?php echo $title ?></h3></a>
                    </div>
                </div>

                <?php if (is_mobile() == false) {?>
                <?php if (is_user_logged_in()) { ?>
                <div class="cat-title"><h2>Favoritos</h2></div>


                <?php
                        // muestra post marcados como favoritos por el usuario actual
                        foreach (wpfp_get_users_favorites() as $key => $favorito) :
                        //
                        $tit = do_shortcode($favorito);
                        $post = get_post($tit, OBJECT);
                        $enlace_post = get_permalink($post->ID);
                ?>

                <p><a href="<?php echo $enlace_post; ?>"><?php echo ($post->post_title); ?></a></p>

                <?php endforeach;
                    } else {
                ?>
                <div class="cat-title"><h2>Registro</h2></div>

                <?php } ?>

                <div>
                    <?php echo do_shortcode('[clean-login-register]'); ?>
                </div>
                <?php } ?>
            </div>
			<?php endif;?>
        </div>
    </div>

</article>
<?php endwhile; ?>
</div>
