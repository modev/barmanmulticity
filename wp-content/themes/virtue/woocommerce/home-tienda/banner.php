<?php
$the_banner = new WP_Query('post_type=banner');

if ( $the_banner->have_posts() ) : ?>
	<?php while ( $the_banner->have_posts() ) : $the_banner->the_post(); ?>
		<a class="banner-top-tienda" href="<?php echo get('informacion_general_enlace'); ?>">
				<picture>
					<source srcset="<?php print get_image('informacion_general_imagen_desktop',1,1,0); ?>" media="(min-width: 1200px)">
					<source srcset="<?php echo get_image('informacion_general_imagen_moviles',1,1,0); ?>" media="(max-width: 767px)">
					<img srcset="<?php print get_image('informacion_general_imagen_desktop',1,1,0); ?>" alt="Barman club">
				</picture>
		</a>
	<?php endwhile; ?>
<?php endif; ?>
