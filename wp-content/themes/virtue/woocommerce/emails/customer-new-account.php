<?php

/**
 * Customer new account email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
?>

<?php do_action('woocommerce_email_header', $email_heading); ?>

<table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="background:#ffffff;">    <tbody>
        <tr>
            <td style="font-family:Arial,Helvetica,sans-serif;padding:55px 38px 40px 38px;text-align:center;text-transform:uppercase;font-weight:bold;color:#d10018;font-size:37px">
                ¡Bienvenido a <span class="il">Barman</span>!
            </td>
        </tr>
        <tr>
            <td style="font-family:Arial,Helvetica,sans-serif;padding:0px 38px 0px 38px;text-align:center;text-transform:none;font-weight:normal;color:#4e4e4e;font-size:18px">A partir de ahora estarás siempre al tanto de lo Último en<br> fiestas, restaurantes, bares, tendencias<br> y mucho más en <span class="il">Barman</span> Club. </td>
        </tr>
        <!-- <tr>
            <td style="font-family:Arial,Helvetica,sans-serif;padding:20px 38px 45px 38px;text-align:center;text-transform:none;font-weight:normal;color:#959595;font-size:15px">Para empezar a disfrutar de todos los beneficios que tiene<br> ser parte de este Club, el valor del envÃ­o de tu prÃ³xima compra es <span style="color:red">gratis*.</span></td>
        </tr> -->
        <tr>
            <td style="padding:0px 150px 0px 150px">
                <table width="100%" border="0" cellspacing="9" cellpadding="0" style="background:#fafafa;border-style:solid;border-width:2px;border-color:#eaeaea">
                    <tbody>
                        <tr>
                            <td width="6%" style="padding:3px 0px 0px 0px">
                                <img src="https://ci6.googleusercontent.com/proxy/8CdqrKq3KoK2tBd2wfeKu7N_bhXQiEhnvic1cex-YZn-6nJ2ObiO-g6chBWsMd6LZBCVR5B7sAJhkXZ_1uHu8yajfBdHuJuxhLzXLkiM9bzoMc4vPPeDJj9G1mXCSJcARi-jeBhdmj0-JvKZcRyXdJYewA=s0-d-e1-ft#http://pernod-test.s3.amazonaws.com/barman/bienvenida060415/barman_footerLEFTarrow_greyBG.gif" width="17" height="17" alt="" style="display:block" class="CToWUd">
                            </td>
                            <td width="88%" style="text-align:center;font-family:Arial,Helvetica,sans-serif;color:#4e4e4e;font-weight:bold;font-size:16px;padding:3px 0px 0px 0px">
                                <a href="<?php echo get_site_url();?>/tienda" style="color:#4e4e4e;text-decoration:none" target="_blank">
                                    ¡Haz tu compra aquí!</a>
                            </td>
                            <td width="6%" style="padding:3px 0px 0px 0px">
                                <img src="https://ci3.googleusercontent.com/proxy/UnOskjhc9vJsNtmj-hYM0cRDB8j4EuEmlzdK45LsxRYtPfMK-ZR6JDJh_42CMhbkjeHcCyJNNWkSH9pP8doxgYCSnW_HxtF2V_5xT76bPe9H5FSfHk4vNISE4U1JnSUaUJz92zr2brFpAhMookV1Z0hOWSI=s0-d-e1-ft#http://pernod-test.s3.amazonaws.com/barman/bienvenida060415/barman_footerRIGHTarrow_greyBG.gif" width="17" height="17" alt="" style="display:block" class="CToWUd">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:40px 0px 0px 0px">
                <img src="https://ci4.googleusercontent.com/proxy/ZZcaR8cK69trSGqzqnYwSVBCaTs62H4eo8c-xTbUDZiM1Ksa-CbnoyOc10NyIrdGRZ2uXbCcxolFqAz7nifultDa1ekhLQj9eXG4BV5UB9dE-hXe2e2jU52U6kPmPVofbWBKKXaS-_M3jA=s0-d-e1-ft#http://pernod-test.s3.amazonaws.com/barman/bienvenida060415/barman_footerTexture.gif" width="650" height="16" alt="" style="display:block" class="CToWUd">
            </td>
        </tr>
        <tr>
            <td style="padding:50px 0px 20px 0px">
                <img src="https://ci3.googleusercontent.com/proxy/x5v46BuCxZOjYG8YefqXSBtr6N2zUiacpndvE3n7EPLGd6HeYr_cczBsBQa8WIYrCA-TcbqzfD04rWrWQbid7n-q_CwrAFYXDZNbIx4vzI4NNCO5PfquR29a250_mo9Z7wyLuXegyzl21bA=s0-d-e1-ft#http://pernod-test.s3.amazonaws.com/barman/bienvenida060415/barman_footer_bottles.gif" width="650" height="172" alt="" style="display:block" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 665.5px; top: 776px;">
            </td>
        </tr>
        <tr>
            <td style="padding:30px 38px 30px 38px;background:#f8f8f8;border-top-color:#eaeaea;border-top-style:solid;border-top-width:thin">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr>
                            <td width="15%" style="padding:0px 0px 0px 119px"><img src="https://ci6.googleusercontent.com/proxy/8CdqrKq3KoK2tBd2wfeKu7N_bhXQiEhnvic1cex-YZn-6nJ2ObiO-g6chBWsMd6LZBCVR5B7sAJhkXZ_1uHu8yajfBdHuJuxhLzXLkiM9bzoMc4vPPeDJj9G1mXCSJcARi-jeBhdmj0-JvKZcRyXdJYewA=s0-d-e1-ft#http://pernod-test.s3.amazonaws.com/barman/bienvenida060415/barman_footerLEFTarrow_greyBG.gif" width="17" height="17" alt="" style="display:block" class="CToWUd"></td>
                            <td width="70%" style="text-align:center;font-family:Arial,Helvetica,sans-serif;color:#4e4e4e;font-size:16px;font-weight:normal">Anfitriones de la buena vida.</td>
                            <td width="15%" style="padding:0px 119px 0px 0px"><img src="https://ci3.googleusercontent.com/proxy/UnOskjhc9vJsNtmj-hYM0cRDB8j4EuEmlzdK45LsxRYtPfMK-ZR6JDJh_42CMhbkjeHcCyJNNWkSH9pP8doxgYCSnW_HxtF2V_5xT76bPe9H5FSfHk4vNISE4U1JnSUaUJz92zr2brFpAhMookV1Z0hOWSI=s0-d-e1-ft#http://pernod-test.s3.amazonaws.com/barman/bienvenida060415/barman_footerRIGHTarrow_greyBG.gif" width="17" height="17" alt="" style="display:block" class="CToWUd"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<?php do_action('woocommerce_email_footer'); ?>
