<?php
/**
 * Customer completed order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
?>

<?php
do_action('woocommerce_email_header', $email_heading);

$telefono = get_telefono_email_customer($order);
?>


<table width="650" border="0" cellspacing="0" cellpadding="0" align="center" style="background:#ffffff;">
    <tr>
        <td style="font-family:arial, helvetica, sans-serif; padding:55px 38px 60px 38px; color:#959595; font-size:15px;">

            <p><?php printf(__("Tu pedido ha sido recibido y ya está siendo procesado. Los detalles para la entrega son los siguientes:", 'woocommerce'), get_option('blogname')); ?></p>

            <p><?php printf(__("Recibirás una llamada en los próximos 10 minutos para programar la hora de entrega de tu producto. Si no la recibes llama al número " . apply_filters( 'telefono_distribuidor', '4926307') . " en ".apply_filters( 'ciudad_distribuidor', 'Bogotá'), 'woocommerce'), get_option('blogname'));   ?></p>

            <p><?php printf(__("No vendemos a menores de edad. Ten presente que quien recibe el pedido debe presentar su cédula.", 'woocommerce'), get_option('blogname')); ?></p>

            <p><?php do_action('woocommerce_email_before_order_table', $order, true, false); ?></p>

        </td>
    </tr>


    <tr>
        <td style="font-family:arial, helvetica, sans-serif; color:#4e4e4e; color:#4e4e4e; font-size:26px; font-weight:bold; text-align:left; padding:0px 57px 15px 57px;">
            Pedido: <span style="color:#d10018;">#<?php echo $order->id; ?></span></td>
    </tr>


    <tr>
        <td style="padding:0px 38px 0px 38px; color:#959595;">

            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#e9e9e9;">
                <thead>
                    <tr>
                        <td width="3%">&nbsp;</td>
                        <td width="41%" style="font-family:arial, helvetica, sans-serif; color:#747474; font-weight:bold; text-transform:uppercase; font-size:14px; padding:25px 0px 10px 19px; text-align:left;">Producto</td>
                        <td width="22%" style="font-family:arial, helvetica, sans-serif; color:#747474; font-weight:bold; text-transform:uppercase; font-size:14px; padding:25px 0px 10px 0px; text-align:center;">Cantidad</td>
                        <td width="31%" style="font-family:arial, helvetica, sans-serif; color:#747474; font-weight:bold; text-transform:uppercase; font-size:14px; padding:25px 19px 10px 0px; text-align:right;">Precio</td>
                        <td width="3%">&nbsp;</td>
                    </tr>
                </thead>
                <tbody>
                    <?php echo $order->email_order_items_table(false, true); ?>
                </tbody>
                <tfoot>
                    <?php
                    if ($totals = $order->get_order_item_totals()) {
                        $i = 0;
                        foreach ($totals as $total) {
                            $i++;
                            ?><tr>
                                <td width="3%">&nbsp;</td>
                                <td style="background:#ffffff;">&nbsp;</td>
                                <td style="background:#ffffff; font-family:arial, helvetica, sans-serif; color:#747474; font-size:12px; text-transform:uppercase; padding:10px 0px 7px 0px; text-align:right;"><?php echo $total['label']; ?></td>
                                <td style="background:#ffffff; font-family:arial, helvetica, sans-serif; color:#959595; font-size:14px; padding:10px 19px 7px 0px; text-align:right;"><?php echo $total['value']; ?></td>
                                <td width="3%">&nbsp;</td>
                            </tr><?php
                        }
                    }
                    ?>

                    <?php
                    if (!empty($cupones)) :
                        ?>

                        <tr>
                            <td width="3%">&nbsp;</td>
                            <td style="background:#ffffff;">&nbsp;</td>
                            <td style="background:#ffffff; font-family:arial, helvetica, sans-serif; color:#747474; font-size:12px; text-transform:uppercase; padding:10px 0px 7px 0px; text-align:right;">Cupones que se usaron</td>
                            <td style="background:#ffffff; font-family:arial, helvetica, sans-serif; color:#959595; font-size:14px; padding:10px 19px 7px 0px; text-align:right;">
                                <?php
                                $separado = implode(",", $cupones);
                                echo $separado;
                                ?>
                            </td>
                            <td width="3%">&nbsp;</td>
                        </tr>

                    <?php endif; ?>

                    <tr>
                        <td colspan="5"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="30" alt=""></td>
                    </tr>

                </tfoot>
            </table>

        </td>
    </tr>

    <tr>
        <td style="padding:40px 0px 0px 0px;">
         <?php 

                        $cedula =  get_post_meta( $order->id, '_billing_cedula', true );
                        $phone =  get_post_meta( $order->id, '_billing_phone_2', true );    

                        ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fbfbfb;">
                <tbody>
                    <tr>
                        <td><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_infoTopBar.gif" width="650" height="10" alt="" style="display:block;"></td>
                    </tr>
                    <tr>
                        <td style="font-family:arial, helvetica, sans-serif; color:#4e4e4e; color:#4e4e4e; font-size:20px; font-weight:bold; text-align:left; padding:40px 57px 10px 57px;"><?php _e('Customer details', 'woocommerce'); ?></td>
                    </tr>
                    <tr>
                        <td style="padding:0px 57px 0px 57px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#ffffff; border-color:#e7e7e7; border-style:solid; border-width:thin;">
                                <tbody>
                                    <tr>
                                        <td width="25%" style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; font-weight:bold; text-transform:uppercase; color:#747474; padding:20px 0px 5px 20px;"><?php _e('Email:', 'woocommerce'); ?></th> </td>
                                        <td width="75%" style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; color:#959595; padding:20px 0px 5px 0px;"> <a href="mailto:apinedaj@gmail.com" target="_blank"><?php echo $order->billing_email; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; font-weight:bold; text-transform:uppercase; color:#747474; padding:5px 0px 20px 20px;"><?php _e('Teléfono:', 'woocommerce'); ?></td>
                                        <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; color:#959595; padding:5px 0px 20px 0px;"> <?php echo $order->billing_phone; ?></td>
                                    </tr>
                                    <tr>
                                    <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; font-weight:bold; text-transform:uppercase; color:#747474; padding:5px 0px 20px 20px;"><?php print "Celular"  ?></td>
                                    <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; color:#959595; padding:5px 0px 20px 0px;"> <?php echo $phone; ?></td>
                                </tr>
                                <tr>
                                    <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; font-weight:bold; text-transform:uppercase; color:#747474; padding:5px 0px 20px 20px;"><?php print "Cédula"; ?></td>
                                    <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; color:#959595; padding:5px 0px 20px 0px;"> <?php echo $cedula; ?></td>
                                </tr>
                                    <!--<tr>
                                            <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; font-weight:bold; text-transform:uppercase; color:#747474; padding:5px 0px 20px 20px;">Celular</td>
                                            <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; color:#959595; padding:5px 0px 20px 0px;"> 3166291971</td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="font-family:arial, helvetica, sans-serif; color:#4e4e4e; color:#4e4e4e; font-size:20px; font-weight:bold; text-align:left; padding:40px 57px 10px 57px;">Facturar a</td>
    </tr>
    <tr>
        <td style="padding:0px 57px 0px 57px;">
            <?php wc_get_template('emails/email-addresses.php', array('order' => $order)); ?>
        </td>
    </tr>

</table>


<?php
do_action('woocommerce_email_footer');
?>



