<?php
/**
 * Admin new order email
 *
 * @author WooThemes
 * @package WooCommerce/Templates/Emails/HTML
 * @version 2.0.0
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>
	<tr>
		<td style="font-family:Arial, Helvetica, sans-serif; padding:55px 38px 60px 38px; color:#959595;
		font-size:15px;">
			<p>Hola <span style="font-weight:bold; font-size:19px; color:#4e4e4e;"><?php printf( __( ' %s. ', 'woocommerce' ),
			$order->billing_first_name . ' ' . $order->billing_last_name ); ?></span></p>
	        <p>Tu pedido reciente en Barman se ha completado.Los detalles se muestran a continuación para tu referencia:</p>
	        <p>Entrega en menos de 1 hora.<br>
	        No vendemos a menores de edad, es necesario presentar la cédula de la persona que recibe el pedido.
	    </td>
    </tr>
    <tr>
      	<td style="font-family:Arial, Helvetica, sans-serif; color:#4e4e4e; color:#4e4e4e; font-size:26px;
      	font-weight:bold; text-align:left; padding:0px 57px 15px 57px;"><a href="<?php echo admin_url( 'post.php?post=' . $order->id . '&action=edit' ); ?>">
		<?php printf( __( 'Order: %s', 'woocommerce'), $order->get_order_number() ); ?></a></span>
		</td>
    </tr>
    <tr>
        <td style="padding:0px 38px 0px 38px; color:#959595;">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#e9e9e9;">
		        <tbody>
		        	<tr>
		        		<td>
							<?php  do_action( 'woocommerce_email_before_order_table', $order, true, false ); ?>

							<table cellspacing="0" cellpadding="6" style="width: 100%;" bordercolor="#eee">
								<tbody>
									<tr>
							            <td width="3%"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="20" alt=""/></td>
							            <td width="41%" style="font-family:Arial, Helvetica, sans-serif; color:#747474; font-weight:bold; text-transform:uppercase; font-size:14px; padding:25px 0px 10px 19px; text-align:left;">Producto</td>
							            <td width="22%" style="font-family:Arial, Helvetica, sans-serif; color:#747474; font-weight:bold; text-transform:uppercase; font-size:14px; padding:25px 0px 10px 0px; text-align:center;">Cantidad</td>
							            <td width="31%" style="font-family:Arial, Helvetica, sans-serif; color:#747474; font-weight:bold; text-transform:uppercase; font-size:14px; padding:25px 19px 10px 0px; text-align:right;">Precio</td>
							            <td width="3%"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="20" alt=""/></td>
							        </tr>

									<?php echo $order->email_order_items_table( false, true ); ?>
									<tr>
            							<td colspan="5">
            							<img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="10" alt=""
            							style="display:block;"/></td>
            						</tr>
            						<?php
										if ( $totals = $order->get_order_item_totals() ) {
											$i = 0;
											foreach ( $totals as $total ) {
												$i++;
												?>
												<tr>
													<td width="3%">&nbsp;</td>
													<td scope="row" colspan="2" style="background:#FFFFFF; font-family:Arial, Helvetica, sans-serif; color:#747474; font-size:12px; text-transform:uppercase;
													padding:10px 0px 7px 0px; text-align:right;"><?php echo $total['label']; ?></td>
													<td style="background:#FFFFFF; font-family:Arial, Helvetica, sans-serif; color:#747474; font-weight:bold; font-size:14px;
													padding:10px 19px 7px 0px; text-align:right;"><?php echo $total['value']; ?></td>
													<td width="3%">&nbsp;</td>
												</tr>
												<tr>
										            <td width="3%"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="2" alt="" style="display:block;"/></td>
										            <td colspan="3" style="background:#FFFFFF; text-align:center;"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_totalsHLine.gif"
										            width="525" height="2" alt="" style="display:block;"/></td>
										            <td width="3%"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="2" alt="" style="display:block;"/></td>
										        </tr>


												<?php
											}
										}
									?>
									<tr>
						            	<td colspan="5">
						            	<img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="30" alt=""/>
						            	</td>
						            </tr>
						        </tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
		<?php do_action( 'woocommerce_email_after_order_table', $order, true, false ); ?>
	 </tr>
    <tr>
      <td style="font-family:Arial, Helvetica, sans-serif; color:#4e4e4e; color:#959595; font-size:12px; font-weight:normal; text-align:left;
      padding:15px 57px 0px 57px; font-style:italic;">
      <?php do_action( 'woocommerce_email_order_meta', $order, true, false ); ?>
    </tr>
    <tr>
      <td style="padding:40px 0px 0px 0px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fbfbfb;">
        <tbody>
          <tr>
            <td><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_infoTopBar.gif" width="650" height="10" alt="" style="display:block;"/></td>
          </tr>
          <tr>
            <td style="font-family:Arial, Helvetica, sans-serif; color:#4e4e4e; color:#4e4e4e; font-size:20px;
            font-weight:bold; text-align:left; padding:40px 57px 10px 57px;">Datos del cliente</td>
          </tr>
          <tr>
            <td style="padding:0px 57px 0px 57px;"><table width="100%" border="0" cellspacing="0"
            cellpadding="0" style="background:#ffffff; border-color:#e7e7e7; border-style:solid; border-width:thin;">
              <tbody>
              	<tr>
                  <td width="25%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left;
                  font-weight:bold; text-transform:uppercase; color:#747474; padding:20px 0px 5px 20px;">
                  <?php _e( 'Email:', 'woocommerce' ); ?></td>
                  <td width="75%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left;
                  color:#959595; padding:20px 0px 5px 0px;"><?php echo $order->billing_email; ?></td>
                </tr>

                <tr>
                  <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left; font-weight:bold;
                  text-transform:uppercase; color:#747474; padding:5px 0px 20px 20px;">
                  <?php _e( 'Teléfono:', 'woocommerce' ); ?></td>
                  <td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left; color:#959595;
                  padding:5px 0px 20px 0px;"><?php echo $order->billing_phone; ?></td>
                </tr>
              </tbody>
            </table></td>
          </tr>
          <tr>
            <td style="font-family:Arial, Helvetica, sans-serif; color:#4e4e4e; color:#4e4e4e; font-size:20px;
            font-weight:bold; text-align:left; padding:40px 57px 10px 57px;">Facturar a</td>
          </tr>
          <tr>
            <td style="padding:0px 57px 0px 57px;">
            <table width="100%" border="0" cellspacing="0"
              	cellpadding="0" style="background:#ffffff; border-color:#e7e7e7; border-style:solid; border-width:thin;">
              	<tbody>
                	<tr>
	                  	<td style="font-family:Arial, Helvetica, sans-serif; font-size:14px; text-align:left; color:#959595;
	                  	padding:20px 20px 20px 20px;">
	                  	<?php wc_get_template( 'emails/email-addresses.php', array( 'order' => $order ) ); ?>

	                   	</td>
                	</tr>
              	</tbody>
            </table>
            </td>
          </tr>
          <tr>
            <td style="padding:30px 0px 0px 0px;"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/barman_infoTopBar.gif"
            width="650" height="10" alt="" style="display:block;"/></td>
          </tr>
        </tbody>

<?php do_action( 'woocommerce_email_footer' ); ?>
