<?php
/**
 * Email Addresses
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.2.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#ffffff; border-color:#e7e7e7; border-style:solid; border-width:thin;">
    <tbody>
        <tr>
            <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; color:#959595; padding:20px 20px 20px 20px;">
                <p><?php echo $order->get_formatted_billing_address(); ?></p>
            </td>
            <?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && ( $shipping = $order->get_formatted_shipping_address() )) : ?>

                <td style="font-family:arial, helvetica, sans-serif; font-size:14px; text-align:left; color:#959595; padding:20px 20px 20px 20px;">

                    <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>

                    <p><?php echo $shipping; ?></p>

                </td>

            <?php endif; ?>
        </tr>
    </tbody>
</table>
