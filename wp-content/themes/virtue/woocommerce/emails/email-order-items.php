<?php
/**
 * Email Order Items
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

foreach ( $items as $item_id => $item ) :
	$_product     = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
	$item_meta    = new WC_Order_Item_Meta( $item['item_meta'], $_product );
	?>
	<tr>
		<td width="3%">&nbsp;</td>
		<td style="background:#ffffff; font-family:arial, helvetica, sans-serif; color:#959595; font-size:14px; padding:10px 0px 7px 19px; text-align:left; border-bottom-color:#e9e9e9; border-bottom-style:solid; border-bottom-width:4px;">
		<?php

			// Show title/image etc
			if ( $show_image ) {
				echo apply_filters( 'woocommerce_order_item_thumbnail', '<img src="' . ( $_product->get_image_id() ? current( wp_get_attachment_image_src( $_product->get_image_id(), 'thumbnail') ) : wc_placeholder_img_src() ) .'" alt="' . __( 'Product Image', 'woocommerce' ) . '" height="' . esc_attr( $image_size[1] ) . '" width="' . esc_attr( $image_size[0] ) . '" style="vertical-align:middle; margin-right: 10px;" />', $item );
			}

			// Product name
			echo apply_filters( 'woocommerce_order_item_name', $item['name'], $item );

			// SKU
			if ( $show_sku && is_object( $_product ) && $_product->get_sku() ) {
				echo ' (#' . $_product->get_sku() . ')';
			}

			// File URLs
			if ( $show_download_links && is_object( $_product ) && $_product->exists() && $_product->is_downloadable() ) {

				$download_files = $order->get_item_downloads( $item );
				$i              = 0;

				foreach ( $download_files as $download_id => $file ) {
					$i++;

					if ( count( $download_files ) > 1 ) {
						$prefix = sprintf( __( 'Download %d', 'woocommerce' ), $i );
					} elseif ( $i == 1 ) {
						$prefix = __( 'Download', 'woocommerce' );
					}
					echo '<br/><small>' . $prefix . ': <a href="' . esc_url( $file['download_url'] ) . '" target="_blank">' . esc_html( $file['name'] ) . '</a></small>';
				}
			}

			// Variation
			if ( $item_meta->meta ) {
				echo '<br/><small>' . nl2br( $item_meta->display( true, true ) ) . '</small>';
			}
		?>

		<?php if (isset($item['rp_wcdpd'])):?>
			<p class="esta_en_promocion">Promoción Fin de año ¡Lleva la 4ta Gratis! </p>
		<?php endif;?>

		</td>
		<td style="background:#ffffff; font-family:arial, helvetica, sans-serif; color:#959595; font-size:14px; padding:10px 0px 7px 0px; text-align:center; border-bottom-color:#e9e9e9; border-bottom-style:solid; border-bottom-width:4px;">
			<?php echo $item['qty'] ;?>
		</td>
		<td style="background:#ffffff; font-family:arial, helvetica, sans-serif; color:#959595; font-size:14px; padding:10px 19px 7px 0px; text-align:right; border-bottom-color:#e9e9e9; border-bottom-style:solid; border-bottom-width:4px;">
			<?php echo $order->get_formatted_line_subtotal( $item ); ?>
		</td>
		<td width="3%">&nbsp;</td>
	</tr>

	<?php if ( $show_purchase_note && is_object( $_product ) && $purchase_note = get_post_meta( $_product->id, '_purchase_note', true ) ) : ?>
		<tr>
			<td colspan="5"><img src="http://www.barmanclub.co/wp-content/uploads/2015/02/spacer.gif" width="20" height="10" alt="" style="display:block;"></td>
		</tr>
	<?php endif; ?>

<?php endforeach; ?>
