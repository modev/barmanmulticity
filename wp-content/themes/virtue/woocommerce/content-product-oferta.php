<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

//global $product_remplazo;
$product_remplazo = new WC_product($producto_oferta);

$post_oferta = get_post($producto_oferta);

// Ensure visibility
if (!$product_remplazo || !$product_remplazo->is_visible())
    return;

// Extra post classes
$classes = array();

$classes[] = 'grid_item';
$classes[] = 'product_item';
$classes[] = 'clearfix';
if (isset($virtue['product_img_resize']) && $virtue['product_img_resize'] == 0) {
    $resizeimage = 0;
} else {
    $resizeimage = 1;
}

//traer categorias
$cat_producto = get_the_terms($product_remplazo->id, 'product_cat');
?>

<div class="<?php echo $itemsize; ?> kad_product <?php
foreach ($cat_producto as $cat) {
    echo $cat->name;
}
?>"
     data-producto-categoria="<?php
     foreach ($cat_producto as $cat) {
         echo strtolower($cat->name);
     }
     ?>"
     data-producto-titulo="<?php strtolower($product_remplazo->post->post_title); ?>"  
     >


    <div class="woocommerce-message-left">
        <div class="grupo-img-cart">
            <a href="<?php echo $product_remplazo->get_permalink(); ?>" class="product_item_link">

                <?php echo woocommerce_show_product_loop_sale_flash($post_oferta, $product_remplazo); ?>
                <?php
                if ($resizeimage == 1) {
                    if (has_post_thumbnail()) {
                        $product_remplazo_image = wp_get_attachment_image_src(get_post_thumbnail_id($post_oferta->ID), 'full');
                        $product_remplazo_image_url = $product_remplazo_image[0];
                        ?>
                        <img src="<?php echo $product_remplazo_image_url; ?>" class="attachment-shop_catalog wp-post-image" alt="<?php $product_remplazo->post->post_title; ?>">
                        <?php
                    } elseif (woocommerce_placeholder_img_src()) {
                        echo woocommerce_placeholder_img('shop_catalog');
                    }
                } else {
                    echo '<div class="kad-woo-image-size">';
                    echo woocommerce_template_loop_product_thumbnail();
                    echo '</div>';
                }
                ?>
            </a>
        </div>

        <div class="grupo-info-cart">
            <a href="<?php echo $product_remplazo->get_permalink(); ?>" class="product_item_link">
                <span class="title_product">TE ESTÁS PERDIENDO DE UNA OFERTA HOY</span>

                <p><?php _e('Este mes por la compra de ', 'woocommerce') ?><?php echo $product_remplazo->post->post_title; ?><?php _e(' pagas el mismo valor.', 'woocommerce') ?></p>
                <div class="product_details">

                    <?php foreach ($cat_producto as $cat) : ?>
                        <div class="cat_producto"><?php echo $product_remplazo->post->post_title; ?></div>
<?php endforeach; ?>
                    <div class="precio-producto">
                        <span class="precio-producto-label">Precio</span>
                        <?php if ($price_html = $product_remplazo->get_price_html()) : ?>
                            <span class="product_price headerfont"><?php echo $price_html; ?></span>
<?php endif; ?>

                    </div>

                </div>
            </a>
        </div>
    </div>

    <div class="woocommerce-message-right">

        <div class="buy">


            <?php
            /**
             * Reemplaza la variable global $product con el producto en oferta
             */
            global $product;
            $tmp_product = $product;
            $product = $product_remplazo;
            do_action('woocommerce_after_shop_loop_item');
            $product = $tmp_product;

            ?>
        </div>
        <?php

        /**
         * Consigue ls id de los productos en el carrito
         */
        global $woocommerce;
        $items = $woocommerce->cart->get_cart();
        $ids = array();
        foreach ($items as $item => $values) {
            $_product = $values['data']->post;
            $ids[] = $_product->ID;
        }
        /**
         * Se valida si el producto de promoción está en el carrito
         */
        if (!in_array($ids, $product_remplazo->id)):

        ?>
        <div class="buy">
            <?php do_action('woocommerce_shop_popup_item', $producto_original); ?>
        </div>
        <?php endif; ?>
        <!--        <div class="buy">
                    <span class="cerrar-popup">No, por ahora</span>
                </div>-->

    </div>

</div>



