<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>

<?php
//login y registro
do_action( 'woocommerce_before_checkout_form', $checkout ); ?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<div class="wc-checkout-left-panel">
		<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<div class="row">
			<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

			<?php if ( is_user_logged_in()  ) : ?>
			<div class="col-sm-12">
				<?php
				//formulario de facturacion
				do_action( 'woocommerce_checkout_billing' );
				//informacion de envio
				do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
			<?php endif ?>

			<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
		</div><!-- / row -->

		<?php endif; ?>
	</div>

	<div class="wc-checkout-right-panel">
		<div class="row">
			<div class="col-sm-12" id="customer_order">
				<h3 id="order_review_heading">
					<?php _e( 'Su orden', 'woocommerce' ); ?>
				</h3>

				<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				</div>

				<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
			</div>
		</div>
	</div><!-- / wc-checkout-right-panel -->

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
