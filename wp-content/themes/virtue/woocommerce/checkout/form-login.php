<?php
/**
 * Checkout login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() || 'no' === get_option( 'woocommerce_enable_checkout_login_reminder' ) ) {
	return;
}
?>
<div class="wc-checkout-left-panel">
	<div class="formulario-registro-login">
		<div class="row login-form-checkout">
			<h3>INGRESE</h3>
			<p>Por favor ingrese a su cuenta para realizar el pedido.</p>
			<?php echo do_shortcode('[clean-login]'); ?>
		</div>

		<div class="row login-form-checkout-registro">
			<h3>REGÍSTRESE</h3>
			<?php echo do_shortcode('[clean-login-register]'); ?>
		</div>
	</div>
</div>
