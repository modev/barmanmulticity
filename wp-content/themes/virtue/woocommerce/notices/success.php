<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $woocommerce, $product;

if (!$messages)
    return;
?>

<?php foreach ($messages as $message) : ?>

    <div class="woocommerce-message success">

        <div class="woocommerce-message-left">
            <?php echo wp_kses_post($message); ?>
            <?php if ($product) : ?>
                <p class="price"><?php //echo $product->get_price_html();         ?></p>
            <?php endif ?>
        </div>

        <?php if ($product) : ?>
            <div class="woocommerce-message-right">
                <header class="subtotal-mensaje-header">
                    <div class="subtotal-mensaje-heading">
                        <span class="subtotal">Subtotal: &nbsp;</span>
                        <span class="precio">
                            <?php echo $woocommerce->cart->get_cart_subtotal(); ?>
                        </span>
                    </div>
                    <div class="subtotal-mensaje-items">
                        <span><?php echo $woocommerce->cart->get_cart_contents_count(); ?></span> items en su Carrito
                    </div>
                </header>
                <footer class="subtotal-mensaje-footer">
                    <div class="copy">
                        <span class="icon-confianza"></span>
                        <span class="copy-texto">
                            Realice su orden con toda confianza
                        </span>
                    </div>

                    <div class="wc-message-botones">
                        <div class="boton-comprar">
                            <?php
                            if (sizeof($woocommerce->cart->cart_contents) > 0) :
                                echo '<a href="' . $woocommerce->cart->get_cart_url() . '" title="' . __('Checkout') . '">' . __('Comprar') . '</a>';
                            endif;
                            ?>
                        </div>
                        <div class="boton-tienda">
                            <a href="<?php echo get_permalink(4); ?>" title="tienda">Añadir más productos</a>
                        </div>
                    </div>

                </footer>
            </div>

            <?php $oferta = $product->get_cross_sells(); ?>

            <?php if ($oferta) : ?>
                <div class="woocommerce-message-footer">

                    <div class="cross-sells">



                        <?php set_query_var('producto_original', $product->id); ?>
                        <?php set_query_var('producto_oferta', reset($oferta)); ?>
                        <?php wc_get_template_part('content', 'product-oferta'); ?>


                    </div>
                </div>
            <?php endif ?>
        <?php endif ?>

    </div><!-- mensaje exitoso -->

<?php endforeach; ?>