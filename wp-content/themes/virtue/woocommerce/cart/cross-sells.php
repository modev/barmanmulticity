<?php
/**
 * Cross-sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cross-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $woocommerce_loop;

$crosssells = $producto_original->get_cross_sells();

if (0 === sizeof($crosssells))
    return;

$meta_query = WC()->query->get_meta_query();

$args = array(
    'p'=> (int) $crosssells[0],
    'post_type' => 'product',
    'ignore_sticky_posts' => 1,
    'no_found_rows' => 1,
    'posts_per_page' => apply_filters('woocommerce_cross_sells_total', $posts_per_page),
    'orderby' => $orderby,
    //'post__in' => array($crosssells),
    'meta_query' => $meta_query
    );

$products_cross = new WP_Query($args);

$woocommerce_loop['columns'] = apply_filters('woocommerce_cross_sells_columns', $columns);

if ($products_cross->have_posts()) :
    ?>

<div class="cross-sells">

    <?php woocommerce_product_loop_start(); ?>
    <?php while ($products_cross->have_posts()) : $products_cross->the_post(); 
    ?>
    <p>
        <?php
        print_r(get('texto_promocion'));    
        set_query_var('producto_original', $producto_original->id);
        wc_get_template_part('content', 'product-popup');

        endwhile; // end of the loop.  ?>

        <?php woocommerce_product_loop_end(); ?>
    </div>

    <?php
// Show the product's upsells
//    woocommerce_upsell_display();
    ?>

<?php endif; ?>

<?php
wp_reset_query();
