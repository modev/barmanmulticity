/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/////////////
/////////////
/////////////
/////////////YOUTUBE
/////////////
/////////////

jQuery(document).ready(function() {

  var playerInfoList = [];
  
  var players = new Array();

  jQuery("div.contenedor_video").each(function(i) {
    var id_video = jQuery(this).attr("data-id-yt");
    var cont_id = jQuery(this).attr("id");

    var img = jQuery('#' + cont_id + ' img');

    var width = img.width();
    var height = img.height();

    playerInfoList[id_video] = [{
        id: cont_id,
        videoId: id_video,
        height: height,
        width: width
      }];

  });

  var cargado_libreria = false;

  jQuery("div.contenedor_video").click(function() {
    var id_video = jQuery(this).attr("data-id-yt");

    if (cargado_libreria != true) {
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      cargado_libreria = true;
      console.log("se agrego libreria");
    }


    jQuery('div#video_' + id_video).html("<div id='player_" + id_video + "'></div>");

    videoPlayer(id_video);

  });
  
  function videoPlayer(id_video) {

    var info = playerInfoList[id_video];

    //hide and show divs
    var img = jQuery('#' + info[0].id + ' img');
    var width = img.width();
    var height = img.height();
    //jQuery('#' + info[0].id + ' .imagen').hide().width(width).height(height);

    // 3. This function creates an <iframe> (and YouTube player) after the API code downloads.
    var player;
    

    if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
      window.onYouTubePlayerAPIReady = function() {
        onYouTubePlayer();
      };
    } else {
      onYouTubePlayer();
    }

    function onYouTubePlayer() {
      if (typeof playerInfoList === 'undefined')
        return;

      createPlayer(info);
    }

    function createPlayer(playerInfo) {
      var info = playerInfo[0];

      player = new YT.Player('player_' + info.videoId, {
        width: info.width,
        height: info.height,
        videoId: info.videoId,
        playerVars: {'showinfo': 0},
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
      
      players.push(player);
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
      event.target.playVideo();
    }

    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING) {
        //alert(event.target.getVideoUrl());
        // alert(players[0].getVideoUrl());
        var temp = event.target.getVideoUrl();
        for (var i = 0; i < players.length; i++) {
          if (players[i].getVideoUrl() != temp) {
            players[i].stopVideo();
          }
        }
      }
    }

  }
});