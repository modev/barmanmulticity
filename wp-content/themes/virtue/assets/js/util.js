var VALIDATOR = (function () {

    var my = {};

    my.filterNumbers = function (evt) {
        //asignamos el valor de la tecla a keynum
        if (window.event) {// IE
            keynum = evt.keyCode;
        } else {
            keynum = evt.which;
        }

        //comprobamos si se encuentra en el rango n�merico
        if ((((keynum > 47 && keynum < 58) || (keynum > 95 && keynum < 106)) && evt.shiftKey == 0) || //numeros 
            keynum == 37 || //flecha atras
            keynum == 39 || //flecha adelante
            keynum == 8 || //backspace
            keynum == 9 //tab
            )
            return true;
        else
            return false;
    }

    my.filterAlphaNumerics = function (evt) {
        //asignamos el valor de la tecla a keynum
        if (window.event) // IE
            keynum = evt.keyCode;
        else
            keynum = evt.which;
        //comprobamos si se encuentra en el rango
        if ((((keynum > 47 && keynum < 58) || (keynum > 95 && keynum < 106)) && evt.shiftKey == 0) || //numeros
            keynum > 64 && keynum < 91 || //may�sculas
            keynum > 96 && keynum < 123 || //min�sculas
            keynum == 241 || keynum == 209 || //(�,�)
            keynum == 193 || keynum == 225 || //[�,�]
            keynum == 201 || keynum == 233 || //[�,�]
            keynum == 205 || keynum == 237 || //[�,�]
            keynum == 211 || keynum == 243 || //[�,�]
            keynum == 218 || keynum == 250 || //[�,�]
            keynum == 45 || keynum == 46 || //[.,-]
            keynum == 8 || //backspace
            keynum == 9 || //tab
            keynum == 37 || //flecha atras
            keynum == 39 || //flecha adelante
            keynum == 32 || //espacio
            keynum == 0)
            return true;
        else
            return false;
    }

    my.filterLetters = function (evt) {
        //asignamos el valor de la tecla a keynum
        if (window.event) // IE
            keynum = evt.keyCode;
        else
            keynum = evt.which;
        //comprobamos si se encuentra en el rango
        if (keynum > 64 && keynum < 91 || //may�sculas
            keynum > 111 && keynum < 123 || //min�sculas
            keynum == 241 || keynum == 209 || //(�,�)
            keynum == 193 || keynum == 225 || //[�,�]
            keynum == 201 || keynum == 233 || //[�,�]
            keynum == 205 || keynum == 237 || //[�,�]
            keynum == 211 || keynum == 243 || //[�,�]
            keynum == 218 || keynum == 250 || //[�,�]
            keynum == 45 || keynum == 46 || //[.,-]
            keynum == 8 || //backspace
            keynum == 9 || //tab
            keynum == 37 || //flecha atras
            keynum == 39 || //flecha adelante
            keynum == 32 || //espacio)
            keynum == 0)
            return true;
        else
            return false;
    }

    my.LimitChars = function (evt, max) {

        evento = window.event || evt;
        target = evento.target || evento.srcElement;

        //asignamos el valor de la tecla a keynum
        if (window.event) // IE
            keynum = evt.keyCode;
        else
            keynum = evt.which;

        if (document.getElementById(target.id).value.length < max ||
            keynum == 8 || //backspace
            keynum == 9 || //tab
            keynum == 46 || //suprimir
            keynum == 37 || //flecha atras
            keynum == 39) //flecha adelante
            return true;
        else
            return false;
    }

    my.ValidateCedula = function (cedula) {

        cedula = cedula.trim();
        //Preguntamos si la cedula consta de 10 digitos

        if (cedula.length == 10) {

            //Obtenemos el digito de la region que sonlos dos primeros digitos
            var digito_region = cedula.substring(0, 2);

            //Pregunto si la region existe ecuador se divide en 24 regiones
            if (digito_region >= 1 && digito_region <= 24) {
                return true;
            } else {
                // Esta cedula no pertenece a ninguna region
                return false;
            }
        } else {
            //Esta cedula tiene menos de 10 Digitos
            if(cedula.length < 6)
                return false;
            else
                return true;
        }
    }

    my.ValidateEmail = function ( email ) {        
        expr = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*[.]([a-z]{2,3})$/;
        if ( expr.test(email) )
            return true;
        else
            return false;
    }   

    return my;
} ());