<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              mocionsoft.com
 * @since             1.0.0
 * @package           Mocion_show_promotions
 *
 * @wordpress-plugin
 * Plugin Name:       mocion_show_promotions
 * Plugin URI:        http://mocionsoft.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            wittolfr
 * Author URI:        mocionsoft.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mocion_show_promotions
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mocion_show_promotions-activator.php
 */
function activate_mocion_show_promotions() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mocion_show_promotions-activator.php';
	Mocion_show_promotions_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mocion_show_promotions-deactivator.php
 */
function deactivate_mocion_show_promotions() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mocion_show_promotions-deactivator.php';
	Mocion_show_promotions_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_mocion_show_promotions' );
register_deactivation_hook( __FILE__, 'deactivate_mocion_show_promotions' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-mocion_show_promotions.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_mocion_show_promotions() {

	$plugin = new Mocion_show_promotions();
	$plugin->run();

}
run_mocion_show_promotions();
