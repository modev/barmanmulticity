
	jQuery(document).on("ready", function() {
		var date = new Date();
		jQuery(".date").datepicker({ 
			defaultDate:null,
			changeMonth: true,
			changeYear: true,
			yearRange: date.getFullYear() - 120 + ":" + date.getFullYear(),
			dateFormat: "yy-mm-dd",
			currentText: "now",
			
		    maxDate: new Date(1997, 6, 30),

		}); 
	});

	function validate_register(el){
		
		var ok = true;

		jQuery('span.error').empty();
                
                if(jQuery('#register_nombre input').val() == "") {
			jQuery('#register_nombre span.error').text("(*) Ingrese un nombre.");
			ok = false;
		}
                
                if(jQuery('#register_apellido input').val() == "") {
			jQuery('#register_apellido span.error').text("(*) Ingrese un apellido.");
			ok = false;
		}

		if(!VALIDATOR.ValidateEmail(jQuery('#register_email input').val().trim()) ) {
			jQuery('#register_email span.error').text("(*) Ingrese un correo válido");
			ok = false;
		}

		if(!validateAge(jQuery('#register_birth input').val()) ) {
			jQuery('#register_birth span.error').text("(*) Solo pueden registrarse personas mayores de edad.");
			ok = false;
		}

		if(jQuery('#register_birth input').val().length < 10) {
			jQuery('#register_birth span.error').text("(*) Ingrese su fecha de nacimiento");
			ok = false;
		}

		if(jQuery('#register_password1 input').val().length < 6) {
			jQuery('#register_password1 span.error').text("(*) Ingrese una contraseña de al menos 6 dígitos");
			ok = false;
		}

		if(jQuery('#register_password2 input').val().length < 6) {
			jQuery('#register_password2 span.error').text("(*) Confirme su contraseña");
			ok = false;
		}

		if(jQuery('#register_password1 input').val() != jQuery('#register_password2 input').val()) {
			jQuery('#register_password1 span.error,#register_password2 span.error').text("(*) Las contraseñas no coinciden");
			ok = false;
		}
                
		if(ok) {
			var navegador = navigator.userAgent;
			 if (navigator.userAgent.indexOf('MSIE') !=-1) {
			   return true;
			 } else {
				el.form.submit();
			 }	
			el.disabled = true;
		} else {
			return false;
		}
	}

	function validateAge(date) {
  
		var cDate = new Date();
		var cYear = cDate.getFullYear();
		var cMonth = cDate.getMonth()+1;
		var cDay = cDate.getDate();

		var year = parseInt(date.substring(6,12));
		var month = parseInt(date.substring(3,5));
		var day = parseInt(date.substring(0,2));
		  
		if((cYear - year) == 18) {
			if((cMonth - month) == 0) {
				if((cDay - day) >= 0)
					return true 
				else
					return false;
			} else { 
				if((cMonth - month) > 0)
					return true 
				else 
					return false;
			}
		} else {
			if((cYear - year) > 18) 
				return true;
			else 
				return false;
		}
	}
