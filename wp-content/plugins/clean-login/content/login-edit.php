<?php
global $current_user;
get_currentuserinfo();
?>

<div class="cleanlogin-container cleanlogin-full-width">
    <form class="cleanlogin-form" method="post" action="#">

        <input type="hidden" name="email" value="<?php echo $current_user->user_email; ?>">

        <h4><?php echo __('Change password', 'cleanlogin'); ?></h4>

        <p class="cleanlogin-form-description"><?php echo __("If you would like to change the password type a new one. Otherwise leave this blank.", 'cleanlogin'); ?></p>

        <fieldset>

            <div id="register_password1"  class="cleanlogin-field">
                <label for="pass1"><?php echo __('New password', 'cleanlogin'); ?>*</label>
                <input type="password" name="pass1" value="" autocomplete="off" maxlength="30">
                <span class="error"></span>
            </div>

            <div id="register_password2"  class="cleanlogin-field">
                <label for="pass2"><?php echo __('Confirm password', 'cleanlogin'); ?>*</label>
                <input type="password" name="pass2" value="" autocomplete="off" maxlength="30">
                <span class="error"></span>
            </div>

        </fieldset>

        <div>	
            <input type="submit" value="<?php echo __('Update profile', 'cleanlogin'); ?>" name="submit" onclick="return validate_register(this);">
            <input type="hidden" name="action" value="edit">		
        </div>

        <p class="cleanlogin-form-description">(*) Los campos marcados con asterisco son obligatorios.</p>

    </form>
</div>


<?php
//wp_enqueue_script( 'register-form', get_template_directory_uri() . '/assets/js/util.js'); 
wp_enqueue_script('register-form-utils', get_bloginfo('template_directory') . '/assets/js/util.js');
//wp_enqueue_script( 'register-form-custom', plugin_dir_url(__FILE__).'custom-clean-login.js');  
?>
