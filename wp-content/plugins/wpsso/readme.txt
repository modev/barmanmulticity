=== WordPress Social Sharing Optimization - Improves Shared Content on Social Websites ===
Contributors: jsmoriss
Donate Link: https://surniaulula.com/extend/plugins/wpsso/
Tags: nextgen gallery, featured, attached, open graph, meta tags, facebook, google, google+, g+, twitter, linkedin, social, seo, pinterest, rich pins, multilingual, object cache, transient cache, wp_cache, nggalbum, nggallery, singlepic, imagebrowser, gallery, twitter cards, photo card, gallery card, player card, summary card, easy digital downloads, woocommerce, marketpress, e-commerce, multisite, hashtags, bbpress, buddypress, jetpack, photon, slideshare, vimeo, wistia, youtube, polylang
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.txt
Requires At Least: 3.0
Tested Up To: 4.1
Stable Tag: 2.7.5.4

Make sure social websites present your content correctly, no matter how your webpage is shared - from buttons, browser add-ons, or pasted URLs.

== Description ==

<table><tr><td valign="top">
<p><img src="https://ps.w.org/wpsso/assets/icon-256x256.png?rev=" width="256" height="256" style="max-width:none;margin-right:40px;" /></p>
</td><td valign="top">

<p><strong>Make sure social websites present your content in the best possible way, no matter <em>how</em> your webpage is shared</strong> &mdash; from sharing buttons on the webpage, browser add-ons and extensions, or URLs pasted directly on social websites.</p>

= Advanced Social Management =

<p>WordPress Social Sharing Optimization (WPSSO) gives you <strong>total control over the information social websites need</strong>, improving Google Search ranking, social engagement, and click-through-rates on Facebook, Google+, Twitter, Pinterest, LinkedIn, StumbleUpon, Tumblr and and many more &mdash; no sharing buttons required!</p>

</td></tr></table>

<p><strong>Already have social sharing buttons?</strong> No problem! Sharing buttons <em>only submit URLs to social websites</em> &mdash; WPSSO can then provide everything those social websites need to know about your shared content. Use WPSSO by itself, or in combination with any of your favorite social sharing buttons.</p>

<p><blockquote>
<h4>Industry Reviews</h4>

<p>"<em>If you want to make every tweet, like, share, pin, and +1 count, then you should definitely start using WPSSO on your WordPress site.</em>" &mdash; <a href="http://www.indexwp.com/wordpress-social-sharing-optimization-pro-review/">indexwp.com</a></p>

<p>"<em>I’m now a customer. This plugin is invaluable and a real time saver. The fact that it won’t slow my site down either is something that also sold me on it.</em>" &mdash; <a href="http://www.wpkube.com/wpsso-plugin-review/">wpkube.com</a></p>

<p>"<em>Wherever you are at with the social media marketing strategy for your site, the WP Social Sharing Optimization plugin can help you get better results.</em>" &mdash; <a href="http://wplift.com/wordpress-social-sharing-optimization">wplift.com</a></p>

<p>"<em>If you want to get the most out of the social shares your content receives, ensure your content stands out when it is posted on the various social networks, and listed in the search engines, then this plugin is a great choice that covers all the bases.</em>" &mdash; <a href="http://www.wpmayor.com/wordpress-social-sharing-optimization-pro-plugin-review/">wpmayor.com</a></p>

<p>"<em>WPSSO Pro provides a handy tool for your marketing team, and improves your content whenever it’s shared by you or your fans. The free version has a lot of great features and makes a good trial version, but I think that eCommerce sites really benefit from the eCommerce plugin and video integrations (as well as the inclusion of Twitter card support) that the Pro version offers.</em>" &mdash; <a href="http://www.sellwithwp.com/optimized-social-media-wpsso-plugin-review/">sellwithwp.com</a></p>
</blockquote></p>

<p><blockquote>
<h4>Articles and Tutorials</h4>

<ul>
<li><a href="http://www.wpsuperstars.net/optimize-content-for-social-networks/">How To Optimize Your Content For Social Networks Using WordPress</a></li>
<li><a href="http://surniaulula.com/2014/10/25/social-seo-wordpress-seo-vs-wpsso/">Social SEO: WordPress SEO by Yoast vs WPSSO</a></li>
</ul>
</blockquote></p>

<p>You can download the <a href="https://wordpress.org/plugins/wpsso/">Free version of WPSSO on WordPress.org</a> and <a href="http://surniaulula.com/extend/plugins/wpsso/">purchase Pro license(s) on SurniaUlula.com</a> (includes a <em>No Risk 30 Day Refund Policy</em>).</p>

= Quick List of Features =

**Free / Basic Version**

* Adds Open Graph / Rich Pin meta tags (Facebook, Google+, Pinterest, LinkedIn, etc.).
* Configurable image sizes for Open Graph (Facebook, LinkedIn, etc.) and Pinterest.
* Optional fallback to a default image and video for index and search webpages.
* Supports featured, attached, gallery shortcode, and/or HTML image tags in content.
* Validates image dimensions to provide accurate media for social websites.
* Auto-regeneration of inaccurate / missing WordPress image sizes.
* Fully renders content (including shortcodes) for accurate description texts.
* Configurable title separator character (hyphen by default).
* Support of WordPress and/or SEO titles for Posts and Pages.
* Includes author and publisher profile URLs for Facebook and Google Search.
* Includes hashtags from Post and Page WordPress tags.
* Includes the author's name for Pinterest Rich Pins.
* Includes a Google / SEO description meta tag if a known SEO plugin is not detected.
* Fallback to the image alt value if the content and except do not include any text.
* Provides Facebook, Google+ and Twitter URL profile contact fields.
* Validation tools and special meta tag preview tabs on admin edit pages.
* Customizable *multilingual* Site Title and Site Description texts.
* Contextual help for *every* plugin option and [comprehensive online documentation](http://surniaulula.com/codex/plugins/wpsso/).
* Uses object and transient caches to provide incredibly fast execution speeds.
* **Additional free extension plugins for WPSSO**:
	* [App Meta](https://wordpress.org/plugins/wpsso-am/) for Apple Store / iTunes and Google Play App meta tags.
	* [Place and Location Meta](https://wordpress.org/plugins/wpsso-plm/) for Facebook *Location* and Pinterest *Place* Rich Pin meta tags.
	* [Social Sharing Buttons](https://wordpress.org/plugins/wpsso-ssb/) for fast and accurate social sharing buttons.

**Pro / Power-User Version**

* **No Risk 30 Day Refund Policy**
* Twitter Card meta tags:
	* *Product Card* &mdash; Product information from an e-commerce plugin.
	* *Player Card* &mdash; An embedded video in the Post / Page content.
	* *Gallery Card* &mdash; A Media Library gallery or NextGEN Gallery shortcode.
	* *Photo Card* &mdash; An Attachment page or NextGEN Gallery ImageBrowser webpage.
	* *Large Image Summary Card* &mdash; An image from the custom Social Settings, Featured, Attached, or NextGEN Gallery singlepic.
	* *App Card* &mdash; Apple Store iPhone / iPad or Google Play App (requires the [WPSSO App Meta](https://wordpress.org/plugins/wpsso-am/) extension).
	* *Summary Card* &mdash; All other webpages.
* Customizable image dimensions for each Twitter Card type.
* Configurable title and description lengths for different contexts (Open Graph, Twitter Card, SEO).
* Additional profile contact fields with configurable label and field names.
* Custom settings and meta tag values for each Post, Page, and custom post type.
* Options to exclude specific Google / SEO, Open Graph, and Twitter Card meta tags.
* Support for embedded videos in iframe and object HTML tags.
* **Integrates with 3rd party plugins and services for additional image, video, product, and content information** (see [About Pro Modules](http://surniaulula.com/codex/plugins/wpsso/notes/modules/) and [Integration Notes](http://surniaulula.com/codex/plugins/wpsso/installation/integration/) for details). The following modules are included with the Pro version, and are automatically loaded if/when the supported plugins and/or services are detected.
	* Plugins
		* All in One SEO Pack
		* bbPress
		* BuddyPress
		* Easy Digital Downloads
		* HeadSpace2 SEO
		* JetPack Photon
		* NextGEN Gallery
		* MarketPress - WordPress eCommerce
		* Polylang
		* WooCommerce
		* WordPress SEO by Yoast
		* WP e-Commerce
	* Service APIs
		* Gravatar Images
		* Slideshare Presentations
		* Vimeo Videos
		* Wistia Videos
		* YouTube Videos and Playlists

**Looking for the Pro version?** You can [purchase Pro license(s) here](http://surniaulula.com/extend/plugins/wpsso/) to update the Free version quickly and easily.

= 3rd Party Integration =

Aside from the additional support for Twitter Cards, the main difference between the Free and Pro versions is the integration of 3rd party plugins and services.

**Images and Videos**

WPSSO detects and uses all images - associated or included in your Post or Page content - including WordPress image galleries. WordPress Media Library images (and NextGEN Gallery in the Pro version) are resized according to their intended audience (Facebook, Twitter, Pinterest, etc). The Pro version also detects embedded videos from Slideshare, Vimeo, Wistia, and Youtube (including their preview images).

WPSSO (Pro version) also includes support for [JetPack Photon](http://jetpack.me/support/photon/) and [NextGEN Gallery v1 and v2](https://wordpress.org/plugins/nextgen-gallery/) albums, galleries and images (shortcodes, image tags, album / gallery preview images, etc.).

**Enhanced SEO**

WPSSO (Pro version) integrates with [WordPress SEO by Yoast](https://wordpress.org/plugins/wordpress-seo/), [All in One SEO Pack](https://wordpress.org/plugins/all-in-one-seo-pack/), and [HeadSpace2 SEO](https://wordpress.org/plugins/headspace2/) &mdash; making sure your custom SEO settings are reflected in the Open Graph, Rich Pin, and Twitter Card meta tags.

**eCommerce Products**

WPSSO (Pro version) also supports [Easy Digital Downloads](https://wordpress.org/plugins/easy-digital-downloads/), [MarketPress - WordPress eCommerce](https://wordpress.org/plugins/wordpress-ecommerce/), [WooCommerce v1 and v2](https://wordpress.org/plugins/woocommerce/), and [WP e-Commerce](https://wordpress.org/plugins/wp-e-commerce/) product pages, creating appropriate meta tags for [Facebook Products](https://developers.facebook.com/docs/payments/product/), [Twitter Product Cards](https://dev.twitter.com/docs/cards/types/product-card) and [Pinterest Rich Pins](http://developers.pinterest.com/rich_pins/), including variations and additional / custom images.

**Forums and Social**

WPSSO (Pro version) supports [bbPress](https://wordpress.org/plugins/bbpress/) and [BuddyPress](https://wordpress.org/plugins/buddypress/) (see the [BuddyPress Integration Notes](http://surniaulula.com/codex/plugins/wpsso/notes/buddypress-integration/)), making sure your meta tags reflect the page content, including appropriate titles, descriptions, images, etc.

= Custom Contacts =

WPSSO (Pro version) allows you to customize the field names, label, and show/remove the following contacts from the user profile page:

* AIM
* Facebook 
* Google+ 
* Jabber / Google Talk
* LinkedIn 
* Pinterest 
* Skype 
* Tumblr 
* Twitter 
* Yahoo IM
* YouTube

= Complete Meta Tags =

WPSSO adds Facebook / [Open Graph](http://ogp.me/), [Pinterest Rich Pins](http://developers.pinterest.com/rich_pins/), [Twitter Cards](https://dev.twitter.com/docs/cards), and [Search Engine Optimization](http://en.wikipedia.org/wiki/Search_engine_optimization) meta tags to the head section of webpages. These meta tags are used by Google Search and most social websites to describe and display your content correctly (title, description, hashtags, images, videos, product, author profile / authorship, publisher, etc.). WPSSO uses the *existing* content of your webpages to build HTML meta tags &mdash; There's no need to manually enter / configure any additional values or settings (although many settings and options *are* available). <a href="http://surniaulula.com/extend/plugins/wpsso/screenshots/">See examples from Google Search, Google+, Facebook, Twitter, Pinterest, StumbleUpon, Tumblr, etc.</a> &mdash; along with screenshots of the WPSSO settings pages.

WPSSO (Pro version) provides the [Summary](https://dev.twitter.com/docs/cards/types/summary-card), [Large Image Summary](https://dev.twitter.com/docs/cards/large-image-summary-card), [Photo](https://dev.twitter.com/docs/cards/types/photo-card), [Gallery](https://dev.twitter.com/docs/cards/types/gallery-card), [Player](https://dev.twitter.com/docs/cards/types/player-card) and [Product](https://dev.twitter.com/docs/cards/types/product-card) Twitter Cards &mdash; *including configurable image sizes for each card type*.

* **Google / SEO Link and Meta Tags**
	* author
	* description
	* publisher
* **Facebook Meta Tags**
	* fb:admins
	* fb:app_id
* **Open Graph / Rich Pin Meta Tags**
	* article:author
	* article:publisher
	* article:published_time
	* article:modified_time
	* article:section
	* article:tag
	* og:description
	* og:image
	* og:image:secure_url
	* og:image:width
	* og:image:height
	* og:locale
	* og:site_name
	* og:title
	* og:type
	* og:url
	* og:video
	* og:video:secure_url
	* og:video:width
	* og:video:height
	* og:video:type
	* product:price:amount
	* product:price:currency
	* product:availability
* **Schema Meta Tags**
	* description
	* image
	* url
* **Twitter Card Meta Tags** (Pro version)
	* twitter:card (Summary, Large Image Summary, Photo, Gallery, Player and Product)
	* twitter:creator
	* twitter:data1
	* twitter:data2
	* twitter:data3
	* twitter:data4
	* twitter:description
	* twitter:image
	* twitter:image:width
	* twitter:image:height
	* twitter:image0
	* twitter:image1
	* twitter:image2
	* twitter:image3
	* twitter:label1
	* twitter:label2
	* twitter:label3
	* twitter:label4
	* twitter:player
	* twitter:player:width
	* twitter:player:height
	* twitter:site
	* twitter:title

= Proven Performance =

WPSSO is *fast and coded for performance*, making full use of all available caching techniques (persistent / non-persistent object and disk caching). WPSSO loads only the library files and object classes it needs, keeping it small, fast, and yet still able to support a wide range of 3rd party integration features.

<blockquote>
<h4>How Fast is WPSSO?</h4>

<p>Very Fast. A few example metrics from the <a href="https://wordpress.org/plugins/p3-profiler/">P3 (Plugin Performance Profiler)</a> plugin, using <a href="http://wptest.io/">WP Test Data</a>, and the default settings of several popular plugins, along with the WordPress Core and Twenty Fourteen theme for reference:</p>

<ul>
	<li><strong><font color="blue">0.0050</font></strong> secs (5.0 ms) - WPSSO Social Sharing Buttons (SSB) v1.1</li>
	<li><strong><font color="blue">0.0116</font></strong> secs (11.6 ms) - <em><strong>WordPress Social Sharing Optimization (WPSSO) v2.7.1</strong></em></li>
	<li><strong><font color="blue">0.0149</font></strong> secs (14.9 ms) - All in One SEO Pack v2.2.1</li>
	<li><strong><font color="blue">0.0157</font></strong> secs (15.7 ms) - MarketPress - WordPress eCommerce v2.9.2.1 (No Products)</li>
	<li><strong><font color="blue">0.0186</font></strong> secs (18.6 ms) - NextGEN Facebook (NGFB) v7.7.1</li>
	<li><strong><font color="green">0.0230</font></strong> secs (23.0 ms) - Easy Digital Downloads v1.9.8 (No Products)</li>
	<li><strong><font color="green">0.0254</font></strong> secs (25.4 ms) - MailPoet Newsletters (aka Wysija Newsletters) v2.6.9</li>
	<li><strong><font color="green">0.0322</font></strong> secs (32.2 ms) - WP e-Commerce v3.8.13.3 (No Products)</li>
	<li><strong><font color="green">0.0345</font></strong> secs (34.5 ms) - <strong>WordPress Twenty Fourteen Theme v1.1</strong></li>
	<li><strong><font color="green">0.0393</font></strong> secs (39.3 ms) - bbPress v2.5.3 (No Forums or Topics)</li>
	<li><strong><font color="green">0.0425</font></strong> secs (42.5 ms) - WooCommerce v2.1.5 (No Products)</li>
	<li><strong><font color="green">0.0488</font></strong> secs (48.8 ms) - <strong>WordPress Core v3.9.1</strong></li>
	<li><strong><font color="orange">0.0572</font></strong> secs (57.2 ms) - SEO Ultimate v7.6.2</li>
	<li><strong><font color="orange">0.0579</font></strong> secs (57.9 ms) - Facebook v1.5.5</li>
	<li><strong><font color="orange">0.0656</font></strong> secs (65.6 ms) - BuddyPress v1.9.2 (No Activities)</li>
	<li><strong><font color="red">0.1055</font></strong> secs (105.5 ms) - WordPress SEO by Yoast v1.5.4.2</li>
	<li><strong><font color="red">0.1980</font></strong> secs (198.0 ms) - JetPack by WordPress.com v2.9.2</li>
	<li><strong><font color="red">0.2085</font></strong> secs (208.5 ms) - NextGEN Gallery by Photocrati v2.0.66 (No Galleries or Images)</li>
</ul>

<p><small><em>Benchmarks were executed on dedicated hardware, using an APC opcode/object cache, WordPress v3.9.1, and P3 v1.5.1 configured with opcode optimization enabled (improves accuracy).</em></small></p>
</blockquote>

= Clean Uninstall =

Try the WPSSO plugin with complete confidence &mdash; when uninstalled, WPSSO removes *all* traces of itself from the database (options, site options, user and post meta, transients, etc.).

= Great Support =

WPSSO support and development is on-going. You can review the [FAQ](http://faq.wpsso.surniaulula.com/) and [Notes](http://notes.wpsso.surniaulula.com/) pages for additional setup information. If you have any suggestions or comments, post them to the [WordPress support forum](https://wordpress.org/support/plugin/wpsso) or the [Pro version support website](http://support.wpsso.surniaulula.com/).

**Follow Surnia Ulula on [Google+](https://plus.google.com/+SurniaUlula/?rel=author), [Facebook](https://www.facebook.com/SurniaUlulaCom), and [Twitter](https://twitter.com/surniaululacom), and [YouTube](http://www.youtube.com/user/SurniaUlulaCom)**.

== Installation ==

= Install and Uninstall =

<ul>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation/developer-special-buy-one-get-one-free/">Developer Special – Buy one, Get one Free</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation/migrate-from-ngfb/">How-To Migrate from NGFB to WPSSO</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation-to/install-the-plugin/">Install the Plugin</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation/integration/">Integration Notes</a>
	<ul>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation/integration/buddypress-integration/">BuddyPress Integration</a></li>
	</ul></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation/debugging-and-problem-solving/">Debugging and Problem Solving</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation/uninstall-the-plugin/">Uninstall the Plugin</a></li>
</ul>

= Plugin Setup =

<ul>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/installation/a-setup-guide/">A Setup Guide for WPSSO</a></li>
</ul>

== Frequently Asked Questions ==

= Frequently Asked Questions =

<ul>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/can-i-use-the-pro-version-on-multiple-websites/">Can I use the Pro version on multiple websites?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/does-linkedin-read-the-open-graph-meta-tags/">Does LinkedIn read the Open Graph meta tags?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/doesnt-an-seo-plugin-cover-that/">Doesn’t an SEO plugin cover that?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-can-i-exclude-ignore-certain-parts-of-the-content-text/">How can I exclude / ignore certain parts of the content text?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-can-i-fix-a-err_too_many_redirects-error/">How can I fix a ERR_TOO_MANY_REDIRECTS error?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-can-i-see-what-facebook-sees/">How can I see what Facebook sees?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-can-i-share-a-single-nextgen-gallery-image/">How can I share a single NextGEN Gallery image?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-do-i-attach-an-image-without-showing-it-on-the-webpage/">How do I attach an image without showing it on the webpage?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-do-i-fix-my-themes-front-page-pagination/">How do I fix my theme’s front page pagination?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-do-i-install-the-wpsso-pro-version/">How do I install the WPSSO Pro version?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/how-does-wpsso-find-detect-select-images/">How does WPSSO find / detect / select images?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/w3c-says-there-is-no-attribute-property/">W3C says “there is no attribute ‘property’”</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/what-about-google-search-and-google-plus/">What about Google Search and Google Plus?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/what-features-of-nextgen-gallery-are-supported/">What features of NextGEN Gallery are supported?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/what-is-the-difference-between-ngfb-and-wpsso/">What is the difference between NGFB and WPSSO?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/what-is-the-difference-between-the-free-and-pro-versions/">What is the difference between the Free and Pro versions?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-arent-pins-from-my-website-posting-rich/">Why aren’t Pins from my website posting Rich?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-do-my-facebook-shares-have-small-images/">Why do my Facebook shares have small images?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-does-facebook-play-videos-instead-of-linking-them/">Why does Facebook play videos instead of linking them?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-does-google-structured-data-testing-tool-show-errors/">Why does Google Structured Data Testing Tool show errors?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-does-wpsso-ignore-some-img-html-tags/">Why does WPSSO ignore some &lt;img/&gt; HTML tags?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-doesnt-facebook-show-the-correct-image/">Why doesn’t Facebook show the correct image?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-dont-my-twitter-cards-show-on-twitter/">Why don’t my Twitter Cards show on Twitter?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-is-the-open-graph-title-the-same-for-every-webpage/">Why is the Open Graph title the same for every webpage?</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/faq/why-is-the-page-blank-or-its-components-misaligned/">Why is the page blank or its components misaligned?</a></li>
</ul>

== Other Notes ==

<ul>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/modules/">About Pro Modules</a>
	<ul>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/modules/author-gravatar/">Author Gravatar</a></li>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/modules/easy-digital-downloads/">Easy Digital Downloads</a></li>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/modules/headspace2-seo/">HeadSpace2 SEO</a></li>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/modules/jetpack-photon/">Jetpack Photon</a></li>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/modules/slideshare-vimeo-wistia-youtube-apis/">Slideshare, Vimeo, Wistia, Youtube APIs</a></li>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/modules/woocommerce/">WooCommerce</a></li>
	</ul></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/contact-information/">Contact Information and Feeds</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/">Developer Resources</a>
	<ul>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/constants/">Constants</a></li>
		<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/">Filters</a>
		<ul>
			<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/examples/">Examples</a>
			<ul>
				<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/examples/add-availability-to-product-card-for-woocommerce/">Add Availability to Product Card for WooCommerce</a></li>
				<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/examples/detect-youtube-url-links-as-videos/">Detect YouTube URL Links as Videos</a></li>
				<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/examples/disable-the-social-settings-metabox-by-post-id/">Disable the Social Settings Metabox by Post ID</a></li>
				<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/examples/force-wistia-videos-to-autoplay-on-facebook/">Force Wistia Videos to Autoplay on Facebook</a></li>
				<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/examples/modify-the-default-topics-list/">Modify the Default Topics List</a></li>
				<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/examples/modify-the-home-page-title-for-facebook-open-graph/">Modify the Home Page Title for Facebook / Open Graph</a></li>
			</ul></li>
			<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/head/">Head Filters</a></li>
			<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/media/">Media Filters</a></li>
			<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/open-graph/">Open Graph Filters</a></li>
			<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/twitter-card/">Twitter Card Filters</a></li>
			<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/developer/filters/webpage/">Webpage Filters</a></li>
		</ul></li>
	</ul></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/inline-variables/">Inline Variables</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/multisite-network-support/">Multisite / Network Support</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/performance-tuning/">Performance Tuning</a></li>
	<li><a href="http://surniaulula.com/codex/plugins/wpsso/notes/working-with-image-attachments/">Working with Image Attachments</a></li>
</ul>

== Screenshots ==

01. The Plugin General Settings Page
02. The Plugin Advanced Settings Page
03. The Plugin Social Settings on Posts and Pages
04. An Example Facebook Link Share
05. An Example Facebook Video Share
06. An Example Google+ Link Share
07. An Example Google+ Video Share
08. An Example Google Search Result showing Author Profile Info
09. An Example LinkedIn Share
10. An Example Pinterest Image Pin
11. An Example Pinterest Product Pin
12. An Example Pinterest Product Pin (Zoomed)
13. An Example StumbleUpon Share
14. An Example Tumblr 'Link' Share
15. An Example Tumblr 'Photo' Share
16. An Example Tumblr 'Video' Share
17. An Example Twitter 'Summary' Card
18. An Example Twitter 'Large Image Summary' Card
19. An Example Twitter 'Photo' Card
20. An Example Twitter 'Gallery' Card
21. An Example Twitter 'Product' Card
22. An Example Twitter 'Player' Card

== Changelog ==

<blockquote>
<p>New versions of the plugin are released approximately every week (more or less). 
New features are added, tested, and released incrementally, instead of grouping them together in a major version release. 
When minor bugs fixes and/or code improvements are applied, new versions are also released.
This release schedule keeps the code stable and reliable, at the cost of more frequent updates.</p>
</blockquote>

= Version 2.7.5.4 =

* **Bugfixes**
	* Added a test for a valid image ID when checking for featured images.
* **Improvements**
	* Added a `wp_attachment_is_image()` check for attachment page media.
	* Added support for WordPress SEO's `WPSEO_Taxonomy_Meta::get_term_meta()` static class method (Pro version).
* **New Features**
	* *None*

= Version 2.7.5.2 =

* **Bugfixes**
	* Added a extra check for the attachment post type, in order to override the featured image and "published" post status checks.
	* Fixed an empty array test for attachment images that would caused the plugin to always return the default image.
* **Improvements**
	* *None*
* **New Features**
	* *None*

= Version 2.7.5.1 =

* **Bugfixes**
	* *None*
* **Improvements**
	* Added an extra sanity check for empty media arrays returned by the YouTube API XML (Pro version).
* **New Features**
	* *None*

= Version 2.7.5 =

* **Bugfixes**
	* *None*
* **Improvements**
	* Refactored code to rename the $addons array variable to $mods, in order to clarify that all 'modules' are included in the Pro version.
	* When checking for duplicate meta tags, added extra validation to remove our own meta tags in cases where a caching plugin or webserver is badly configured (ie. query arguments are ignored or removed).
* **New Features**
	* *None*

== Upgrade Notice ==

= 2.7.5.4 =

Added a test for a valid image ID when checking for featured images, and a wp_attachment_is_image() check for attachment page media.

= 2.7.5.2 =

Added checks for an "attachment" page, to use the information for the attachment itself, and not the attached post/page.

