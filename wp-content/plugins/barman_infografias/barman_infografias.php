<?php
/**
 * Plugin Name: Barman Infografias
 * Description: Create custom type infografias
 * Version: 1.0
 * Author: Mocion
 */

// Add Shortcode
function infografias_shortcode($attr) {

    $destination = wp_upload_dir();
    $destination_path = $destination['baseurl'];
    
    $height = isset($attr["alto"]) ? $attr["alto"] : "500px";
    $nameindex = isset($attr["index"]) ? $attr["index"] : "index";
    $namezip = isset($attr["zip"]) ? $attr["zip"] : "ejemplo";

    $uri = $destination_path . '/infografias/' . $namezip . '/' . $nameindex . '.html';

    $html = '<div><iframe src="' . $uri . '" width="100%" height="'.$height.'" frameborder="0" scrolling="no"></iframe></div>';

    return $html;
}

add_shortcode('infografias', 'infografias_shortcode');




function custom_post_type_infografias() {

  // Set UI labels for Custom Post Type
  $labels = array(
    'name'                => _x( 'Infografias', 'Post Type General Name', 'twentythirteen' ),
    'singular_name'       => _x( 'Infografia', 'Post Type Singular Name', 'twentythirteen' ),
    'menu_name'           => __( 'Infografias', 'twentythirteen' ),
    'parent_item_colon'   => __( 'Parent Infografia', 'twentythirteen' ),
    'all_items'           => __( 'Todas los Infografias', 'twentythirteen' ),
    'view_item'           => __( 'Ver Infografia', 'twentythirteen' ),
    'add_new_item'        => __( 'Agregar nuevo Infografia', 'twentythirteen' ),
    'add_new'             => __( 'Agregar Nuevo', 'twentythirteen' ),
    'edit_item'           => __( 'Editar Infografia', 'twentythirteen' ),
    'update_item'         => __( 'Adtualizar Infografia', 'twentythirteen' ),
    'search_items'        => __( 'Buscar Infografia', 'twentythirteen' ),
    'not_found'           => __( 'Not Found', 'twentythirteen' ),
    'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
  );
	
  // Set other options for Custom Post Type
	
  $args = array(
    'label'               => __( 'infografias', 'twentythirteen' ),
    'description'         => __( 'Infografia news and reviews', 'twentythirteen' ),
    'labels'              => $labels,
    // Features this CPT supports in Post Editor
    'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
    /* A hierarchical CPT is like Pages and can have
    * Parent and child items. A non-hierarchical CPT
    * is like Posts.
    */	
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'taxonomies' => array('category'), 
  );
	
  // Registering your Custom Post Type
  register_post_type( 'infografias', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
add_action( 'init', 'custom_post_type_infografias', 0 );


/**
 * descomprime el zip en la carpeta de uploads despues que se suba para las infografias
 * @param type $content
 * @return type
 */
function unzip_files_infografias($content) {

    $post_meta = get_post_meta(get_the_ID());

    if (isset($post_meta["zip"])) {

        $filename = $post_meta["zip"][0];

        WP_Filesystem();

        $destination = wp_upload_dir();
        $destination_path = $destination['basedir'];
        $origen = $destination["basedir"];
        $origen = str_replace("uploads", "files_mf", $origen);
        $unzipfile = unzip_file($origen . '/' . $filename, $destination_path . '/infografias');
    }

    return $content;
    //return str_replace( "\r<figcaption>", "<figcaption>", $content );
}

add_filter('content_save_pre', 'unzip_files_infografias', 10, 1);