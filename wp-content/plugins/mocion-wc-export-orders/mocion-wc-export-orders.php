<?php
/*
  Plugin Name: Mocion WC Export Orders
  Plugin URI: http://www.imaginate-solutions.com/
  Description: This plugin lets store owners to export orders. Basado en woocommerce-export-orders y personalizado por MOCION
  Version: 0.1
  Author: Dhruvin Shah
  Author URI: http://www.imaginate-solutions.com/
 */ {
    /**
     * Localisation
     * */
    load_plugin_textdomain('woo-export-order', false, dirname(plugin_basename(__FILE__)) . '/');
    
    
    include 'export_orders_with_coupons.php';
    include 'export_coupons_page.php';
    include 'export_orders.php';

    /**
     * woo_export class
     * */
    if (!class_exists('woo_export')) {

        class woo_export {

            public function __construct() {

                // WordPress Administration Menu
                add_action('admin_menu', array(&$this, 'm_wc_export_orders_menu'));

                add_action('admin_enqueue_scripts', array(&$this, 'export_enqueue_scripts_css'));
                add_action('admin_enqueue_scripts', array(&$this, 'export_enqueue_scripts_js'));
            }

            /**
             * Functions
             */
            function export_enqueue_scripts_css() {

                if (isset($_GET['page']) && ($_GET['page'] == 'export_orders_with_coupons_page' || $_GET['page'] == 'export_orders_page' || $_GET['page'] == 'export_coupons_page')) {
                    wp_enqueue_style('woocommerce_admin_styles', plugins_url() . '/woocommerce/assets/css/admin.css');

                    wp_enqueue_style('dataTable', plugins_url('/css/data.table.css', __FILE__), '', '', false);

                    wp_enqueue_style('TableTools', plugins_url('/TableTools/media/css/TableTools.css', __FILE__), '', '', false);
                }
            }

            function export_enqueue_scripts_js() {

                if (isset($_GET['page']) && ($_GET['page'] == 'export_orders_with_coupons_page' || $_GET['page'] == 'export_orders_page' || $_GET['page'] == 'export_coupons_page')) {
                    wp_register_script('dataTable', plugins_url() . '/mocion-wc-export-orders/js/jquery.dataTables.js');
                    wp_enqueue_script('dataTable');

                    wp_register_script('TableTools', plugins_url() . '/mocion-wc-export-orders/TableTools/media/js/TableTools.js');
                    wp_enqueue_script('TableTools');

                    wp_register_script('ZeroClip', plugins_url() . '/mocion-wc-export-orders/TableTools/media/js/ZeroClipboard.js');
                    wp_enqueue_script('ZeroClip');
                }
            }

            function m_wc_export_orders_menu() {
                add_menu_page('Reportes', 'Reportes', 'manage_woocommerce', 'export_orders_with_coupons_page');
                
                add_submenu_page('export_orders_with_coupons_page.php', __('Pedidos con cupones', 'woo-export-order'), __('Pedidos con cupones', 'woo-export-order'), 'manage_woocommerce', 'export_orders_with_coupons_page', array(&$this, 'export_orders_with_coupons_page'));
                add_submenu_page('export_coupons_page.php', __('Cupones', 'woo-export-order'), __('Cupones', 'woo-export-order'), 'manage_woocommerce', 'export_coupons_page', array(&$this, 'export_coupons_page'));
                add_submenu_page('export_orders_page.php', __('Ventas', 'woo-export-order'), __('Ventas', 'woo-export-order'), 'manage_woocommerce', 'export_orders_page', array(&$this, 'export_orders_page'));
                //remove_submenu_page('export_settings','exports_settings');
            }

            function export_orders_with_coupons_page() {
                return export_orders_with_coupons();
            }
            
            function export_coupons_page() {
                return export_coupons();
            }
            
            function export_orders_page() {
                return export_orders();
            }

        }

    }

    $woo_export = new woo_export();
}
?>