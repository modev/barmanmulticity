<?php

/**
 * Plugin Name: WooCommerce API Manager
 * Plugin URI: https://www.toddlahman.com/shop/woocommerce-api-manager/
 * Description: Manages APIs for software API Key activation/deactivation and software updates. API Manager code integration into products available from <a href="http://www.toddlahman.com/shop/api-code-integration/">Todd Lahman</a>.
 * Version: 1.3.7
 * Author: Todd Lahman LLC
 * Author URI: https://www.toddlahman.com
 * Text Domain: woocommerce-api-manager
 * Domain Path: /i18n/languages/
 *
 * Intellectual Property rights, and copyright, reserved by Todd Lahman, LLC as allowed by law include,
 * but are not limited to, the working concept, function, and behavior of this plugin,
 * the logical code structure and expression as written.
 *
 * @package     WooCommerce API Manager
 * @author      Todd Lahman LLC
 * @category    Plugin
 * @copyright   Copyright (c) Todd Lahman LLC
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'f7cdcfb7de76afa0889f07bcb92bf12e', '260110' );

/**
 * If WooCommerce is not active disable WooCommerce API Manager.
 *
 * @since 1.0
 */
if ( ! is_woocommerce_active() ) {
	add_action( 'admin_notices', 'WooCommerce_API_Manager::woocommerce_inactive_notice' );
	return;
}

final class WooCommerce_API_Manager {

	/**
	 * @var string
	 */
	public $version = '1.3.7';

	/**
	 * @var The single instance of the class
	 * @since 1.3
	 */
	private static $_instance = null;

	/**
	 * @var boolean
	 */
	public static $wc_am_self_upgrade = false; // Should this product query the remote upgrade API for upgrades of itself?

	/**
	 * @var boolean
	 */
	public $upgrade_status;

	/**
	 * @var string
	 */
	public $wc_api_manager_version_name = 'wc_plugin_api_manager_version';

	/**
	 * @var string
	 */
	public $plugin_url;

	/**
	 * @var string
	 */
	public $plugins_dir_url;

	/**
	 * @var string
	 */
	public $plugin_path;

	/**
	 * @var string
	 */
	public $plugins_basename;

	/**
	 * @var string
	 */
	public $wc_version;

	/**
	 * @var string
	 */
	public $text_domain = 'woocommerce-api-manager';

	/**
	 * Self Upgrade Values
	 */
	// Base URL to the remote upgrade API server
	public $upgrade_url = 'https://www.toddlahman.com/';

	// URL to customer dashboard
	public $renew_license_url = 'https://www.toddlahman.com/my-account';

	// Is this a plugin or a theme?
	public $plugin_or_theme = 'plugin';

	/**
	 * @var string
	 */
	public $update_version;

	/**
	 * @var string
	 */
	public $update_check = 'am_plugin_update_check';

	public $file;
	public $fid = 'f7cdcfb7de76afa0889f07bcb92bf12e';
	public $pid = '260110';

	/**
	 * @var object
	 */
	public $hash;
	public $array;
	public $helpers;
	public $encrypt;
	public $tools;
	public $key;
	public $tool_tips;

	/**
	 * Singular class instance safeguard.
	 * Ensures only one instance of a class can be instantiated.
	 * Follows a singleton design pattern.
	 *
	 * @since 1.3
	 * @static
	 * @return WooCommerce_API_Manager - Main instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) && ! ( self::$_instance instanceof WooCommerce_API_Manager ) ) {
			self::$_instance = new self();
			if ( ! is_object( self::$_instance->hash ) ) {
				self::$_instance->hash = WC_Api_Manager_Hash::instance();
			}
			if ( ! is_object( self::$_instance->array ) ) {
				self::$_instance->array = WC_Api_Manager_Array::instance();
			}
			if ( ! is_object( self::$_instance->helpers ) ) {
				self::$_instance->helpers = WC_Api_Manager_Helpers::instance();
			}
			if ( ! is_object( self::$_instance->encrypt ) ) {
				self::$_instance->encrypt = WC_Api_Manager_Encryption::instance();
			}
			if ( is_admin() ) {
				if ( ! is_object( self::$_instance->key ) ) {
					self::$_instance->key = WC_Api_Manager_Key::instance();
				}
				if ( ! is_object( self::$_instance->tool_tips ) ) {
					self::$_instance->tool_tips = WC_API_Manager_Tool_Tips::instance();
				}
				if ( ! is_object( self::$_instance->tools ) ) {
					self::$_instance->tools = WC_API_Manager_Tools::instance();
				}
				self::$_instance->update_check();
			}
		}
		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.3.3
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.3.3
	 */
	private function __wakeup() {}

	/**
	 * Gets things started by adding an action to initialize this plugin once
	 * WooCommerce is known to be active and initialized
	 */
	public function __construct() {

		$this->upgrade_status = self::$wc_am_self_upgrade;

		// WooCommerce Version
		$this->wc_version = get_option( 'woocommerce_version' );

		$this->file = plugin_basename( __FILE__ );

		$this->define_constants();

		// Include required files
		$this->includes();

		/**
		 * Deletes all data if plugin deactivated
		 */
		register_deactivation_hook( __FILE__, array( $this, 'deactivation' ) );

	}

	/**
	 * Define WAM Constants
	 */
	public function define_constants() {
		if ( ! defined( 'WAM_PLUGIN_FILE' ) ) {
			define( 'WAM_PLUGIN_FILE', __FILE__ );
		}
	}

	/**
	 * Processes all Software Update Requests
	 *
	 * @return WC_Plugin_Update_API_Manager_API
	 */
	public function handle_upgrade_api_request() {
		// Responds to plugin update requests
		require_once( 'includes/class-wc-am-update-api.php' );
		return WC_Plugin_Update_API_Manager_API::instance( $_REQUEST );
	}

	/**
	 * Processes all Software Activation/Deactivation Requests
	 *
	 * @return WC_API_Manager_Software_API
	 */
	public function handle_software_api_request() {
		// Responds to plugin update requests
		require_once( 'includes/class-wc-am-software-api.php' );
		return WC_API_Manager_Software_API::instance( $_REQUEST );
	}

	/** Helper functions ******************************************************/

	/**
	 * Get the plugin's url.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_url() {
		if ( isset( $this->plugin_url ) ) {
			return $this->plugin_url;
		}

		return $this->plugin_url = plugins_url( '/', __FILE__ );
	}

	/**
	 * Get the plugin directory url.
	 *
	 * @access public
	 * @return string
	 */
	public function plugins_dir_url() {
		if ( isset( $this->plugins_dir_url ) ) {
			return $this->plugins_dir_url;
		}

		return $this->plugins_dir_url = plugin_dir_url( '/', __FILE__ );
	}

	/**
	 * Get the plugin path.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_path() {
		if ( isset( $this->plugin_path ) ) {
			return $this->plugin_path;
		}

		return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}

	/**
	 * Get the plugin basename.
	 *
	 * @access public
	 * @return string
	 */
	public function plugins_basename() {
		if ( isset( $this->plugins_basename ) ) {
			return $this->plugins_basename;
		}

		return $this->plugins_basename = untrailingslashit( plugin_basename( __FILE__ ) );
	}

	/**
	 * Get Ajax URL.
	 *
	 * @return string
	 */
	public function ajax_url() {
		return admin_url( 'admin-ajax.php', 'relative' );
	}

	/**
	 * admin_scripts function.
	 */
	public function admin_styles() {
		wp_enqueue_style( 'woocommerce_api_manager_admin_styles', $this->plugin_url() . 'assets/css/admin.css', true );
	}

	public function admin_scripts() {
		wp_enqueue_script( 'jquery-ui-tooltip' );
	}

	/**
	 * Enqueue frontend scripts and styles.
	 *
	 * @return void
	 */
	public function frontend_scripts() {}

	/**
	 * Get WooCommerce styles for the frontend
	 * @param  array
	 * @return array
	 */
	public function wc_enqueue_styles( $styles ) {

		if ( is_page( get_option( 'woocommerce_myaccount_page_id' ) ) ) {
			$styles['woocommerce-api-manager'] = array(
				'src'     => str_replace( array( 'http:', 'https:' ), '', $this->plugin_url() ) . 'assets/css/woocommerce-api-manager.css?' . filemtime( $this->plugin_path() . '/assets/css/woocommerce-api-manager.css' ),
				'deps'    => 'woocommerce-smallscreen',
				'version' => WC_VERSION,
				'media'   => ''
			);
		}

		return $styles;

	}

	/**
	 * Output any queued javascript code in the footer.
	 */

	/**
	 * Output queued JavaScript code in the footer inline.
	 * @since 1.3
	 * @param  string $wc_queued_js JavaScript
	 * @return string Inline JavaScript
	 */
	public function wc_print_js( $wc_queued_js ) {

		if ( ! empty( $wc_queued_js ) ) {

			echo "<!-- WooCommerce API Manager JavaScript -->\n<script type=\"text/javascript\">\njQuery(function($) {";

			// Sanitize
			$wc_queued_js = wp_check_invalid_utf8( $wc_queued_js );
			$wc_queued_js = preg_replace( '/&#(x)?0*(?(1)27|39);?/i', "'", $wc_queued_js );
			$wc_queued_js = str_replace( "\r", '', $wc_queued_js );

			echo $wc_queued_js . "});\n</script>\n";

			unset( $wc_queued_js );
		}
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 *
	 * @access public
	 * @return void
	 */
	public function includes() {

		// Set up localisation
		$this->load_plugin_textdomain();

		require_once( 'includes/class-wc-am-install.php' );
		require_once( 'includes/class-wc-am-hash.php' );
		require_once( 'includes/class-wc-am-array.php' );
		require_once( 'includes/class-wc-am-helpers.php' );
		require_once( 'includes/class-wc-am-encryption.php' );
		require_once( 'includes/class-wc-am-plugin-update.php' );

		/**
		 * Upgrade API
		 */
		$this->api_request_url( 'upgrade-api' );
		add_action( 'woocommerce_api_upgrade-api', array( $this, 'handle_upgrade_api_request' ) );

		/**
		 * Software API
		 */
		$this->api_request_url( 'am-software-api' );
		add_action( 'woocommerce_api_am-software-api', array( $this, 'handle_software_api_request' ) );

		// Add the Settings | Documentation | Support links on the Plugins administration screen
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'action_links' ) );

		add_action( 'woocommerce_admin_css', array( $this, 'admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		//add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );
		add_filter( 'woocommerce_enqueue_styles', array( $this, 'wc_enqueue_styles' ), 10, 1 );

		// AJAX
		add_action( 'wp_ajax_woocommerce_lost_api_key', array( $this, 'lost_api_key_ajax' ) );
		add_action( 'wp_ajax_nopriv_woocommerce_lost_api_key', array( $this, 'lost_api_key_ajax' ) );

		// Shortcodes
		add_shortcode( 'woocommerce_api_manager_lost_api_key', array( $this, 'lost_api_key_page' ) );

		// Display Update API Manager data on a User's account page
		add_action( 'woocommerce_before_my_account', array( $this, 'get_my_api_manger_account_template' ) );

		// Delete the API Manager user order array before the order is deleted
		add_action( 'woocommerce_delete_order_items', array( $this, 'delete_user_order_info' ) );

		if ( get_option( 'woocommerce_api_manager_remove_all_download_links' ) == 'yes' ) {

			// Removes all download links from email, My Account, and Order Details in My Account
			$this->remove_my_account_email_download_links();

		} else {

			/**
			 * For My Account dashboard Available downloads section
			 * If there is an Amazon S3 URL in the Downloadable Files > File URL field for the product,
			 * then replace the My Account > Available Downloads link with the Amazon S3 link
			 */
			add_filter( 'woocommerce_available_download_link', array( $this, 'my_acount_download_amazon_s3_url'), 10, 2 );

			// In emails and My Account view-order Order Details, a download link will be converted to an
			// Amazon S3 download link, with an expiration of 24 hours.
			//add_filter( 'woocommerce_get_item_downloads', array( $this, 'order_details_amazon_s3_url' ), 1, 3 );
			add_filter( 'woocommerce_get_item_downloads', '__return_empty_array' );

		}

		// plugin update download API hook
		require_once( 'includes/class-wc-am-downloads.php' );

		if ( is_admin() ) {

			// License key verification for udpates
			require_once( 'includes/class-wc-am-key.php' );
			// Settings admin page
			require_once( 'includes/class-wc-am-tool-tips.php' );

			// Check for external connection blocking
			add_action( 'admin_notices', array( $this, 'check_external_blocking' ) );

			require_once( 'admin/tools/class-wc-am-tools.php' );

			add_action( 'admin_footer', array( $this, 'wc_print_js' ), 25 );

			require_once( 'admin/class-wc-am-settings-admin.php' );

			// WooCommerce product pages
			require_once( 'includes/class-wc-am-product-admin.php' );

			require_once( 'includes/class-wc-am-order-admin.php' );

			// Load WooCommerce Dashboard Settings
			// require_once( 'admin/class-wc-am-settings.php' );

		}
	}

	/**
	 * Return the API URL for a given request
	 * http://docs.woothemes.com/document/wc_api-the-woocommerce-api-callback/
	 *
	 * @param mixed $request
	 * @param mixed $ssl (default: null)
	 * @return string
	 */
	public function api_request_url( $request, $ssl = null ) {
		if ( is_null( $ssl ) ) {
			$scheme = parse_url( get_option( 'home' ), PHP_URL_SCHEME );
		} elseif ( $ssl ) {
			$scheme = 'https';
		} else {
			$scheme = 'http';
		}

		if ( get_option('permalink_structure') ) {
			return esc_url_raw( trailingslashit( home_url( '/wc-api/' . $request, $scheme ) ) );
		} else {
			return esc_url_raw( add_query_arg( 'wc-api', $request, trailingslashit( home_url( '', $scheme ) ) ) );
		}
	}

	private function update_check() {
		// Classes only loaded if self upgrade set to true
		if ( $this->upgrade_status ) {

			require_once( 'admin/class-wc-am-menu.php' );

			/**
			 * Check for software updates
			 */
			$options = get_option( 'wc_api_manager' );

			if ( ! empty( $options ) && $options !== false ) {

				$upgrade_url = $this->upgrade_url;
				$plugin_name = $this->plugins_basename(); // same as plugin slug. if a theme use a theme name like 'twentyeleven'
				$product_id = get_option( 'wc_api_manager_product_id' );
				$api_key = $options['api_key'];
				$activation_email = $options['activation_email'];
				$renew_license_url = 'https://www.toddlahman.com/my-account';
				$instance = get_option( 'wc_api_manager_instance' );
				$domain = str_ireplace( array( 'http://', 'https://' ), '', home_url() );
				$software_version = get_option( 'wc_plugin_api_manager_version' );
				$plugin_or_theme = 'plugin'; // 'theme' or 'plugin'
				// $this->text_domain is used to define localization for translation.

				return WC_API_Manager_Software_Update::instance( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme, $this->text_domain );

			}

		}
	}

	/**
	 * Include Settings | Documentation | Support links links on the Plugins administration screen
	 * Added string keys to prevent duplicates
	 * @since 1.3
	 * @param string $links
	 * @return array
	 */
	public function action_links( $links ) {

		return array_merge( array(
			'Settings' 			=> '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=api_manager' ) . '">' . __( 'Settings', 'woocommerce-api-manager' ) . '</a>',
			'Docs' 				=> '<a href="' . esc_url( apply_filters( 'woocommerce_api_manager_docs_url', 'http://docs.woothemes.com/document/woocommerce-api-manager/', 'woocommerce-api-manager' ) ) . '">' . __( 'Docs', 'woocommerce-api-manager' ) . '</a>',
			'Premium Support' 	=> '<a href="' . esc_url( apply_filters( 'woocommerce_api_manager_support_url', 'http://support.woothemes.com/' ) ) . '">' . __( 'Premium Support', 'woocommerce-api-manager' ) . '</a>',
		), $links );

	}

	/**
	 * For Emails  and My Account view-order Order Details
	 * If there is an Amazon S3 URL in the Downloadable Files > File URL field for the product,
	 * then replace the Order Details > Download File, and the same link in emails, with the Amazon S3 link
	 * @since 1.3.3
	 * @param  array $files
	 * @param  array $item
	 * @param  array $data
	 * @return array
	 */
	public function order_details_amazon_s3_url( $files, $item, $data ) {

		$product_id = $item['variation_id'] > 0 ? $item['variation_id'] : $item['product_id'];

		$url = $this->helpers->get_download_url( $product_id );
		if ( ! empty( $url ) && $this->helpers->find_amazon_s3_in_url( $url ) === true ) {

			$download_id = $this->helpers->get_download_id( $product_id );

			// Email links get 24 hours before they expire, this link also appears on /my-account/view-order Order Details
			$expire = time() + DAY_IN_SECONDS;

			$files[ $download_id ][''] = $this->helpers->get_download_filename( $product_id );
			$files[ $download_id ]['download_url'] = $this->helpers->format_secure_s3_url( $url, $expire );

			// if the Email and My Account > Order Details download link should not be displayed
			if ( get_option( 'woocommerce_api_manager_remove_email_link' ) == 'yes' ) {
				return array();
			}

			return $files;
		}

		return $files;

	}

	/**
	 * For My Account dashboard Available downloads section
	 * If there is an Amazon S3 URL in the Downloadable Files > File URL field for the product,
	 * then replace the My Account > Available Downloads link with the Amazon S3 link
	 * @since 1.3.2
	 * @param  string $download_url
	 * @param  array $download
	 * @return string
	 */
	public function my_acount_download_amazon_s3_url( $download_url, $download ) {

		$product_id = ( ! empty( $download['variation_id'] ) ) ? $download['variation_id'] : $download['product_id'];

		$url = $this->helpers->get_download_url( $product_id );

		if ( ! empty( $url ) && $this->helpers->find_amazon_s3_in_url( $url ) === true ) {

			return '<a href="' . esc_url( $this->helpers->format_secure_s3_url( $url ) ) . '">' . $download['download_name'] . '</a>';

		}

		return '<a href="' . esc_url( $download['download_url'] ) . '">' . $download['download_name'] . '</a>';

	}

	/**
	 * Removes all download links from email, My Account, and Order Details in the My Account dashboard
	 * The My Account template also contain JavaScript in the footer to remove all Available Download HTML leftover
	 * The My API Downloads table contains download URLs for each product
	 * @since 1.3.4
	 * @return bool
	 */
	public function remove_my_account_email_download_links() {

		// Remove all Available Download Lins from My Account dashboard
		add_filter( 'woocommerce_my_account_my_downloads_title', '__return_false' );
		add_filter( 'woocommerce_available_download_count', '__return_false' );
		add_filter( 'woocommerce_available_download_link', '__return_false' );

		// Remove all download links from emails and My Account view-order Order Details
		add_filter( 'woocommerce_get_item_downloads', '__return_empty_array' );

	}

	/** AJAX *****************************************************************/

	public function lost_api_key_ajax() {

		check_ajax_referer( 'wc-lost-api-key', 'security' );

		$email = esc_attr( trim( $_POST['email'] ) );

		if ( ! is_email( $email ) ) {

			wp_send_json( array(
				'success' 	=> false,
				'message'	=> __( 'Invalid Email Address', 'woocommerce-api-manager' )
			) );

		}

		// returns $user->ID
		$user = get_user_by( 'email', $email );

		if ( is_object( $user ) ) {

			// Get the orders for this customer
			$user_orders = $this->helpers->get_users_data( $user->ID );

			if ( isset( $user_orders ) ) {

				foreach ( $user_orders as $order_key => $data ) {

					// Find the API keys that are part of the same data array as the email address
					if ( $data['license_email'] == $email ) {

						// Check if this is an order_key, or the new longer api_key
						$order_data_key = ( ! empty( $data['api_key'] ) ) ? $data['api_key'] : $data['order_key'];

						$api_keys[] = $order_data_key;

					}
				}

				// Populate an order data array that only matches the API keys
				foreach ( $api_keys as $key => $api_key ) {

					$order_info[] = $user_orders[$api_key];

				}

			}
		}

		if ( count( $order_info ) > 0 ) {

			$mailer = WC()->mailer();

			ob_start();

			wc_get_template( 'email-lost-api-keys.php', array(
				'keys'	=> $order_info,
				'email_heading' => __( 'Your API Keys', 'woocommerce-api-manager' )
			), 'woocommerce-api-manager', $this->plugin_path() . '/templates/' );

			$message = ob_get_clean();

			woocommerce_mail( $email, __( 'Your API Keys', 'woocommerce-api-manager' ), $message );

			wp_send_json( array(
				'success' 	=> true,
				'message'	=> __( 'Your API Keys have been emailed', 'woocommerce-api-manager' )
			) );

		} else {

			wp_send_json( array(
				'success' 	=> false,
				'message'	=> __( 'No API Keys were found for your email address', 'woocommerce-api-manager' )
			) );

		}

	}

	/** Templates ************************************************************/

	/**
	 * Emails a list of API Keys to a customer
	 * @return void
	 *
	 */
	public function lost_api_key_page() {

		wc_get_template( 'lost-api-key.php', '', 'woocommerce-api-manager', $this->plugin_path() . '/templates/' );

	}

	/**
	 * Loads the my-api-manager.php template on the My Account page.
	 * @return void
	 */
	public function get_my_api_manger_account_template() {

		$user_id = get_current_user_id();

		wc_get_template( 'my-api-keys.php', array( 'user_id' => $user_id ), 'woocommerce-api-manager', $this->plugin_path() . '/templates/' );
		wc_get_template( 'my-api-downloads.php', array( 'user_id' => $user_id ), 'woocommerce-api-manager', $this->plugin_path() . '/templates/' );

	}

	/**
	 * delete_user_order_info Deletes API Manager user order and activations data when the corresponding
	 * WC order is deleted
	 *
	 * @since 1.1.1
	 * @param  int $post_id
	 * @return void
	 */
	public function delete_user_order_info( $post_id ) {
		global $wpdb;

		$order_meta = get_post_meta( $post_id );

		if ( ! empty( $order_meta ) && is_array( $order_meta ) ) {

			$order_key 	= get_post_meta( $post_id, '_order_key', true );
			$user_id 	= get_post_meta( $post_id, '_customer_user', true );

		}

		/** Delete the Activation data ********************************************/
		if ( ! empty( $order_key ) && ! empty( $user_id ) ) {

			delete_user_meta( $user_id, $wpdb->get_blog_prefix() . $this->helpers->user_meta_key_activations . $order_key );

			/** Delete the Order data ********************************************/

			// All user orders in a multidimensional array each indexed by order_key
			$user_orders = $this->helpers->get_users_data( $user_id );

		}

		if ( is_array( $user_orders ) && ! empty( $user_orders ) ) {

			foreach ( $user_orders as $key => $value ) {

				if ( $value['order_id'] == $post_id ) {

					// Delete the activation data array
	    			unset( $user_orders[$key] );

					update_user_meta( $user_id, $wpdb->get_blog_prefix() . $this->helpers->user_meta_key_orders, $user_orders );

					$re_check_user_orders = $this->helpers->get_users_data( $user_id );

					if ( empty( $re_check_user_orders ) ) {

						delete_user_meta( $user_id, $wpdb->get_blog_prefix() . $this->helpers->user_meta_key_orders );

					}

				}

			}

		}

	}

	/**
	 * Displays an inactive notice when WooCommerce is inactive.
	 *
	 * @since 1.0
	 */
	public static function woocommerce_inactive_notice() { ?>
		<div id="message" class="error">
			<p><?php printf( __( 'The %sWooCommerce API Manager is inactive.%s The %sWooCommerce%s plugin must be active for the WooCommerce API Manager to work. Please activate WooCommerce on the %splugin page%s once it is installed.', 'woocommerce-api-manager' ), '<strong>', '</strong>', '<a href="http://wordpress.org/extend/plugins/woocommerce/" target="_blank">', '</a>', '<a href="' . esc_url( admin_url( 'plugins.php' ) ) . '">', '</a>' ); ?></p>
		</div>
		<?php
	}

	/**
	 * Deletes all data if plugin deactivated
	 * @return void
	 */
	public function deactivation() {
		global $wpdb, $blog_id;

		$options = get_option( 'wc_api_manager' );

		if ( ! empty( $options ) && $options !== false ) {

			$this->license_key_deactivation();

		}

		// Remove options
		if ( is_multisite() ) {

			switch_to_blog( $blog_id );

			foreach ( array(
					'wc_api_manager',
					'wc_api_manager_product_id',
					'wc_api_manager_instance',
					'wc_api_manager_deactivate_checkbox',
					'wc_api_manager_activated',
					'wc_plugin_api_manager_version',
					) as $option) {

					delete_option( $option );

					}

			restore_current_blog();

		} else {

			foreach ( array(
					'wc_api_manager',
					'wc_api_manager_product_id',
					'wc_api_manager_instance',
					'wc_api_manager_deactivate_checkbox',
					'wc_api_manager_activated',
					'wc_plugin_api_manager_version',
					) as $option) {

					delete_option( $option );

					}

		}

		// Remove tables
		$wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "woocommerce_api_manager_secure_hash" );

	}

	/**
	 * Deactivates the license on the API server
	 * @return void
	 */
	public function license_key_deactivation() {

		$activation_status = get_option( 'wc_api_manager_activated' );

		$default_options = get_option( 'wc_api_manager' );

		$api_email = $default_options['activation_email'];
		$api_key = $default_options['api_key'];

		$args = array(
			'email' => $api_email,
			'licence_key' => $api_key,
			);

		if ( $activation_status == 'Activated' && $api_key != '' && $api_email != '' ) {
			$this->key->deactivate( $args ); // reset license key activation
		}
	}

	/**
	 * Check for external blocking contstant
	 * @return string
	 */
	public function check_external_blocking() {
		// show notice if external requests are blocked through the WP_HTTP_BLOCK_EXTERNAL constant
		if( defined( 'WP_HTTP_BLOCK_EXTERNAL' ) && WP_HTTP_BLOCK_EXTERNAL === true ) {

			// check if our API endpoint is in the allowed hosts
			$host = parse_url( $this->upgrade_url, PHP_URL_HOST );

			if( ! defined( 'WP_ACCESSIBLE_HOSTS' ) || stristr( WP_ACCESSIBLE_HOSTS, $host ) === false ) {
				?>
				<div class="error">
					<p><?php printf( __( '<b>Warning!</b> You\'re blocking external requests which means you won\'t be able to get %s updates. Please add %s to %s.', 'woocommerce-api-manager' ), $this->ame_software_product_id, '<strong>' . $host . '</strong>', '<code>WP_ACCESSIBLE_HOSTS</code>'); ?></p>
				</div>
				<?php
			}

		}
	}

	/**
	 * Load Localisation files.
	 *
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'woocommerce-api-manager', false, dirname( $this->plugins_basename() ) . '/i18n/languages' );
	}

} // End class

/**
 * Returns the main instance of WooCommerce_API_Manager to prevent the need to use globals.
 *
 * @since  1.3
 * @return WooCommerce_API_Manager
 */
function WCAM() {
	return WooCommerce_API_Manager::instance();
}

// Initialize the class instance only once
WCAM();
