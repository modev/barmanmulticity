<?php

// Make sure that we are uninstalling
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit;
}

function wam_uninstall() {
	global $wpdb, $blog_id;

	$options = get_option( 'wc_api_manager' );

	if ( ! empty( $options ) && $options !== false ) {

		wam_license_key_deactivation();

	}

	// Remove options
	if ( is_multisite() ) {

		switch_to_blog( $blog_id );

		foreach ( array(
				'wc_api_manager',
				'wc_api_manager_product_id',
				'wc_api_manager_instance',
				'wc_api_manager_deactivate_checkbox',
				'wc_api_manager_activated',
				'wc_plugin_api_manager_version',
				) as $option) {

				delete_option( $option );

				}

		restore_current_blog();

	} else {

		foreach ( array(
				'wc_api_manager',
				'wc_api_manager_product_id',
				'wc_api_manager_instance',
				'wc_api_manager_deactivate_checkbox',
				'wc_api_manager_activated',
				'wc_plugin_api_manager_version',
				) as $option) {

				delete_option( $option );

				}

	}

	// Remove tables
	$wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "woocommerce_api_manager_secure_hash" );

}

function wam_license_key_deactivation() {

	$activation_status = get_option( 'wc_api_manager_activated' );

	$default_options = get_option( 'wc_api_manager' );

	$api_email = $default_options['activation_email'];
	$api_key = $default_options['api_key'];

	$args = array(
		'email' => $api_email,
		'licence_key' => $api_key,
		);

	if ( $activation_status == 'Activated' && $api_key != '' && $api_email != '' ) {
		require_once( 'includes/class-wc-am-key.php' );
		$key = new WC_Api_Manager_Key;
		$key->deactivate( $args ); // reset license key activation
	}
}

wam_uninstall();
