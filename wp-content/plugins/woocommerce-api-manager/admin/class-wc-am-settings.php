<?php

/**
 * WooCommerce Admin Settings Class
 *
 * @package WooCommerce/Update API Manager/Settings Admin
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.0
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WC_API_Manager_Settings {

	private $settings;
	private $license_check;

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	// Load admin menu
	public function __construct() {

		//$this->license_check 	= ( WCAM()->helpers->is_plugin_active( 'woocommerce-software-add-on/woocommerce-software.php' ) ) ? __('The Software Add-On license keys replace the Update API Manager license keys for all products.', 'woocommerce-api-manager' ) : __('ATTENTION! <a href="http://www.woothemes.com/products/software-add-on/" target="_blank">WooCommerce Software Add-on</a> is required to be installed and activated to use this option.', 'woocommerce-api-manager' );

		// Settings init
		$this->settings = array(
			array( 'name' => __( 'Update API Manager', 'woocommerce-api-manager' ), 'type' => 'title', 'desc' => '', 'id' => 'api_manager' ),
			array(
				'name' 		=> __('Software Add-On License Keys', 'woocommerce-api-manager' ),
				//'desc' 		=> $this->license_check,
				'desc' 		=> __('The Software Add-On license keys replace the Update API Manager license keys for all products.', 'woocommerce-api-manager' ),
				'id' 		=> 'wc_api_manager_software_add_on_license_key',
				'type' 		=> 'checkbox',
			),
			array( 'type' => 'sectionend', 'id' => 'api_manager'),
		);

		// Settings hooks
		add_action( 'woocommerce_settings_general_options_after', array( $this, 'admin_settings' ) );
		add_action( 'woocommerce_update_options_general', array( $this, 'save_admin_settings' ) );

	}

	/**
	 * admin_settings function.
	 *
	 * @access public
	 * @return void
	 */
	public function admin_settings() {
		woocommerce_admin_fields( $this->settings );
	}

	/**
	 * save_admin_settings function.
	 *
	 * @access public
	 * @return void
	 */
	public function save_admin_settings() {
		woocommerce_update_options( $this->settings );
	}

} // end of class

WC_API_Manager_Settings::instance();
