<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<div class="woocommerce_order_items_wrapper">
	<table id="activations-table" class="woocommerce_order_items" cellspacing="0">
		<thead>
	    	<tr>
				<th><?php _e( 'API Key', 'woocommerce-api-manager' ) ?></th>
				<th><?php _e( 'Version', 'woocommerce-api-manager' ) ?></th>
				<th><?php _e( 'Software Title', 'woocommerce-api-manager' ) ?></th>
				<th><?php _e( 'Status', 'woocommerce-api-manager' ) ?></th>
				<th><?php _e( 'Date &amp; Time', 'woocommerce-api-manager' ) ?></th>
				<th><?php _e( 'Domain/Platform', 'woocommerce-api-manager' ) ?></th>
				<th><?php _e( 'Action', 'woocommerce-api-manager' ) ?></th>
			</tr>
		</thead>
		<tbody>
	    	<?php $i = 1; foreach ( $activations as $activation ) : $i++ ?>
				<tr<?php if ( $i % 2 == 1 ) echo ' class="alternate"' ?>>
					<td class="am_tooltip" title="<?php echo $activation['order_key']; ?>"><?php echo sanitize_text_field( '&hellip; ' . WCAM()->helpers->get_last_string_characters( $activation['order_key'], 4 ) ) ?></td>
					<td><?php echo ( ! empty( $activation['software_version'] ) ) ? sanitize_text_field( $activation['software_version'] ) : ''; ?></td>
					<td><?php echo sanitize_text_field( $activation['product_id'] ); ?></td>
					<td class="activation_active"><?php echo ( $activation['activation_active'] ) ? __( 'Active', 'woocommerce-api-manager' ) : __( 'Inactive', 'woocommerce-api-manager' ) ?></td>
					<td><?php echo date_i18n( __( 'M j\, Y \a\t h:i a', 'woocommerce-api-manager' ), strtotime( $activation['activation_time'] ) ) ?></td>
					<td><a href="<?php echo esc_url( $activation['activation_domain'] ) ?>" target="_blank"><?php echo WCAM()->helpers->remove_url_prefix( $activation['activation_domain'] ) ?></a></td>
					<td>
						<button type="button" rel="<?php echo WCAM()->helpers->esc_url_no_scheme( $activation['activation_domain'] ); ?>" id="<?php echo sanitize_text_field( $activation['instance'] ); ?>" class="delete_api_key button"><?php _e( 'Delete', 'woocommerce-api-manager' ); ?></button>
					</td>
	      		</tr>
	    	<?php endforeach; ?>
		</tbody>
	</table>
</div>
