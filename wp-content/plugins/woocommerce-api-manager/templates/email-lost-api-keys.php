<?php // API Manager ?>

<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<?php if ( count( $keys ) > 0 ) : ?>

	<?php foreach ( $keys as $key ) :

			// Determine the Software Title from the customer order data
			$software_title = ( empty( $key['_api_software_title_var'] ) ) ? $key['_api_software_title_parent'] : $key['_api_software_title_var'];

			if ( empty( $software_title ) ) :

				$software_title = $key['software_title'];

			endif;

			// Check if this is an order_key, or the new longer api_key
			$order_data_key = ( ! empty( $key['api_key'] ) ) ? $key['api_key'] : $key['order_key'];

	?>

		<h3><?php echo $software_title; ?></h3>

		<ul>
			<li><?php ( WCAM()->helpers->get_billing_country( $key['order_id'] ) == 'GB' ) ? _e( 'API Licence Key:', 'woocommerce-api-manager' ) : _e( 'API License Key:', 'woocommerce-api-manager' ); ?> <strong><?php echo $order_data_key; ?></strong></li>
			<li><?php ( WCAM()->helpers->get_billing_country( $key['order_id'] ) == 'GB' ) ? _e( 'API Licence Email:', 'woocommerce-api-manager' ) : _e( 'API License Email:', 'woocommerce-api-manager' ); ?> <strong><?php echo $key['license_email']; ?></strong></li>
		</ul>

	<?php endforeach; ?>

<?php endif; ?>

<?php do_action( 'woocommerce_email_footer' ); ?>
