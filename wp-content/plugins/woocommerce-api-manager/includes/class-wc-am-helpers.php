<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce API Manager Helpers Class
 *
 * @package API Manager/Helpers
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.0.0
 *
 */

// For troubleshooting
//exit( var_dump( $wpdb->last_query ) );

class WC_Api_Manager_Helpers {

	public $user_meta_key_orders 		= 'wc_am_orders';
	public $user_meta_key_activations 	= 'wc_am_activations_';

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	public function __construct() {

		// Handle customer request to delete a domain name from My Account
		add_action( 'init', array( $this, 'delete_my_account_url' ) );

		/**
		 * Don't generate new API License Key for Subscription renewal
		 * @since 1.3.6
		 */
		if ( $this->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) ) {
			add_action( 'woocommerce_subscriptions_renewal_order_created', array( $this, 'remove_order_complete' ) );
		}

		// order_complete function saves user_meta data to be used by the email template and the API Manager
		add_action( 'woocommerce_order_status_completed', array( $this, 'order_complete' ) );

		if ( is_admin() ) {
			// Allows the API License Key from post meta order data to be found in shop order search
			add_filter( 'woocommerce_shop_order_search_fields', array( $this, 'searchable_api_license_key' ) );
		}

		add_action( 'woocommerce_email_before_order_table', array( $this, 'email_license_keys' ) );

	}

	/**
	 * Don't generate new API License Key for Subscription renewal, and don't email API License Keys for renewals
	 * @since 1.3.6
	 * @return void
	 */
	public function remove_order_complete() {
		remove_action( 'woocommerce_order_status_completed', array( $this, 'order_complete' ) );
		remove_action( 'woocommerce_email_before_order_table', array( $this, 'email_license_keys' ) );
	}

	/**
	 * Builds the user data array for an order to provide access the License Key and Update APIs.
	 * @since 1.0 refactored 1.3
	 * @param  int $order_id
	 * @return array
	 */
	public function order_complete( $order_id ) {
		global $wpdb;

		// WooCommerce 2.2 compatibility
		// https://github.com/woothemes/woocommerce/pull/5558
		if ( function_exists( 'wc_get_order' ) ) {
			$order = wc_get_order( $order_id );
		} else {
			$order = new WC_Order( $order_id );
		}

		if ( count( $order->get_items() ) > 0 ) {

			foreach ( $order->get_items() as $item ) {

				$item_product_id = ( isset( $item['product_id'] ) ) ? $item['product_id'] : $item['id'];

				$item_variation_id = ( empty( $item['variation_id'] ) ) ? '' : $item['variation_id'];

				// verify this is a variable product
				if ( ! empty( $item_variation_id ) ) {

					$meta = get_post_custom( $item_variation_id );

				} else {

					$meta = get_post_custom( $item_product_id );

				}

				$parent_meta = get_post_custom( $item_product_id );

				// verify this is a downloadable API product
				$downloadable_product 	= ( ! empty( $meta['_downloadable'][0] ) && $meta['_downloadable'][0] == 'yes' ) ? true : false;
				$api_product 			= ( ! empty( $parent_meta['_is_api'][0] ) && $parent_meta['_is_api'][0] == 'yes' ) ? true : false;

				if ( $downloadable_product && $api_product ) {

					$quantity = isset( $item['item_meta']['_qty'][0] ) ? absint( $item['item_meta']['_qty'][0] ) : 1;

					// Determine if the parent or variable (child) title should be used
					if ( isset( $meta['_api_software_title_parent'][0] ) && $meta['_api_software_title_parent'][0] != '' && ! empty( $meta['_api_software_title_parent'][0] ) ) {

						$software_title_parent = $meta['_api_software_title_parent'][0];
						$software_title = $meta['_api_software_title_parent'][0];

					} else if ( isset( $meta['_api_software_title_var'][0] ) && $meta['_api_software_title_var'][0] != '' && ! empty( $meta['_api_software_title_var'][0] ) ) {

						$software_title = $meta['_api_software_title_var'][0];

					} else {

						$software_title = '';
					}

					for ( $i = 0; $i < $quantity; $i++ ) {

						$api_key = $order->order_key . '_am_' . $this->generate_key( 12, false );

						// Saves user order meta data
						$meta_data = $this->get_user_order_array(
										$api_key,
										$order->user_id,
										$order_id,
										$order->order_key,
										$order->billing_email,
										( ! empty( $software_title_parent ) ) ? $software_title_parent : '',
										( ! empty( $meta['_api_software_title_var'][0] ) ) ? $meta['_api_software_title_var'][0] : '',
										$software_title,
										$item_product_id,
										$item_variation_id,
										$meta['_api_new_version'][0],
										( ! empty( $meta['_api_activations'][0] ) ) ? $meta['_api_activations'][0] : '',
										( ! empty( $meta['_api_activations_parent'][0] ) ) ? $meta['_api_activations_parent'][0] : '',
										'yes',
										( empty( $item['variation_id'] ) ) ? 'no' : 'yes',
										'',
										'',
										current_time( 'mysql' ),
										'',
										$api_key
									);

						$this->save_order_complete_user_meta( $meta_data, $api_key, $i );

					}

				} // end if is_downloadable

			} // end foreach

		} // end if there is an order

	}

	/**
	 * Email API License Key and API License Email after order complete
	 *
	 * @access public
	 * @return void
	 */
	public function email_license_keys( $order ) {

		$current_info = $this->get_users_data( $order->user_id );

		wc_get_template( 'email-api-keys-order-complete.php', array(
			'order_id' 	=> $order->id,
			'order_key' => $order->order_key,
			'keys'		=> $current_info
		), 'woocommerce-api-manager', WCAM()->plugin_path() . '/templates/' );

	}

	/**
	 * Adds user_meta info for download permissions query to the database
	 *
	 * @access public
	 * @return void
	 */
	public function save_order_complete_user_meta( $data, $api_key, $loop ) {
		global $wpdb;

		$current_info = $this->get_users_data( $data[$api_key]['user_id'] );

		$new_info = WCAM()->array->array_merge_recursive_associative( $data, $current_info );

		update_user_meta( $data[$api_key]['user_id'], $wpdb->get_blog_prefix() . $this->user_meta_key_orders, $new_info );

		// Adds the API License Key to the post meta order, so it can be found in shop order search
		update_post_meta( $data[$api_key]['order_id'], '_api_license_key_' . $loop, $api_key );

	}

	/**
	 * Gets the user order info stored in an array for the $user_id, aka $object_id
	 *
	 * @since 1.1
	 * @param $user_id int
	 * @return array
	 */
	public function get_users_data( $user_id = 0 ) {
		global $wpdb;

		if ( $user_id === 0 ) {
			$data = array();
			return $data;
		}

		$data = get_metadata( 'user', $user_id, $wpdb->get_blog_prefix() . $this->user_meta_key_orders, true );

		if ( empty( $data ) ) {
			$data = array();
		}

		return $data;
	}

	/**
	 * Gets the user order info stored in an array for the $user_id, aka $object_id
	 *
	 * @since 1.1
	 * @param $user_id int
	 * @return array
	 */
	public function get_users_activation_data( $user_id = 0, $order_key = 0 ) {
		global $wpdb;

		if ( $user_id === 0 ) {
			$data = array();
			return $data;
		}

		if ( $order_key === 0 ) {
			$data = array();
			return $data;
		}

		$data = get_metadata( 'user', $user_id, $wpdb->get_blog_prefix() . $this->user_meta_key_activations . $order_key, true );

		if( empty( $data ) ) {
			$data = array();
		}

		return $data;
	}

	/**
	 * Gets the order info from the postmeta table with the $post_id, aka $object_id
	 *
	 * @since 1.1
	 * @param $user_id int
	 * @return array
	 */
	public function get_postmeta_data( $post_id = 0 ) {
		global $wpdb;

		if ( $post_id === 0 ) {
			$data = array();
			return $data;
		}

		$data = get_metadata( 'post', $post_id, '', true );

		if ( empty( $data ) ) {
			$data = array();
		}

		return $data;
	}

	/**
	 * Find file download id (am_key)
	 *
	 * @access public
	 * @param $post_id (integer)
	 * @return string (limited to first value in array)
	 */
	public function get_download_id( $post_id ) {

		$file_path = get_post_meta( $post_id, '_downloadable_files', true );

		if ( is_array( $file_path ) ) {
			foreach ( $file_path as $key => $value ) {
				$path[] = $key;
			}
		}

		if ( empty( $path[0] )  ) {
			return false;
		}

		return $path[0];
	}

	/**
	 * Find file download URL
	 * @since 1.3.2
	 * @access public
	 * @param $post_id (integer)
	 * @return string 'file' (limited to first value in array)
	 */
	public function get_download_url( $post_id ) {

		$file_path = get_post_meta( $post_id, '_downloadable_files', true );

		if ( is_array( $file_path ) ) {
			foreach ( $file_path as $key => $value ) {
				$path[] = $value;
			}
		}

		if ( empty( $path[0] ) ) {
			return false;
		}

		if ( empty( $path[0]['file'] ) ) {
			return false;
		}

		return $path[0]['file'];
	}

	/**
	 * Find file download filename
	 * @since 1.3.3
	 * @access public
	 * @param $post_id (integer)
	 * @return string 'file' (limited to first value in array)
	 */
	public function get_download_filename( $post_id ) {

		$file_path = get_post_meta( $post_id, '_downloadable_files', true );

		if ( is_array( $file_path ) ) {
			foreach ( $file_path as $key => $value ) {
				$path[] = $value;
			}
		}

		if ( empty( $path[0] ) ) {
			return false;
		}

		if ( empty( $path[0]['name'] ) ) {
			return false;
		}

		return $path[0]['name'];
	}

	/**
	 * Get the Country Code for an order
	 * @since 1.3.4
	 * @param  int $post_id (same as order_id)
	 * @return string
	 */
	public function get_billing_country( $post_id ) {

		return get_post_meta( $post_id, '_billing_country', true );

	}

	/**
	 * Formats an Amazon S3 URL for secure Query String Request Authentication to the S3 REST API
	 * Works with either path-style request (s3.amazonaws.com/bucket), or virtual hosted-style (bucket.s3.amazonaws.com).
	 * @since 1.3.2
	 * @param  string $url
	 * @return string
	 *
	 * http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html#RESTAuthenticationQueryStringAuth
	 * http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html#RESTAuthenticationExamples
	 * http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html
	 * http://docs.aws.amazon.com/AmazonS3/latest/UG/Introduction.html
	 */
	public function format_secure_s3_url( $url, $expire = false ) {

		if ( ! empty( $url ) ) {

			$secret_key = WCAM()->encrypt->decrypt( get_option( 'woocommerce_api_manager_amazon_s3_secret_access_key' ) );

			if ( $expire === false ) {

				$expire = time() + ( get_option( 'woocommerce_api_manager_url_expire' ) * MINUTE_IN_SECONDS );

			}

			$objectpath = parse_url( $url, PHP_URL_PATH );

			$signature = utf8_encode( "GET\n\n\n$expire\n" . $objectpath );

			$hashed_signature = base64_encode( hash_hmac( 'sha1' ,$signature , $secret_key , true ) );

			$query_string = array(
				'AWSAccessKeyId'	=> get_option( 'woocommerce_api_manager_amazon_s3_access_key_id' ),
				'Expires'			=> $expire,
				'Signature'			=> $hashed_signature
			);

			return $url . '?' . http_build_query( $query_string, '', '&' );

		}

		return '';

	}

	/**
	 * Determines if this is an Amazon S3 URL
	 * @since 1.3.2
	 * @param  string $url
	 * @return boolean
	 */
	public function find_amazon_s3_in_url( $url ) {
		$result = preg_match( '!\b(amazonaws.com)\b!', $url );

		if ( $result == 1 ) {
			return true;
		}

		return false;
	}

	/**
	 * Replace My Account Available Downloads link with Amazon S3 link if it is listed in Downloadable
	 * Files File URL for the product
	 * @since 1.3.2
	 * @param  string $download_url
	 * @param  array $download
	 * @return string
	 */
	public static function my_acount_download_link( $download_url, $download ) {

		$url = $this->get_download_url( $download['product_id'] );

		if ( ! empty( $url ) && $this->find_amazon_s3_in_url( $url ) == true ) {

			return '<a href="' . esc_url( $url ) . '">' . $download['download_name'] . '</a>';

		} else {

			return '<a href="' . esc_url( $download['download_url'] ) . '">' . $download['download_name'] . '</a>';

		}
	}

	/**
	 * download_count function.
	 *
	 * @access public
	 * @param mixed $order_id
	 * @param mixed $order_key
	 * @return int|boolean
	 */
	public function get_download_count( $order_id, $order_key ) {
    	global $wpdb;

		$download_count = $wpdb->get_var( $wpdb->prepare( "
			SELECT download_count FROM {$wpdb->prefix}woocommerce_downloadable_product_permissions
			WHERE order_id = %s
			AND order_key = %s
			LIMIT 1
		", $order_id, $order_key ) );

		if ( isset( $download_count ) ) {

			return $download_count;

		}

		return false;
	}

	/**
	 * Returns row of data to determine downloads_remaining, and access_expires.
	 *
	 * @access public
	 * @param string $api_key
	 * @param string $activation_email
	 * @param int $product_id
	 * @param string $download_id (default is empty string)
	 * @return obj
	 */
	public function get_downloadable_data( $order_key, $activation_email, $product_id, $download_id = '' ) {
		global $wpdb;

		$sql = "
			SELECT product_id,order_id,downloads_remaining,user_id,download_count,access_expires,download_id
			FROM {$wpdb->prefix}woocommerce_downloadable_product_permissions
			WHERE user_email = %s
			AND order_key = %s
			AND product_id = %s
			AND download_id = %s";

		$args = array(
			$activation_email,
			$order_key,
			$product_id,
			$download_id
		);

		// Returns an Object
		$result = $wpdb->get_row( $wpdb->prepare( $sql, $args ) );

		if ( is_object( $result ) ) {
			return $result;
		}

		return false;

	}

	/**
	 * Builds a properly formated download url for secure software updates.
	 *
	 * @access public
	 * @param string $api_key
	 * @param string $activation_email
	 * @param int $product_id
	 * @param string $download_id (default is empty string)
	 * @return string
	 */
	public function create_url( $order_key, $activation_email, $product_id, $download_id = '', $user_id ) {
		global $wpdb;

		$sql = "
			SELECT product_id,order_id,downloads_remaining,user_id,download_count,access_expires,download_id
			FROM {$wpdb->prefix}woocommerce_downloadable_product_permissions
			WHERE user_email = %s
			AND order_key = %s
			AND product_id = %s";

		$args = array(
			$activation_email,
			$order_key,
			$product_id
		);

		if ( $download_id ) {
			// backwards compatibility for existing download URLs
			$sql .= " AND download_id = %s";
			$args[] = $download_id;
		}

		// Returns an Object
		$result = $wpdb->get_row( $wpdb->prepare( $sql, $args ) );

		if ( is_object( $result ) ) {

			// Cost value is between 4 and 31
			$hash_data = WCAM()->hash->wam_password_hash( null, null, array( 'cost' => 4 ), $user_id );

			$url_args = array(
				'am_download_file' 	=> $result->product_id,
				'am_order' 			=> $order_key,
				'am_email' 			=> $activation_email,
				'hname' 			=> $hash_data['hname'],
				'hkey' 				=> $hash_data['hkey'],
				'hexpires' 			=> $hash_data['hexpires']
			);

			if ( $result->download_id != '' ) {
				$url_args['am_key'] = $result->download_id;
			}

			return home_url( '/?' ) . http_build_query( $url_args, '', '&' );

		} else {

			return false;

		}

		return false;

	}

	/**
	 * Checks if a plugin is activated
	 *
	 * @since 1.1
	 */
	public static function is_plugin_active( $slug ) {
		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() )
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );

		return in_array( $slug, $active_plugins ) || array_key_exists( $slug, $active_plugins );
	}

	/**
	 * Checks if page content exists
	 *
	 * @since 1.1
	 */
	public function get_page_content( $page_obj ) {

		if ( isset( $page_obj  ) && is_object( $page_obj ) ) {

			if ( ! empty( $page_obj->post_content ) ) {

				return $page_obj->post_content;

			} else {

				return '';

			}

		} else {

			return '';

		}

	}

	/**
	 * Gets the status of a checkbox
	 *
	 * @since 1.1
	 * @param $product_id int
	 * @param $meta_key string
	 * @return boolean
	 */
	public function get_product_checkbox_status( $post_id, $meta_key ) {

		$result = get_post_meta( $post_id, $meta_key, true );

		if ( $result == 'yes' ) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * get_order_info_by_email_with_order_key Gets the user order info
	 * @param  string $activation_email license email
	 * @param  string $order_key        order key
	 * @return array                    array populated with user purchase info
	 */
	public function get_order_info_by_email_with_order_key( $activation_email, $order_key ) {

		if ( ! empty( $activation_email ) ) {

			$user = get_user_by( 'email', $activation_email ); // returns $user->ID

		} else {

			return false;

		}

		if ( ! is_object( $user ) ) {
			return false;
		}

		// Check if this is an order_key
		if ( ! empty( $order_key ) ) {

			$user_orders = $this->get_users_data( $user->ID );

			if ( is_array( $user_orders ) && ! empty( $user_orders[$order_key] ) ) {

				return $user_orders[$order_key]; // returns a single order info array identified by order_key

			} else {

				return false;

			}

		}

		return false;

	}

	/**
	 * Nonce URL
	 * @param  mixed $args string or array
	 * @see http://codex.wordpress.org/Function_Reference/add_query_arg
	 * @return string
	 * @since 1.2.1
	 */
	public function nonce_url( $args ) {

		$action_url = wp_nonce_url( add_query_arg( $args ) );

		return $action_url;

	}

	/**
	 * Finds the default order_ prefix or a unique prefix that was created
	 * by the woocommerce_generate_order_key filter
	 *
	 * example $uniqid = $this->get_uniqid_prefix( $product_post_meta['_order_key'][0], '_' );
	 *
	 * returns order_ if order_ was the prefix
	 *
	 * returns wc_ which is the permanent prefix for WooCommerce 2.1 or greater
	 *
	 * https://github.com/woothemes/woocommerce/issues/405
	 *
	 * @since 1.3
	 *
	 * @param  mixed $haystack
	 * @param  mixed $needle
	 * @return mixed
	 */
	public function get_uniqid_prefix( $haystack, $needle, $position_after_needle = 0 ) {

		$pos = stripos( $haystack, $needle ) + $position_after_needle;

		return trim( substr( $haystack, 0, $pos ) );

	}

	/**
	 * Strips the http:// or https:// prefix from a URL
	 *
	 * Prevents Apache from blocking URLs containing :, %3A, or %2F
	 * Apache exhibits this behavior as a bug, somtimes as default behavior and if
	 * AllowEncodedSlashes is set to Off|NoDecode.
	 * Apache as default may encode some values, such as : or $3A twice.
	 * https://issues.apache.org/bugzilla/show_bug.cgi?id=35256
	 * https://issues.apache.org/bugzilla/show_bug.cgi?id=34602
	 * https://issues.apache.org/bugzilla/show_bug.cgi?id=39746
	 * http://httpd.apache.org/docs/2.2/mod/core.html#allowencodedslashes
	 *
	 * @since 1.3
	 *
	 * @param  string $haystack URL
	 * @return string Shortened URL
	 *
	 */
	public function remove_url_prefix( $haystack ) {

		return preg_replace( '!\b((http?|https)://)\b!', '', $haystack );

	}

	/**
	 * Allows the customer to delete a domain name, or platform, activated for an order on their My Account dashbaord
	 * @since 1.2.1
	 *
	 * @return void or error message
	 */
	public function delete_my_account_url() {
		global $wpdb;

		if ( isset( $_GET['domain'] ) && isset( $_GET['instance'] ) && isset( $_GET['order_key'] ) && isset( $_GET['user_id'] ) && isset( $_GET['_wpnonce'] ) ) {

			if ( wp_verify_nonce( $_GET['_wpnonce'] ) === false ) {
				wc_add_notice( __( 'The domain name could not be deleted.', 'woocommerce-api-manager' ), 'error' );
			}

			$domain 	= sanitize_text_field( $_GET['domain'] );
			$instance 	= sanitize_text_field( $_GET['instance'] );
			$order_key 	= sanitize_text_field( $_GET['order_key'] );
			$user_id 	= intval( $_GET['user_id'] );
			$api_key 	= sanitize_text_field( $_GET['api_key'] );

			$current_info = $this->get_users_activation_data( $user_id, $order_key );

	    	if ( ! empty( $current_info ) ) {

				$active_activations = 0;

		    	foreach ( $current_info as $key => $activations ) {

		    		if ( $activations['activation_active'] == 1 && $order_key == $activations['order_key'] ) {

						$active_activations++;

		    		}

		    	}

	    		foreach ( $current_info as $key => $activation_info ) {

					if ( $activation_info['order_key'] == $api_key && $activation_info['activation_active'] == 1 && $activation_info['instance'] == $instance && $activation_info['activation_domain'] == $domain ) {

						// Delete the activation data array
		    			unset( $current_info[$key] );

			    		// Re-index the numerical array keys:
						$new_info = array_values( $current_info );

						update_user_meta( $user_id, $wpdb->get_blog_prefix() . $this->user_meta_key_activations . $order_key, $new_info );

						wp_safe_redirect( get_permalink( wc_get_page_id( 'myaccount' ) ) );

						break;

						exit();

					}

	    		} // end foreach

			}

		}

	}

	/**
	 * Gets the subscription status, i.e. active
	 * @since 1.3
	 *
	 * @param  int $user_id
	 * @param  int $post_id
	 * @param  string $order_id
	 * @param  int $product_id
	 * @return string or boolean
	 */
	public function get_subscription_status( $user_id, $post_id, $order_id, $product_id ) {
		global $wpdb;

		if ( isset( $user_id ) && isset( $post_id ) && isset( $order_id ) && isset( $product_id ) ) {

			if ( $this->is_plugin_active( 'woocommerce-subscriptions/woocommerce-subscriptions.php' ) && $this->get_product_checkbox_status( $post_id, '_api_is_subscription' ) === true ) {

				// For Subscriptions > 1.4
				if ( version_compare( WC_Subscriptions::$version, '1.4', '>=' ) ) {

					$subs_new = true;

					$subscriptions = array();

					$subscriptions = WC_Subscriptions_Manager::get_users_subscriptions( $user_id );

				} else {

					// For Subscriptions < 1.4

					$subs_new = false;

					$subscription_data = array();

					$subscription_data = get_user_meta( $user_id, "{$wpdb->prefix}woocommerce_subscriptions", true );

				}

			}

		}

		if ( ( ! empty( $subscriptions ) && is_array( $subscriptions ) ) || ( ! empty( $subscription_data ) && is_array( $subscription_data ) ) ) {

			/**
			 * Get the Subscription status
			 */

			// Subscriptions > 1.4
			if ( $subs_new === true && isset( $subscriptions[$order_id . '_' . $product_id]['status'] ) ) {

				// account in good standing has active status - Subscriptions > 1.4
				$status = $subscriptions[$order_id . '_' . $product_id]['status'];

				// provides the same information as above, but may be more efficient than WC_Subscriptions_Manager::get_users_subscriptions( $user_id )
				// $sub_status = $this->get_subscription_status_data( $order_id );

			// For Subscriptions < 1.4
			// In variable product subscription orders, the product id might be the parent or the child
			} else if ( $subs_new === false && isset( $subscription_data[$order_id . '_' . $product_id]['status'] ) ) {

				// account in good standing has active status - Subscriptions < 1.4
				$status = $subscription_data[$order_id . '_' . $product_id]['status']; // Subscriptions < 1.4

			// For Subscriptions < 1.4
			} else if ( isset( $subscription_data[$order_id . '_' . $post_id]['status'] ) ) {

				// account in good standing has active status - Subscriptions < 1.4
				$status = $subscription_data[$order_id . '_' . $post_id]['status']; // Subscriptions < 1.4

			} else {

				$status = false;

			}

		} else {

			// No subscription
			return false;

		}

		if ( ! empty( $status ) ) {

			// Subscription status, i.e. active
			return $status;

		} else {

			// No subscription
			return false;

		}

	}

	/**
	 * Determine subscription status using order_id = post_id
	 *
	 * This method is meant to prevent trial subscription orders from activating in the software API
	 *
	 * For Suscriptions > 1.4
	 *
	 * @param int order_id
	 * @since 1.1.1
	 * @return string subscription status
	 */
	public function get_subscription_status_data( $order_id ) {
		global $wpdb;

		$order_item_id = $wpdb->get_var( $wpdb->prepare( "
			SELECT order_item_id FROM {$wpdb->prefix}woocommerce_order_items
			WHERE order_id = %d
			LIMIT 1
		", $order_id ) );

		$status = $wpdb->get_var( $wpdb->prepare( "
			SELECT meta_value FROM {$wpdb->prefix}woocommerce_order_itemmeta
			WHERE order_item_id = %d
			AND meta_key = %s LIMIT 1
		", $order_item_id, '_subscription_status' ) );

		if ( ! empty( $status ) ) {

			return $status;

		} else {

			return false;

		}

	}

	/**
	 * Checks the status of an subscription and returns an error
	 * Also returns an error if a subscription status could not be determined
	 * @since 1.3
	 * @param  string $status
	 * @return array
	 */
	public function check_subscription_status( $status = '' ) {

		if ( $status == 'on-hold' ) {

			return array( 'hold_subscription' => 'hold_subscription' );

		} else if ( $status == 'cancelled' ) {

			return array( 'cancelled_subscription' => 'cancelled_subscription' );

		} else if ( $status == 'expired' ) {

			return array( 'exp_subscription' => 'exp_subscription' );

		} else if ( $status == 'switched' ) {

			return array( 'switched_subscription' => 'switched_subscription' );

		} else if ( $status == 'suspended' ) {

			return array( 'suspended_subscription' => 'suspended_subscription' );

		} else if ( $status == 'pending' ) {

			return array( 'pending_subscription' => 'pending_subscription' );

		} else if ( $status == 'trash' ) {

			return array( 'trash_subscription' => 'trash_subscription' );

		} else {

			return array( 'no_subscription' => 'no_subscription', 'no_key' => 'no_key' );

		}

	}

	/**
	 * Generates an array of postmeta key values so shop order search can find an order that contains an API License Key
	 * @param  array $search_fields default post meta_keys
	 * @return array merged array including new ap_license_keys
	 */
	public function searchable_api_license_key( $search_fields ) {

		$quantity = 100;

		for ( $i = 0; $i < $quantity; $i++ ) {

			$api_fields[] = '_api_license_key_' . $i;

		}

		return array_merge( $search_fields, $api_fields );

	}

	/***************
	 * Key Generator
	 * *************/
	private function rand( $min = 0, $max = 0 ) {
		global $rnd_value;

		// Reset $rnd_value after 14 uses
		// 32(md5) + 40(sha1) + 40(sha1) / 8 = 14 random numbers from $rnd_value
		if ( strlen( $rnd_value ) < 8 ) {

			if ( defined( 'WP_SETUP_CONFIG' ) ) {

				static $seed = '';

			} else {

				$seed = get_transient('random_seed');

			}

			$rnd_value = md5( uniqid(microtime() . mt_rand(), true ) . $seed );
			$rnd_value .= sha1($rnd_value);
			$rnd_value .= sha1($rnd_value . $seed);
			$seed = md5($seed . $rnd_value);

			if ( ! defined( 'WP_SETUP_CONFIG' ) ) {

				set_transient('random_seed', $seed);

			}

		}

		// Take the first 8 digits for our value
		$value = substr( $rnd_value, 0, 8 );

		// Strip the first eight, leaving the remainder for the next call to wp_rand().
		$rnd_value = substr( $rnd_value, 8 );

		$value = abs( hexdec( $value ) );

		// Some misconfigured 32bit environments (Entropy PHP, for example) truncate integers larger than PHP_INT_MAX to PHP_INT_MAX rather than overflowing them to floats.
		$max_random_number = 3000000000 === 2147483647 ? (float) "4294967295" : 4294967295; // 4294967295 = 0xffffffff

		// Reduce the value to be within the min - max range
		if ( $max != 0 ) {

			$value = $min + ( $max - $min + 1 ) * $value / ( $max_random_number + 1 );

		}

		return abs( intval( $value ) );
	}

	// Creates a unique API Key or instance ID
	public function generate_key( $length = 12, $special_chars = true, $extra_special_chars = false ) {

		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

		if ( $special_chars ) {

			$chars .= '!@#$%^&*()';

		}

		if ( $extra_special_chars ) {

			$chars .= '-_ []{}<>~`+=,.;:/?|';

		}

		$password = '';

		for ( $i = 0; $i < $length; $i++ ) {

			$password .= substr( $chars, $this->rand( 0, strlen( $chars ) - 1 ), 1 );

		}

		// random_password filter was previously in random_password function which was deprecated
		return $password;
	}

	/**
	 * Gets the last character from a string
	 * @since 1.3
	 * @param  string $string
	 * @param  int $offset Number of characters from the end of the string to return. Defaults to 1 character.
	 * @return string
	 */
	public function get_last_string_characters( $string, $offset = 1 ) {

		return substr( $string, -$offset );

	}

	/**
	 * Checks and cleans a URL, but does not add an http:// prefix if it doesn't have one.
	 * This is used for hosts that have security restrictions that block : or //.
	 *
	 * A number of characters are removed from the URL. If the URL is for displaying
	 * (the default behaviour) ampersands are also replaced. The 'clean_url' filter
	 * is applied to the returned cleaned URL.
	 *
	 * @since 1.3.6
	 * @uses wp_kses_bad_protocol() To only permit protocols in the URL set
	 *		via $protocols or the common ones set in the function.
	 *
	 * @param string $url The URL to be cleaned.
	 * @param array $protocols Optional. An array of acceptable protocols.
	 *		Defaults to 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet', 'mms', 'rtsp', 'svn' if not set.
	 * @param string $_context Private. Use esc_url_raw() for database usage.
	 * @return string The cleaned $url after the 'clean_url' filter is applied.
	 */
	public function esc_url_no_scheme( $url, $protocols = null, $_context = 'display' ) {
		$original_url = $url;

		if ( '' == $url )
			return $url;
		$url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
		$strip = array('%0d', '%0a', '%0D', '%0A');
		$url = _deep_replace($strip, $url);
		$url = str_replace(';//', '://', $url);

		// Replace ampersands and single quotes only when displaying.
		if ( 'display' == $_context ) {
			$url = wp_kses_normalize_entities( $url );
			$url = str_replace( '&amp;', '&#038;', $url );
			$url = str_replace( "'", '&#039;', $url );
		}

		if ( '/' === $url[0] ) {
			$good_protocol_url = $url;
		} else {
			if ( ! is_array( $protocols ) )
				$protocols = wp_allowed_protocols();
			$good_protocol_url = wp_kses_bad_protocol( $url, $protocols );
			if ( strtolower( $good_protocol_url ) != strtolower( $url ) )
				return '';
		}

		/**
		 * Filter a string cleaned and escaped for output as a URL.
		 *
		 * @since 1.3.6
		 *
		 * @param string $good_protocol_url The cleaned URL to be returned.
		 * @param string $original_url      The URL prior to cleaning.
		 * @param string $_context          If 'display', replace ampersands and single quotes only.
		 */
		return apply_filters( 'clean_url_no_scheme', $good_protocol_url, $original_url, $_context );
	}

	/**
	 * Performs esc_url_no_scheme() for database usage, but does not add an http:// prefix if it doesn't have one.
	 * This is used for hosts that have security restrictions that block : or //.
	 *
	 * @since 1.3.6
	 * @uses esc_url_no_scheme()
	 *
	 * @param string $url The URL to be cleaned.
	 * @param array $protocols An array of acceptable protocols.
	 * @return string The cleaned URL.
	 */
	public function esc_url_raw_no_scheme( $url, $protocols = null ) {
		return $this->esc_url_no_scheme( $url, $protocols, 'db' );
	}

	/**
	 * Verifies that API Manager data already exists
	 * @return boolean returns true if data exists
	 * @since 1.3
	 */
	public function check_am_data_exists() {
		global $wpdb;

		//Populates an array containing the ID of every user
		$id = $wpdb->get_col("
			SELECT ID
			FROM {$wpdb->prefix}users
		" );

		if ( ! empty( $id ) ) {

			foreach ( $id as $key => $user_id ) {

				$data = get_user_meta( $user_id, $wpdb->get_blog_prefix() . 'wc_am_orders' );

				if ( ! empty( $data ) ) {

					break;

					return true;

				} else {

					return false;

				}

			}

		}

	}

	/**
	 * Returns a properly constructed user order data array
	 * @since 1.3
	 *
	 * @param  int $user_id
	 * @param  int $order_id
	 * @param  string $order_key
	 * @param  string $license_email
	 * @param  string $_api_software_title_parent
	 * @param  string $_api_software_title_var
	 * @param  string $software_title
	 * @param  int $parent_product_id
	 * @param  int $variable_product_id
	 * @param  string $current_version
	 * @param  int $_api_activations
	 * @param  int $_api_activations_parent
	 * @param  string $_api_update_permission
	 * @param  string $is_variable_product
	 * @param  string $license_type
	 * @param  string $expires
	 * @param  string $_purchase_time
	 * @param  string $_api_was_activated
	 * @param  string $api_key
	 * @return array
	 */
	public function get_user_order_array(
										$array_key,
										$user_id,
										$order_id,
										$order_key,
										$license_email,
										$_api_software_title_parent,
										$_api_software_title_var,
										$software_title,
										$parent_product_id,
										$variable_product_id,
										$current_version,
										$_api_activations,
										$_api_activations_parent,
										$_api_update_permission,
										$is_variable_product,
										$license_type,
										$expires,
										$_purchase_time,
										$_api_was_activated,
										$api_key
										) {

		$order_data =
				array( empty( $array_key ) ? '' : sanitize_text_field( $array_key ) =>
					array(
						'user_id'						=> empty( $user_id ) ? '' : absint( $user_id ),
						'order_id' 						=> empty( $order_id ) ? '' : absint( $order_id ),
						'order_key' 					=> empty( $order_key ) ? '' : sanitize_text_field( $order_key ),
						'license_email' 				=> empty( $license_email ) ? '' : sanitize_email( $license_email ),
						'_api_software_title_parent' 	=> empty( $_api_software_title_parent ) ? '' : sanitize_text_field( $_api_software_title_parent ),
						'_api_software_title_var' 		=> empty( $_api_software_title_var ) ? '' : sanitize_text_field( $_api_software_title_var ),
						'software_title' 				=> empty( $software_title ) ? '' : sanitize_text_field( $software_title ),
						'parent_product_id'				=> empty( $parent_product_id ) ? '' : absint( $parent_product_id ),
						'variable_product_id'			=> empty( $variable_product_id ) ? '' : absint( $variable_product_id ),
						'current_version'				=> empty( $current_version ) ? '' : sanitize_text_field( $current_version ),
						'_api_activations'				=> empty( $_api_activations ) ? '' : absint( $_api_activations ),
						'_api_activations_parent'		=> empty( $_api_activations_parent ) ? '' : absint( $_api_activations_parent ),
						'_api_update_permission'		=> empty( $_api_update_permission ) ? '' : sanitize_text_field( $_api_update_permission ),
						'is_variable_product'			=> empty( $is_variable_product ) ? '' : sanitize_text_field( $is_variable_product ),
						'license_type'					=> empty( $license_type ) ? '' : sanitize_text_field( $license_type ),
						'expires'						=> empty( $expires ) ? '' : sanitize_text_field( $expires ),
						'_purchase_time'				=> empty( $_purchase_time ) ? '' : sanitize_text_field( $_purchase_time ),
						'_api_was_activated'			=> empty( $_api_was_activated ) ? '' : sanitize_text_field( $_api_was_activated ),
						'api_key'						=> empty( $api_key ) ? '' : sanitize_text_field( $api_key ),
						)
					);

		return $order_data;

	}


} // End class
