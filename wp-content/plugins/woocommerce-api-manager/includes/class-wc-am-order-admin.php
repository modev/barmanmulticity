<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce API Manager Order Admin Class
 *
 * @package API Manager/Order Admin
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.1.1
 *
 */

class WC_API_Manager_Order_Admin {

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	public function __construct() {

		//AJAX
		add_action( 'wp_ajax_wc_api_manager_delete_activation', array( $this, 'delete_activation' ) );
		add_action( 'wp_ajax_wc_api_manager_add_api_licence_key', array( $this, 'add_api_licence_key' ) );
		add_action( 'wp_ajax_wc_api_manager_delete_api_licence_key', array( $this, 'delete_api_licence_key' ) );

		//Hooks
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
		add_action( 'woocommerce_process_shop_order_meta', array( $this, 'order_save_data' ) );

	}

	public function add_meta_boxes() {

		add_meta_box( 'wc_am_update_api_keys', __( 'API Key Products', 'woocommerce-api-manager' ), array( $this, 'licence_keys_meta_box' ), 'shop_order', 'normal', 'high' );
		add_meta_box( 'wc_am_api_key_activations', __( 'API Key Activations', 'woocommerce-api-manager' ), array( $this, 'activation_meta_box' ), 'shop_order', 'normal', 'high' );
	}

	/**
	 * Delete a activation via ajax
	 * @since 1.2.1
	 * @return void
	 */
	public function delete_activation() {

		check_ajax_referer( 'am-delete-activation', 'security' );

		global $wpdb;

		$domain 	= sanitize_text_field( $_POST['domain'] );
		$instance 	= sanitize_text_field( $_POST['instance'] );
		$order_key 	= sanitize_text_field( $_POST['order_key'] );
		$user_id 	= intval( $_POST['user_id'] );

		$current_info = WCAM()->helpers->get_users_activation_data( $user_id, $order_key );

    	if ( ! empty( $current_info ) ) {

			$active_activations = 0;

	    	foreach ( $current_info as $key => $activations ) {

	    		if ( $activations['activation_active'] == 1 && $order_key == $activations['order_key'] ) {

					$active_activations++;

	    		}

	    	}

    		foreach ( $current_info as $key => $activation_info ) {

				if ( $activation_info['activation_active'] == 1 && $activation_info['activation_active'] == 1 && $activation_info['instance'] == $instance && $activation_info['activation_domain'] == $domain ) {

					// Delete the activation data array
	    			unset( $current_info[$key] );

		    		// Re-index the numerical array keys:
					$new_info = array_values( $current_info );

					update_user_meta( $user_id, $wpdb->get_blog_prefix() . WCAM()->helpers->user_meta_key_activations . $order_key, $new_info );

					$re_check_info = WCAM()->helpers->get_users_activation_data( $user_id, $order_key );

					if ( empty( $re_check_info ) ) {

						delete_user_meta( $user_id, $wpdb->get_blog_prefix() . WCAM()->helpers->user_meta_key_activations . $order_key );

					}

					break;

					wp_die();

				}

    		} // end foreach

		}

		wp_die();

	}

	/**
	 * Deletes an API License Key, and the data associated with it, from an order via AJAX
	 * @since 1.3
	 * @return void
	 */
	public function delete_api_licence_key() {

		check_ajax_referer( 'am-delete-key', 'security' );

		global $wpdb;

		$user_id 	= intval( $_POST['user_id'] );
		$order_id 	= intval( $_POST['order_id'] );
		$loop 		= intval( $_POST['loop'] );
		$api_key 	= sanitize_text_field( $_POST['api_key'] );
		$title 		= sanitize_text_field( $_POST['software_title'] );
		$email 		= sanitize_text_field( $_POST['license_email'] );

		$user_orders = WCAM()->helpers->get_users_data( $user_id );

		if ( ! empty( $user_orders ) && is_array( $user_orders ) ) {

			foreach ( $user_orders as $key => $order_data ) {

				// Software Title
				if ( $order_data['is_variable_product'] == 'no' ) {

					$software_title = $order_data['_api_software_title_parent'];

				} else if ( $order_data['is_variable_product'] == 'yes' ) {

					$software_title = $order_data['_api_software_title_var'];

				} else {

					$software_title = $order_data['software_title'];

				}

				// Check if this is an order_key, or the new longer api_key
				$order_api_key = ( ! empty( $order_data['api_key'] ) ) ? $order_data['api_key'] : $order_data['order_key'];

				if ( $api_key == $order_api_key && $title == $software_title && $email == $order_data['license_email'] && $order_id == $order_data['order_id'] ) {

					// Delete the API Key from the postmeta, so it will no longer be found in shop order searches
					delete_post_meta( $order_id, '_api_license_key_' . $loop );

					// Delete the API License Key and associated data array
	    			unset( $user_orders[$key] );

	    			update_user_meta( $user_id, $wpdb->get_blog_prefix() . WCAM()->helpers->user_meta_key_orders, $user_orders );

					$re_check_user_orders = WCAM()->helpers->get_users_data( $user_id );

					if ( empty( $re_check_user_orders ) ) {

						delete_user_meta( $user_id, $wpdb->get_blog_prefix() . WCAM()->helpers->user_meta_key_orders );

					}

					break;

					wp_die();

				}

			} // end foreach

		}

		wp_die();

	}

	/**
	 * Adds an API License Key to an order via AJAX
	 * @since 1.3
	 * @return string The HTML template to display the API License Key data
	 */
	public function add_api_licence_key() {

		check_ajax_referer( 'add-api-license-key', 'security' );

		$order_id 	= absint( $_POST['order_id'] );
		$product_id = absint( $_POST['product_id'] );
		$i 			= absint( $_POST['loop'] );

		$post_data = WCAM()->helpers->get_postmeta_data( $order_id );

		if ( ! empty( $post_data ) && ! empty( $order_id ) && ! empty( $product_id ) ) {

			$variation_id = get_post_meta( $product_id, 'variable_product_id', true );

			$parent_id = get_post_meta( $product_id, 'parent_product_id', true );

			if ( ! empty( $variation_id ) ) {

				$meta = get_post_custom( $variation_id );

			} else {

				$meta = get_post_custom( $parent_id );

			}

			if ( ! empty( $meta ) && $meta['_downloadable'][0] == 'yes' ) {

				// Determine if the parent or variable (child) title should be used
				if ( isset( $meta['_api_software_title_parent'][0] ) && $meta['_api_software_title_parent'][0] != '' && ! empty( $meta['_api_software_title_parent'][0] ) ) {

					$software_title_parent = $meta['_api_software_title_parent'][0];
					$software_title = $meta['_api_software_title_parent'][0];

				} else if ( isset( $meta['_api_software_title_var'][0] ) && $meta['_api_software_title_var'][0] != '' && ! empty( $meta['_api_software_title_var'][0] ) ) {

					$software_title_var = $meta['_api_software_title_var'][0];
					$software_title = $meta['_api_software_title_var'][0];

				} else {

					$software_title = '';
				}

				if ( empty( $post_data['_order_key'][0] ) ) {
					wp_die();
				}

				$api_key = $post_data['_order_key'][0] . '_am_' . WCAM()->helpers->generate_key( 12, false );

				// Saves user order meta data
				$meta_data = WCAM()->helpers->get_user_order_array(
								$api_key,
								$post_data['_customer_user'][0],
								$order_id,
								$post_data['_order_key'][0],
								$post_data['_billing_email'][0],
								empty( $software_title_parent ) ? '' : sanitize_text_field( $software_title_parent ),
								empty( $software_title_var ) ? '' : sanitize_text_field( $software_title_var ),
								empty( $software_title ) ? '' : sanitize_text_field( $software_title ),
								empty( $parent_id ) ? '' : absint( $parent_id ),
								empty( $variation_id ) ? '' : absint( $variation_id ),
								empty( $meta['_api_new_version'][0] ) ? '' : sanitize_text_field( $meta['_api_new_version'][0] ),
								empty( $meta['_api_activations'][0] ) ? '' : absint( $meta['_api_activations'][0] ),
								empty( $meta['_api_activations_parent'][0] ) ? '' : absint( $meta['_api_activations_parent'][0] ),
								'yes',
								( empty( $variation_id ) ) ? 'no' : 'yes',
								empty( $license_type ) ? '' : sanitize_text_field( $license_type ),
								empty( $expires ) ? '' : sanitize_text_field( $expires ),
								empty( $post_data['_completed_date'][0] ) ? '' : sanitize_text_field( $post_data['_completed_date'][0] ),
								empty( $meta['_api_was_activated'][0] ) ? '' : sanitize_text_field( $meta['_api_was_activated'][0] ),
								$api_key
							);

				// Sets the count for the shop order searchable postmeta _api_license_key_ prefix.
				$loop = $i;
				$loop++;

				WCAM()->helpers->save_order_complete_user_meta( $meta_data, $api_key, $loop );

			} // end if is_downloadable

			// Get the order data array
			$data = $meta_data[$api_key];

			if ( ! empty( $data ) ) {

				// Get the old order_key or the new API License Key
				$order_data_key = ( ! empty( $data['api_key'] ) ) ? $data['api_key'] : $data['order_key'];

				// Get activation info
				$current_info = WCAM()->helpers->get_users_activation_data( $data['user_id'], $data['order_key'] );

				$active_activations = 0;

		    	if ( ! empty( $current_info ) ) foreach ( $current_info as $key => $activations ) {

		    		if ( $activations['activation_active'] == 1 && $order_data_key == $activations['order_key'] ) {

						$active_activations++;

		    		}

		    	}

		    	$num_activations = ( $active_activations  > 0 ) ? $active_activations : 0;

				// Activations limit or unlimited
				if ( $data['is_variable_product'] == 'no' && $data['_api_activations_parent'] != '' )
					$activations_limit = absint( $data['_api_activations_parent'] );
				else if ( $data['is_variable_product'] =='no' && $data['_api_activations_parent'] == '' )
					$activations_limit = 'unlimited';
				else if ( $data['is_variable_product'] == 'yes' && $data['_api_activations'] != '' )
					$activations_limit = absint( $data['_api_activations'] );
				else if ( $data['is_variable_product'] == 'yes' && $data['_api_activations'] == '' )
					$activations_limit = 'unlimited';

				// Software Title
				if ( $data['is_variable_product'] == 'no' )
					$software_title = sanitize_text_field( $data['_api_software_title_parent'] );
				else if ( $data['is_variable_product'] == 'yes' )
					$software_title = sanitize_text_field( $data['_api_software_title_var'] );
				else
					$software_title = sanitize_text_field( $data['software_title'] );

			} // end if $data

		} // end if $order_id and $product_id not empty

		// Sets the form name attribute array index key integer
		$i++;

		include( WCAM()->plugin_path() . '/admin/post-types/meta-boxes/views/html-order-api-key.php' );

		wp_die();

	}

	/**
	 * Get product name with SKU or ID. Used within admin.
	 *
	 * @access public
	 * @since 1.3
	 * @uses WC_Product class object
	 * @param mixed $product
	 * @return string Formatted product name
	 */
	public function get_formatted_name( $product_object ) {

		if ( $product_object->get_sku() ) {
			$identifier = $product_object->get_sku();
		} else {
			$identifier = '#' . $product_object->id;
		}

		return sprintf( __( '%s &ndash; %s', 'woocommerce-api-manager' ), $product_object->get_title(), $identifier );

	}

	/**
	 * Order notes meta box
	 */
	public function licence_keys_meta_box() {
		global $post, $wpdb;

		$post_data = WCAM()->helpers->get_postmeta_data( $post->ID );

		/**
		 * Renewal subscription orders should not have an API Key.
		 * Customer cancelled then renewed orders will have an API Key generated for the new order,
		 * because the new order isn't a renewal order, it is an initial order for a new subscription.
		 * @since 1.3.7
		 */
		$sub_status = WCAM()->helpers->get_subscription_status_data( $post->ID );

		if ( $sub_status != 'active' && ! empty( $post_data['_original_order'] ) ) {
			?><p style="padding:0 8px;"><?php _e( 'This is a subscription renewal order that should not contain any API Keys. Only the Initial Order should contain the active API Key.<br>Any API Keys listed on this subscription renewal order should be deleted, along with any API Key activations.', 'woocommerce-api-manager' ) ?></p><?php
		}

		if ( ! empty( $post_data ) ) {

			$user_id = $post_data['_customer_user'][0];

		}

		if ( ! empty( $user_id ) ) {

			$user_orders = WCAM()->helpers->get_users_data( $user_id );

			$i = 0;

			?>

			<div class="api_order_licence_keys wc-metaboxes-wrapper">
				<?php

				// Sort API Keys from low to high value
				ksort( $user_orders );

				foreach ( $user_orders as $order_key => $data ) {

					// Get the old order_key or the new API License Key
					$order_data_key = ( ! empty( $data['api_key'] ) ) ? $data['api_key'] : $data['order_key'];

					if ( ! empty( $data ) ) {

						// Get the order_key portion of the API License Key
						$order_key_prefix = WCAM()->helpers->get_uniqid_prefix( $order_data_key, '_am_' );

						$post_meta_order_key_prefix = WCAM()->helpers->get_uniqid_prefix( $order_key, '_am_' );

						/**
						 * Get the order_key portion of the API License Key and match that with the post meta order_key
						 * Also match the post meta post_id with the order_id, which are the same thing with a different name
						 */
						if ( $order_key_prefix == $post_meta_order_key_prefix && $post->ID == $data['order_id'] ) {

						// Get activation info
						$current_info = WCAM()->helpers->get_users_activation_data( $data['user_id'], $data['order_key'] );

						$active_activations = 0;

				    	if ( ! empty( $current_info ) ) foreach ( $current_info as $key => $activations ) {

				    		if ( $activations['activation_active'] == 1 && $order_data_key == $activations['order_key'] ) {

								$active_activations++;

				    		}

				    	}

				    	$num_activations = ( $active_activations  > 0 ) ? $active_activations : 0;

						// Activations limit or unlimited
						if ( $data['is_variable_product'] == 'no' && $data['_api_activations_parent'] != '' )
							$activations_limit = absint( $data['_api_activations_parent'] );
						else if ( $data['is_variable_product'] =='no' && $data['_api_activations_parent'] == '' )
							$activations_limit = 'unlimited';
						else if ( $data['is_variable_product'] == 'yes' && $data['_api_activations'] != '' )
							$activations_limit = absint( $data['_api_activations'] );
						else if ( $data['is_variable_product'] == 'yes' && $data['_api_activations'] == '' )
							$activations_limit = 'unlimited';

						// Software Title
						if ( $data['is_variable_product'] == 'no' )
							$software_title = sanitize_text_field( $data['_api_software_title_parent'] );
						else if ( $data['is_variable_product'] == 'yes' )
							$software_title = sanitize_text_field( $data['_api_software_title_var'] );
						else
							$software_title = sanitize_text_field( $data['software_title'] );

						$i++;

						include( WCAM()->plugin_path() . '/admin/post-types/meta-boxes/views/html-order-api-key.php' );

						} // end if order_key and post_id match for this order

					} // end if $data

				} // end foreach

				?>

				<style>
					/*
					.chzn-container .chzn-results { clear: both; }
					.chzn-container, .chosen_select_nostd { width: 60% !important; }
					.chosen-results { width: 98% !important; }
					.chzn-drop { width: 98% !important; }
					*/
					.chosen-container, .chosen_select_nostd { width: 450px !important; }
					.chosen-results { width: 98% !important; }
					.chosen-drop { width: 98% !important; }
				</style>

				<div class="toolbar">

				<?php
				/**
				 * Don't display the add API Key button if this is a subscription renewal order
				 * @since 1.3.7
				 */
				if ( empty( $post_data['_original_order'] ) ) {

				?>
					<p class="buttons">
						<select name="add_api_license_key" class="add_api_license_key chosen_select_nostd" data-placeholder="<?php _e( 'Choose a downloadable software product &hellip;', 'woocommerce-api-manager' ) ?>">
							<?php
								echo '<option value=""></option>';

								$args = array(
									'post_type' 		=> 'product',
									'posts_per_page' 	=> -1,
									'post_status'		=> 'publish',
									'order'				=> 'ASC',
									'orderby'			=> 'title',
								);
								$products = get_posts( $args );

								if ( ! empty( $products ) ) :

									foreach ( $products as $product ) :

									// WooCommerce 2.2 compatibility
									if ( function_exists( 'wc_get_order' ) ) {
										$order = wc_get_order( $post->ID );
									} else {
										$order = new WC_Order( $post->ID );
									}

										if ( count( $order->get_items() ) > 0 ) :

											// Prevent products from displaying more than once if more than one of an order item exists
											$items = array_map( 'unserialize', array_unique( array_map( 'serialize', $order->get_items() ) ) );

											foreach ( $items as $item ) :

												$item_product_id = ( isset( $item['product_id'] ) ) ? $item['product_id'] : $item['id'];

												// Only display products that match  order items from this order
												if ( $item_product_id == $product->ID ) :

													$product_object = get_product( $product->ID );

													$is_api =  WCAM()->helpers->get_product_checkbox_status( $product->ID, '_is_api' );

													// Only display products that use the API Manager
													if ( ! empty( $is_api ) && $is_api ) :

														echo '<option value="' . esc_attr( $product->ID ) . '">' . esc_html( $this->get_formatted_name( $product_object ) ) . '</option>';

														$args_get_children = array(
															'post_type' 		=> 'product_variation',
															'posts_per_page' 	=> -1,
															'post_status'		=> 'publish',
															'order'				=> 'ASC',
															'orderby'			=> 'title',
															'post_parent'		=> $product->ID,
															'meta_query'		=> array(
																array(
																	'key' 	=> '_virtual',
																	'value' => 'yes',
																	'key' 	=> '_downloadable',
																	'value' => 'yes'
																)
															)
														);

														$child_products = get_children( $args_get_children);

														if ( ! empty( $child_products ) ) :

															foreach ( $child_products as $child ) :

																echo '<option value="' . esc_attr( $child->ID ) . '">&nbsp;&nbsp;&mdash;&nbsp;' . esc_html( $child->post_title ) . '</option>';

															endforeach;

														endif;

													endif; // end if $is_api

												endif; // end if $item_product_id == $product->ID

											endforeach; // end foreach order_items

										endif; // end if count items > 0

									endforeach; // end foreach $products

								endif; // end if $products
							?>
						</select>

						<button type="button" class="button add_api_key"><?php _e( 'Add API Key', 'woocommerce-api-manager' ); ?></button>
						<div class="clear"></div>
					</p>

					<?php } // end if this is not a subscription renewal order ?>

				</div>

				<?php
				/**
				 * Javascript
				 */
				ob_start();
				?>
				/**
				 * Add API License Key
				 */
				jQuery( '.api_order_licence_keys' ).on('click', 'button.add_api_key', function() {

					var product 	= jQuery( 'select.add_api_license_key' ).val();

					if ( ! product ) return;

					jQuery( '.api_order_licence_keys' ).block( { message: null, overlayCSS: { background: '#fff url( <?php echo esc_url( WCAM()->plugin_url() ); ?>/assets/images/ajax-loader.gif ) no-repeat center', opacity: 0.6 } } );

					var data = {
						action: 		'wc_api_manager_add_api_licence_key',
						product_id: 	product,
						loop:			jQuery('.api_order_licence_keys .wc-metabox').size(),
						order_id: 		'<?php echo $post->ID; ?>',
						security: 		'<?php echo wp_create_nonce( "add-api-license-key" ); ?>'
					};

					jQuery.post( '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>', data, function( response ) {

						if ( response ) {

						    jQuery( '.api_order_licence_keys' ).prepend( response );

						} else {

							alert('<?php echo esc_js( __( 'Could not create the API License Key.', 'woocommerce-api-manager' ) ); ?>');

						}

						jQuery( '.api_order_licence_keys' ).unblock();

					});

					return false;

				});

				/**
				 * Delete API License Key
				 */
				jQuery('.api_order_licence_keys').on('click', 'button.am_delete_key', function( e ) {

					e.preventDefault();

					var answer = confirm( '<?php echo esc_js( __( 'Are you sure you want to delete this API Licence Key?', 'woocommerce-api-manager' ) ); ?>');

					if ( answer ) {

						var el		= jQuery(this).parent().parent();
						var api_key	= jQuery(this).attr('rel').split(',')[0];
						var title	= jQuery(this).attr('rel').split(',')[1];
						var email	= jQuery(this).attr('rel').split(',')[2];
						var loop 	= jQuery(this).attr('rel').split(',')[3];
						var userid	= jQuery(this).attr('rel').split(',')[4];
						var post_id	= jQuery(this).attr('rel').split(',')[5];

						var file = $(this).attr('rel').split(",")[1];

						if ( api_key ) {

							jQuery( el ).block({ message: null, overlayCSS: { background: '#fff url(<?php echo esc_url( WCAM()->plugin_url() ); ?>/assets/images/ajax-loader.gif ) no-repeat center', opacity: 0.6 } } );

							var data = {
								action: 		'wc_api_manager_delete_api_licence_key',
								api_key: 		api_key,
								software_title:	title,
								license_email:	email,
								loop:			loop,
								user_id:		userid,
								order_id: 		post_id,
								security: 		'<?php echo wp_create_nonce("am-delete-key"); ?>'
							};

							jQuery.post('<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>', data, function( response ) {

								jQuery( el ).fadeOut('300', function() {
									jQuery( el ).remove();
								});
							});

						} else {

							alert('<?php echo esc_js( __( 'Could not delete the API License Key.', 'woocommerce-api-manager' ) ); ?>');

							/*
							jQuery( el ).fadeOut('300', function(){
								jQuery( el ).remove();
							});
							*/
						}

					}

					return false;
				});

				/**
				 * Tooltip
				 */
			    jQuery( '.am_tooltip' ).tooltip({
			      position: {
			        my: "center bottom-20",
			        at: "center top",
			        using: function( position, feedback ) {
			          jQuery( this ).css( position );
			          jQuery( '<div>' )
			            .addClass( "arrow" )
			            .addClass( feedback.vertical )
			            .addClass( feedback.horizontal )
			            .appendTo( this );
			        }
			      }
			    });

			    /**
			     * Expand API Key Text Input on mouseover
			     */
				jQuery('.am_expand_text_box').mouseenter(function(){
					var $this = jQuery(this);
					if (!$this.data('expand')) {
						$this.data('expand', true);
						$this.animate({width:'+=140px',left:'-=6px'}, 'linear');
						$this.siblings('.s').animate({width:'-=140px',left:'+=6px'}, 'linear')
					}
					$this.focus();
				}).mouseleave(function(){
					var $this = jQuery(this);
					$this.data('expand', false);
					$this.animate({width:'-=140px',left:'+=6px'}, 'linear');
					$this.siblings('.s').animate({width:'+=140px',left:'-=6px'}, 'linear')
				});

				<?php
				$javascript = ob_get_clean();
				WCAM()->wc_print_js( $javascript );
				?>
			</div>
			<?php


		} // end if $user_id

	}

	/**
	 * adds activations meta box
	 *
	 * @since 1.0
	 * @param object $post the current post object
	 * @return void
	 */
	public function activation_meta_box( $post ) {
		global $wpdb;

		$post_data = WCAM()->helpers->get_postmeta_data( $post->ID );

		if ( ! empty( $post_data ) ) {

			$post_meta_order_key = ( isset( $post_data['_order_key'][0] ) ) ? $post_data['_order_key'][0] : '';
			$user_id = ( isset( $post_data['_customer_user'][0] ) ) ? $post_data['_customer_user'][0] : '';
			$email = ( isset( $post_data['_billing_email'][0] ) ) ? $post_data['_billing_email'][0] : '';

		}

		if ( ! empty( $user_id ) && ! empty( $post_meta_order_key ) ) {

			$activations = WCAM()->helpers->get_users_activation_data( $user_id, $post_meta_order_key );

			// Sort activations according to most recent activation time
			if ( $activations ) {

				foreach ( $activations as $key => $value ) {

					$time[$key] = $value['activation_time'];

				}

				if ( is_array( $time ) ) {

					array_multisort( $time, SORT_DESC, $activations );

				}

			}

			$num_activations = count( $activations );

		}

		if ( ! empty( $post_meta_order_key ) && ! empty( $user_id ) && ! empty( $email ) && $num_activations > 0 ) {

			include( WCAM()->plugin_path() . '/admin/post-types/meta-boxes/views/html-order-api-activations.php' );

			/**
			 * Javascript
			 */
			ob_start();
			?>
				jQuery( '#activations-table' ).on( 'click', 'button.delete_api_key', function( e ){
					e.preventDefault();
					var answer = confirm('<?php echo esc_js( __( 'Are you sure you want to delete this activation?', 'woocommerce-api-manager' ) ); ?>');
					if ( answer ){

						var el 			= jQuery( this ).parent().parent();
						var domain 		= jQuery( this ).attr( 'rel' );
						var instance 	= jQuery( this ).attr( 'id' );

						if ( domain ) {

							jQuery( el ).block({ message: null, overlayCSS: { background: '#fff url( <?php echo esc_url( WCAM()->plugin_url() ); ?>/assets/images/ajax-loader.gif ) no-repeat center', opacity: 0.6 } });

							var data = {
								action: 	'wc_api_manager_delete_activation',
								domain: 	domain,
								instance: 	instance,
								user_id: 	'<?php echo $user_id; ?>',
								order_key: 	'<?php echo $post_meta_order_key; ?>',
								security: 	'<?php echo wp_create_nonce( "am-delete-activation" ); ?>'
							};

							jQuery.post('<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>', data, function( response ) {
								// Success
								jQuery(el).fadeOut('300', function(){
									jQuery(el).remove();
								});
							});

						} else {
							jQuery( el ).fadeOut('300', function(){
								jQuery( el ).remove();
							});
						}

					}
					return false;
				});
			<?php
			$javascript = ob_get_clean();
			WCAM()->wc_print_js( $javascript );

		} else {
			?><p style="padding:0 8px;"><?php _e( 'No activations yet', 'woocommerce-api-manager' ) ?></p><?php
		}
	}

	/**
	 * saves the data inputed into the order boxes
	 *
	 * @see order_meta_box()
	 * @since 1.0
	 * @return void
	 */
	public function order_save_data( $post_id ) {
		global $wpdb, $post;

		if ( isset( $_POST['user_id'] ) && isset( $_POST['api_key'] ) ) {

			$user_id = stripslashes_deep( $_POST['user_id'] );

			$order_key = $_POST['api_key'];

			$max_loop = max( array_keys( $order_key ) );

			for ( $i = 0; $i <= $max_loop; $i ++ ) {

				if ( ! isset( $order_key[$i] ) ) continue;

				$update_permission = ! empty( $_POST['_api_update_permission'][ $i ] ) ? 'yes' : 'no';

				$license_email 				= ( ! empty( $_POST['license_email'][$i] ) ) ? $_POST['license_email'][$i] : '';
				$_api_software_title_parent = ( ! empty( $_POST['_api_software_title_parent'][$i] ) ) ? $_POST['_api_software_title_parent'][$i] : '';
				$_api_software_title_var 	= ( ! empty( $_POST['_api_software_title_var'][$i] ) ) ? $_POST['_api_software_title_var'][$i] : '';
				$current_version 			= ( ! empty( $_POST['current_version'][$i] ) ) ? $_POST['current_version'][$i] : '';
				$_api_activations 			= ( ! empty( $_POST['_api_activations'][$i] ) ) ? $_POST['_api_activations'][$i] : '';
				$_api_activations_parent 	= ( ! empty( $_POST['_api_activations_parent'][$i] ) ) ? $_POST['_api_activations_parent'][$i] : '';
				$api_key 					= ( ! empty( $_POST['api_key'][$i] ) ) ? $_POST['api_key'][$i] : '';

				// Get order info that can be manipulated and compared
				$current_info = WCAM()->helpers->get_users_data( $user_id[$i] );

				$info = WCAM()->helpers->get_users_data( $user_id[$i] );

				// Get order info that can be used to populate values
				$data = $info[$order_key[$i]];

				if ( ! empty( $data ) ) {

					if ( isset( $order_key[$i] ) ) {

						unset( $current_info[$order_key[$i]] );

						$update = WCAM()->helpers->get_user_order_array(
									$order_key[$i],
									$user_id[$i],
									$data['order_id'],
									$data['order_key'],
									$license_email,
									$_api_software_title_parent,
									$_api_software_title_var,
									$data['software_title'],
									$data['parent_product_id'],
									$data['variable_product_id'],
									$current_version ,
									$_api_activations,
									$_api_activations_parent,
									$update_permission,
									$data['is_variable_product'],
									'',
									'',
									$data['_purchase_time'],
									$data['_api_was_activated'],
									$api_key
								);

						$new_info = WCAM()->array->array_merge_recursive_associative( $update, $current_info );

						update_user_meta( $user_id[$i], $wpdb->get_blog_prefix() . WCAM()->helpers->user_meta_key_orders, $new_info );

					}

				}

			}

		}

	}

} // End of class

WC_API_Manager_Order_Admin::instance();
