<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WooCommerce API Manager Hash Class
 * Hashes passwords, checks hashed passwords against stored hashes, and manages hashed passwords in the database.
 *
 * @package API Manager/Hash
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.3.4
 *
 */
class WC_Api_Manager_Hash {

	private $passwordhash;

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	public function  __construct() {

		if ( version_compare( PHP_VERSION, '5.5.0', '<' ) && version_compare( PHP_VERSION, '5.3.7', '>=' ) ) {
			require_once( 'password.php' );
		}

		if ( version_compare( PHP_VERSION, '5.3.7', '<' ) ) {
			require_once( ABSPATH . 'wp-includes/class-phpass.php');
			// Cost is between 4 and 31
			$this->passwordhash = new PasswordHash( 4, true );
		}

	}

	/**
	 * Hash the password using the most secure PHP algorithm available.
	 * If PHP is 5.5 or greater the built-in password_hash is used.
	 * If PHP is equal to or greater than 5.3.7 and less than 5.5, the password compat functions are used.
	 * If PHP is less than 5.3.7, or if false is returned, the crypt function is used.
	 * https://github.com/ircmaxell/password_compat
	 * @param  string $password
	 * @param  string|const $algorithm
	 * @param  int $user_id
	 * @return array|boolean
	 */
	public function wam_password_hash( $password, $algorithm = '', $options = '', $user_id ) {

		$this->cleanup_hash();

		if ( empty( $password ) ) {
			$password = trim( WCAM()->helpers->generate_key( 16, false ) );
		}

		if ( version_compare( PHP_VERSION, '5.3.7', '<' ) ) {
			$hash = $this->passwordhash->HashPassword( $password );
			if ( ! empty( $hash ) ) {
				$hash_data = $this->add_hash( $user_id, $hash );
				if ( ! empty( $hash_data ) ) {
					return array( 'hname' => $hash_data['hash_name'], 'hkey' => $password, 'hexpires' => $hash_data['hash_time'] );
				}
			}
		}

		if ( function_exists( 'password_hash' ) ) {
			if ( empty( $algorithm ) ) {
				$algorithm = PASSWORD_DEFAULT;
			}

			$hash = password_hash( $password, $algorithm, $options );
			if ( ! empty( $hash ) ) {
				$hash_data = $this->add_hash( $user_id, $hash );
				if ( ! empty( $hash_data ) ) {
					return array( 'hname' => $hash_data['hash_name'], 'hkey' => $password, 'hexpires' => $hash_data['hash_time'] );
				} else {
					$hash = $this->passwordhash->HashPassword( $password );
					if ( ! empty( $hash ) ) {
						$hash_data = $this->add_hash( $user_id, $hash );
						if ( ! empty( $hash_data ) ) {
							return array( 'hname' => $hash_data['hash_name'], 'hkey' => $password, 'hexpires' => $hash_data['hash_time'] );
						}
					}
					return false;
				}
			}
		} else {
			$hash = $this->passwordhash->HashPassword( $password );
			if ( ! empty( $hash ) ) {
				$hash_data = $this->add_hash( $user_id, $hash );
				if ( ! empty( $hash_data ) ) {
					return array( 'hname' => $hash_data['hash_name'], 'hkey' => $password, 'hexpires' => $hash_data['hash_time'] );
				}
			}
		}

		return false;
	}

	/**
	 * Verify hashed password with password
	 * @param  string $password
	 * @param  string $hash
	 * @param  int $user_id
	 * @return boolean
	 */
	public function wam_password_verify( $password, $hash_name, $user_id ) {

		$this->cleanup_hash();

		$hash = $this->get_hash( $user_id, $hash_name );

		if ( empty( $hash ) ) {
			return false;
		}

		if ( version_compare( PHP_VERSION, '5.3.7', '<' ) ) {
			$check_hash = $this->passwordhash->CheckPassword( $password, $hash->hash_value );
			if ( $check_hash ) {
				$this->delete_hash( $hash->hash_value );
				return true;
			} else {
				return false;
			}
		}

		if ( function_exists( 'password_verify' ) ) {
			if ( password_verify( $password, $hash->hash_value ) ) {
				$this->delete_hash( $hash->hash_value );
				return true;
			} else {
				return false;
			}
		} else {
			$check_hash = $this->passwordhash->CheckPassword( $password, $hash->hash_value );
			if ( $check_hash ) {
				$this->delete_hash( $hash->hash_value );
				return true;
			} else {
				return false;
			}
		}

		return false;

	}

	/**
	 * Gets the hash from the database
	 * @param  int $user_id
	 * @param  string $hash_name
	 * @return object|boolean
	 */
    public function get_hash( $user_id, $hash_name ) {
    	global $wpdb;

		$sql = "
			SELECT hash_user_id,hash_name,hash_value,hash_time
			FROM {$wpdb->prefix}woocommerce_api_manager_secure_hash
			WHERE hash_user_id = %d
			AND hash_name = %s";

		$args = array(
			$user_id,
			$hash_name,
		);

		// Returns an Object
		$result = $wpdb->get_row( $wpdb->prepare( $sql, $args ) );

		if ( is_object( $result ) ) {
			return $result;
		}

		return false;

    }

    /**
     * Adds hash data row to the API Manager secure hash table
     * @param int $user_id
     * @param string $hash
     * @return array:boolean
     */
    public function add_hash( $user_id, $hash ) {
    	global $wpdb;

    	$hash_name = WCAM()->helpers->generate_key( 12, false );

    	// hash could contain up to 255 characters

	    $data = array(
	    	'hash_user_id'		=> $user_id,
	    	'hash_name' 		=> $hash_name,
			'hash_value' 		=> $hash,
			'hash_time' 		=> time() + ( get_option( 'woocommerce_api_manager_url_expire' ) * MINUTE_IN_SECONDS )
	    );

	    $format = array(
	    	'%d',
			'%s',
			'%s',
			'%d'
	    );

    	$inserted = $wpdb->insert( $wpdb->prefix . 'woocommerce_api_manager_secure_hash',
			$data,
			$format
        );

    	if ( $inserted ) {
    		return $data;
    	}

    	return false;

    }

    /**
     * Delete after using x number of times.
     * @param  string $hash_value The hashed password.
     * @return void
     */
    public function delete_hash( $hash_value ) {
    	global $wpdb;

		$sql =
			"
			DELETE FROM {$wpdb->prefix}woocommerce_api_manager_secure_hash
			WHERE hash_value = %s
			";

		$wpdb->query( $wpdb->prepare( $sql, $hash_value ) );
    }

    /**
     * Deletes expired hashes
     * @return void
     */
	public function cleanup_hash() {
		global $wpdb;

		$sql =
			"
			DELETE FROM {$wpdb->prefix}woocommerce_api_manager_secure_hash
			WHERE hash_time < %d
			";

		$wpdb->query( $wpdb->prepare( $sql, time() ) );

	}

	/**
	 * Check if the password has expired.
	 * @param  int  $expires
	 * @param  int  $user_id
	 * @param  string  $hash_name
	 * @return boolean
	 */
	public function is_expired( $expires, $user_id, $hash_name ) {

		$this->cleanup_hash();

		$hash_data = $this->get_hash( $user_id, $hash_name );

		if ( ! empty( $hash_data ) && $hash_data->hash_time == $expires ) {
			if ( $hash_data->hash_time < time() ) {
				return true;
			} else {
				return false;
			}
		}

		return true;

	}

}
