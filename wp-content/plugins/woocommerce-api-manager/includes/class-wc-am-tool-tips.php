<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Tool Tips Class
 *
 * @package API Manager/Tips
 * @author Todd Lahman LLC
 * @copyright   Copyright (c) Todd Lahman LLC
 * @since 1.0
 *
 */

class WC_API_Manager_Tool_Tips {

	/**
	 * @var The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * @static
	 * @return class instance
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) && ! is_object( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 */
	private function __clone() {}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 */
	private function __wakeup() {}

	public function tips( $tip ) {

		switch ( $tip ) {
			case 'deactivation':
				?>
				<span class='icon-pos'><a href='' class='tool-tip' title='<?php _e( 'A license key activates a single installation of the WooCommerce Update API Manager on a single blog. To move a license to another blog, the plugin license must be deactivated on the old blog installation, and reactivated with the license key and license email on the new blog. For multisite installations, each blog requires its own license key to activate the WooCommerce Update API Manager.', 'woocommerce-api-manager' ); ?>'><img src='<?php echo esc_url( WCAM()->plugin_url() ); ?>assets/images/icon-question.png' title=''' /></a></span>
				<?php
				break;
		}
	}

}
