=== Fix my posts! ===
Contributors: strackerphil
Donate link:  https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=5A98CHL224QG6
Tags:         admin, administration, database, import, images, meta
Requires at least: 3.0.1
Tested up to: 3.8.1
Stable tag:   trunk
License:      GPLv2 or later
License URI:  http://www.gnu.org/licenses/gpl-2.0.html

Absolutely cool plugin to repair all posts and images after data-import or when your database is messed up!

== Description ==

*This plugin is meant for admins/people that have basic understanding of how WordPress works.*

This is very useful after you import posts/images from another CMS via direct SQL or when you somehow messed up your posts/images.

The Plugin will process your posts one-by-one and repair some basic things when it finds a problem:

**General repair**

* Create a valid post_name for all posts

**Image repair**

* Fix image-metadata
* Creates missing image-thumbnails

*(The plugin uses core WordPress methods to fix images. So the result is exactly same, as if you re-uploaded the images via WordPress)*

**Custom repair**

Simply create an action handler to manually check each single post-item in the database and repair it if required!

`<?php
add_action( 'fix-the-post', 'your_handler' );
function your_handler( $the_post ) {
    $the_post->title = 'new title';
    wp_update_post( $post );
    echo 'Title updated!'; // You will see this in the repair-log on screen.
}
?>`


== Installation ==

1. Upload `fix-my-posts.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. You will get a new 'Fix my posts' menu-item in the 'Tools' menu

== Frequently Asked Questions ==

= Who is this plugin for? =

This plugin is meant for admins/people that have basic understanding of how WordPress works.
It is intended to help admins with setting up or fixing their WordPress sites

== Screenshots ==

1. That's the plugin screen. Nothing fancy, just a button and a progress bar...

2. **New in 1.2**: A repair log and a custom-repair hook!

== Changelog ==

= 1.2 =
* Updated the image-correction code by implementing changes suggested by Fred (http://www.adam-makes-websites.com/discoveries/fixing_messed_up_image_attachments_in_wordpress_that_have_no_postmeta_values)
* New action-hook added for custom repair actions.
* Display repair-log to the user.
* Some smaller bugfixes and improvements.

= 1.1 =
* Bugfix in the image-repair code.
* Added a cancel button to stop the current process-loop.

= 1.0 =
* Initial version released.

== Upgrade Notice ==

(no special notes)