<?php
/*
Plugin Name: Fix my posts!
Plugin URI:  http://wordpress.org/plugins/fix-my-posts
Description: This plugin can be useful after you import posts/images from another CMS via direct SQL or when you somehow messed up your posts/images. "Fix my posts" does multiple things: Sanitize all post-names / fix image-metadata / creates missing image-thumbnails.
Author:      Philipp Stracker
Version:     1.2
Author URI:  http://www.stracker.net/
Textdomain:  fixmyposts
*/

/*
 * We need this WordPress core-libaray to do the image repair actions.
 * @see FixMyPosts::fix_post()
 */
include_once ABSPATH . 'wp-admin/includes/image.php';

/**
 * This class encapsulates the whole plugin.
 * Only function that needs to be called is "FixMyPosts::init()".
 *
 * @since  1.0
 */
class FixMyPosts {
	/**
	 * The slug to the settings-page.
	 *
	 * @since  1.0
	 * @var    string
	 */
	const SETTING_SLUG = 'fixposts';

	/**
	 * The WordPress internal plugin-name (of this plugin).
	 *
	 * @since 1.0
	 * @var   string
	 */
	static $plugin_name = '';

	/**
	 * Sets up the action-hooks for this plugin
	 *
	 * @since  1.0
	 */
	static public function init() {
		if ( '' === session_id() ) {
			session_start();
		}

		self::$plugin_name = plugin_basename( __FILE__ );

		// Adds the entry in the admin menu.
		add_action( 'admin_menu', 'FixMyPosts::add_menu' );

		// Add a quick-link to the options-page in the plugin screen.
		add_filter( 'plugin_action_links_' . self::$plugin_name, 'FixMyPosts::settings_link' );

		// Ajax callback that will start the processing.
		add_filter( 'wp_ajax_fixme_start', 'FixMyPosts::start_process' );

		// Ajax callback that will return the current process-status.
		add_filter( 'wp_ajax_fixme_status', 'FixMyPosts::tell_status' );
	}

	/**
	 * This will hook up a new submenu item in the tools-menu.
	 *
	 * @since  1.0
	 */
	static public function add_menu() {
		add_management_page(
			'Fix my posts!',
			'Fix my posts',
			'delete_users', // Only show this menu item for the super-admin.
			self::SETTING_SLUG,
			'FixMyPosts::render_options'
		);
	}

	/**
	 * Displays a direct link to the plugins option-page in the plugin-overview.
	 *
	 * @since  1.0
	 * @return array A list with all links that will be displayed in the plugin-list.
	 */
	static public function settings_link( $links ) {
		$settings_link = '<a href="tools.php?page=' . self::SETTING_SLUG . '">Fix posts</a>';
		array_unshift( $links, $settings_link );
		return $links;
	}

	/**
	 * Renders the plugin options page.
	 * On that page the user has the choice to start and monitor the process
	 * that checks and repairs the WordPress posts.
	 *
	 * @since  1.0
	 */
	static public function render_options() {
		$count = self::get_post_count();

		$reset = isset( $_POST['reset'] ) && $_POST['reset'] == 'clear';
		if ( $reset ) {
			// This will stop the current processing-loop.
			delete_option( 'fixme_status' );
		}

		?>
		<style>
		.ajax-details { width: 600px; margin: 20px auto; }
		.ajax-response .logdata { width: 860px; margin: 0 auto; font-size: .9em; }
		.ajax-response .logdata > li { border-bottom: 1px solid #CCC; margin: 0; }
		.ajax-response .logdata pre { margin: 0; padding: 4px 0; }
		.ajax-response .logdata > li.start { text-align: center; background: #CCC; border-top: 1px solid #F5F5F5; }
		.ajax-response .logdata > li.done { text-align: center; background: #CCC; }
		.tc { text-align: center; }
		.progress { width: 400px; height: 40px; line-height: 40px; position: relative; margin: 20px auto 10px; border-radius: 22px; border: 1px solid #1B607F; background: #FFF; overflow: hidden; box-shadow: 0 2px 6px #EEE inset, 0 1px 1px #AAA inset; }
		.progress .val { position: absolute; left: 0; top: 0; bottom: 0; background: #0074A2; transition: width .3s; border-radius: 20px 2px 2px 20px; box-shadow: 0 1px 3px rgba(255, 255, 255, 0.3) inset, 0 -1px 3px rgba(0, 0, 0, 0.3) inset; }
		.progress .label { position: absolute; left: 50%; top: 50%; height: 20px; line-height: 20px; width: 50px; margin-top: -10px; margin-left: -25px; background: rgba(255,255,255,.5); border-radius: 10px; text-align: center; color: #000; }
		.cancel-form { margin-top: 20px; float: right; }
		</style>
		<div class="wrap">
			<h2>Fix my posts!</h2>

			<br class="clear" />

			<form method="POST" class="fixme-form">
				<h3>How it works</h3>
				<p>
					Once you click on the start button the script will start to process all your posts, one by one.<br />
					The Plugin will create a valid post_name for all posts. Additionally the plugin will create/update the meta-data and thumbnails of all images in the database.<br />
					The process may take a while until it is completed. You will see a progress-bar with the current status-info in the bottom.
				</p>
				<p>
					<u>A word of warning</u>: We tested the plugin as well as possible, but since it will make adjustments in the database we still recommend to make a database backup before using this plugin.
				</p>

				<h3>Ready?</h3>
				<p>
					<strong><?php echo number_format( $count, 0, '.', ',' ) ?></strong> posts will be processed.
				</p>
				<p class="submit">
					<input type="submit" name="submit" id="submit" class="button button-primary" value="Okay, start!" />
				</p>
			</form>

			<div class="ajax-details" style="display:none">
				<hr />
				<h4 class="tc">Please wait, your posts are processed...</h4>
				<div class="progress"><div class="val"></div><div class="label"></div></div>
				<div class="tc"><em class="details"></em></div>

				<form method="POST" class="cancel-form">
					<input type="hidden" name="reset" value="clear" />
					<input type="submit" name="submit" id="submit" class="button" value="No, stop!" />
				</form>
			</div>

			<div class="ajax-response">
				<ul class="logdata"></ul>
			</div>
			<br class="clear">
		</div>
		<script>
		jQuery(function init_ajax() {
			var form = jQuery('.fixme-form'),
				ajax_details = jQuery('.ajax-details'),
				ajax_response = jQuery('.ajax-response'),
				result_box = jQuery('ul.results'),
				ajax_url = '<?php echo admin_url( 'admin-ajax.php' ); ?>',
				btn_start = jQuery('#submit', form),
				progress_val = jQuery('.progress .val', ajax_details),
				progress_lbl = jQuery('.progress .label', ajax_details),
				progress_info = jQuery('.details', ajax_details),
				cancel_form = jQuery('.cancel-form', ajax_details),
				ul_log = jQuery('.logdata', ajax_response),
				is_processing = false,
				last_count = 0,
				last_status = '',
				timer = null
				;

			/*
			 * This will frequently check the ajax status and display the
			 * current status on the page.
			 */
			var check_ajax_status = function() {
				/**
				 * Parses the data-object of the Ajax-response.
				 * @see handle_status() below for details.
				 */
				var show_progress = function(data, status) {
					var done = data.total - data.pending,
						percent = (100 / data.total) * done;

					// Display the log items to the user.
					for ( ind = 0; ind < data.items.length; ind += 1 ) {
						var item = data.items[ind];
						if ( typeof item === 'object' ) {
							ul_log.append('<li><pre><strong>' + item.id + '</strong>: ' + item.log + '</pre></li>');
						}
					}

					/*
					 * Condition handles asynchronous-response issues when
					 * ajax-poll is not answered in expected order.
					 * e.g.: poll1 -> poll2 -> response2 -> response1
					 */
					if ( done > last_count ) {
						last_count = done;
						progress_val.css({'width': percent + '%'});
						progress_lbl.text( parseInt( percent ) + ' %' );
						progress_info.text( done + ' / ' + data.total );
					}

					if ( is_processing && status == 'done' ) {
						process_done();
					}
				};

				/**
				 * Ajax response with status information.
				 * The status information is an object with following properties:
				 *   - status: 'done' / 'working'
				 *   - data:   <data-object>
				 *
				 *   data-object:
				 *   - total:   <number>
				 *   - pending: <number>
				 *   - items:   [ <item-object> ]  (array)
				 *
				 *   item-object:
				 *   - id:  <number>
				 *   - log: <text>
				 */
				var handle_status = function(response) {
					var data = false, res = false;
					try {
						res = jQuery.parseJSON( response );

						show_progress( res.data, res.status );

						if ( ! is_processing && res.status == 'working' ) {
							process_start();
						}
					} catch( ex ) {
						// Ignore errors.
					}
				};

				var args = {
					'action': 'fixme_status'
				};
				jQuery.post( ajax_url, args, handle_status );
			};

			/*
			 * Start the processing via an ajax call, then set up the interval
			 * to check the processing status.
			 */
			var process_start = function() {
				if ( ! is_processing ) {
					btn_start.prop('disabled', true);
					is_processing = true;
					last_count = 0;

					progress_val.css({'width': '0%'});
					progress_lbl.text( '0 %' );
					progress_info.text( '' );
					cancel_form.show();
					ul_log.empty();

					ajax_details.show();

					var args = {
						'action': 'fixme_start'
					};
					try {
						jQuery.post( ajax_url, args );
					} catch(ex) {
						/*
						 * Ajax-Request failed. Most likely there was a timeoutthe
						 * while processing posts. We choose to ignore it.
						 */
						//window.location.reload();
					}

					timer = window.setInterval( function() { check_ajax_status() }, 1000 );
					ul_log.append('<li class="start">Starting</li>');
				}
			};

			// clean up at the end.
			var process_done = function() {
				if ( is_processing ) {
					window.clearInterval( timer );
					timer = null;

					is_processing = false;
					btn_start.prop('disabled', false);
					last_count = 0;

					progress_val.css({'width': '100%'});
					progress_lbl.text( 'Done!' );
					cancel_form.hide();

					ul_log.append('<li class="done">Finished</li>');
				}
			};

			// When user submits the form make ajax request to start the processing.
			form.submit(function(ev) {
				ev.preventDefault();
				process_start();
				return false;
			});

			check_ajax_status();
		});
		</script>
		<?php
	}

	/**
	 * Returns the next post_id of the post-queue and removes $last_post from the queue.
	 * When the queue does not exist it will be created and initialized
	 *
	 * @since  1.0
	 * @param  int $last_post The last post that was processed (it will be
	 *                       removed from the queue).
	 * @return mixed Either <post_id> of the next post to process or FALSE when
	 *               no more post is found.
	 */
	static public function get_next_post( $last_post ) {
		$queue = get_option( 'fixme_queue', false );

		if ( empty( $queue ) ) {
			// Initialize the queue.
			global $wpdb;

			$sql = "
				SELECT ID
				FROM {$wpdb->posts}
				WHERE post_type NOT IN ('revision')
			";
			$queue = $wpdb->get_col( $sql );
		}

		// remove processed post from the queue.
		if ( ( $key = array_search( $last_post, $queue ) ) !== false ) {
			unset( $queue[$key] );
		}

		if ( ! empty( $queue ) ) {
			// udpate the queue in database.
			update_option( 'fixme_queue', $queue );

			// get the next post to process.
			return reset( $queue );
		} else {
			// delete the queue.
			delete_option( 'fixme_queue' );

			// tell process-loop to stop.
			return false;
		}
	}

	/**
	 * Returns the total number of posts in the database.
	 *
	 * @since  1.0
	 */
	static public function get_post_count() {
		global $wpdb;
		$sql = "
			SELECT COUNT(1)
			FROM {$wpdb->posts}
			WHERE post_type NOT IN ('revision')
		";
		return $wpdb->get_var( $sql );
	}

	/**
	 * Returns the number of unprocessed-posts.
	 *
	 * @since  1.0
	 */
	static public function get_post_count_pending() {
		$queue = get_option( 'fixme_queue', false );
		if ( $queue === false ) {
			return 0;
		}
		return count( $queue );
	}

	/**
	 * Starts the post processing.
	 * This is an ajax handler which is not supposed to make any reponse.
	 * Arguments are passed in via $_POST.
	 *
	 * @since  1.0
	 */
	static public function start_process() {
		if ( 'stopped' != get_option( 'fixme_status', 'stopped' ) ) {
			return;
		}

		//$_SESSION['fixme_data'] = 'fixme_data-' . mt_rand( 100, 999 ) . time();
		$fixme_key = 'fixme_data';

		// Set the PHP Timeout limit to 1 hour (3600 sec).
		set_time_limit(3600);
		update_option( $fixme_key, array() );
		$item_id = 0;

		/*
		 * This is the repair-loop!
		 * The current status of the loop is queried via ajax, @see tell_status().
		 */
		while ( $item_id = self::get_next_post( $item_id ) ) {
			if ( empty( $item_id ) ) {
				return;
			}

			update_option( 'fixme_status', 'working' );
			$post = get_post( $item_id );

			$log = self::fix_post( $post );

			/*
			 * Append the data to the DB, so the tell_status method can return
			 * the information.
			 */
			$data = get_option( $fixme_key, array() );
			if ( ! is_array( $data ) ) {
				$data = array();
			}
			$data[] = array(
				'id' => $item_id,
				'log' => $log,
			);
			update_option( $fixme_key, $data );
		}

		/*
		 * When done delete the status; on next ajax-poll the javascript will be
		 * informed that we are finished.
		 */
		delete_option( 'fixme_status' );

		die();
	}

	/**
	 * Returns the current post processing status to the user.
	 * This is an ajax handler which is supposed to return status information
	 * and then die().
	 *
	 * @since  1.0
	 */
	static public function tell_status() {
		$fixme_key = 'fixme_data';

		$data = array();
		$data['items'] = get_option( $fixme_key, array() );
		$data['total'] = self::get_post_count();
		$data['pending'] = max( 0, self::get_post_count_pending() - 1);

		// When all items are processed remove the status-flag.
		if ( $data['pending'] == 0 ) {
			delete_option( 'fixme_status' );
		}

		$status = get_option( 'fixme_status', 'done' );

		// Clear the data from DB, so it will not be returned on next ajax-poll.
		delete_option( $fixme_key );

		/*
		 * Report data back to the browser.
		 * This is used to update the status and the repair-log
		 */
		echo json_encode( array(
			'status' => $status,
			'data' => $data,
		) );
		die();
	}

	/**
	 * The core of this plugin: This method gets a WP_Post object and is
	 * responsible for fixing it.
	 * No return value required: Simply fix it, here and now!
	 *
	 * 1.2: Implemented changes suggested by Fred:
	 *      {@link http://www.adam-makes-websites.com/downloads/fix_wp_image_meta_data.txt}
	 * 1.2: Add the action-hook "fix-the-post" to allow custom repair.
	 *
	 * @since  1.0
	 * @param  WP_Post $post A post object that should be checked/fixed.
	 * @return string The log-details which will be displayed to the user.
	 */
	static public function fix_post( $post ) {
		$log_details = array();
		$log_details[] = sprintf( __( 'Repair post [%1$s]', 'fixmyposts' ), $post->ID );

		// => ACTION: Fix the post-name.
		$post->post_name = '';
		wp_update_post( $post );

		// Check images: This will create the image-meta entry and also create thumbnails.
		if ( $post->post_type == 'attachment' &&
				substr( $post->post_mime_type, 0, 6 ) == 'image/' ) {

			// The file-path is an absolute path.
			$file_path = get_attached_file( $post->ID );

			$log_details[] = sprintf( __( 'Repairing image metadata: "%1$s"', 'fixmyposts' ), $file_path );

			// New condition since 1.2 - check for alternate file-name.
			if ( strlen( $file_path ) == 0 || ! file_exists( $file_path ) ) {
				// Try to get the filename from the post-table "GUID" column.
				if ( ! empty( $post->guid ) &&
						($uploads = wp_upload_dir()) &&
						(false === $uploads['error']) )  {
					$file_path = '';
					$file_name = '';

					// First try to remove the base-URL from the GUID.
					if ( strpos( $post->guid, $uploads['baseurl'] ) === 0 ) {
						$file_name = substr( $post->guid, strlen( $uploads['baseurl'] )+1 );
					}

					// Then prepend the absolute path to upload-dir to the filename.
					if ( strlen( $file_name ) > 0 ) {
						$file_path = $uploads['basedir'] . "/$file_name";

						$log_details[] = sprintf( __( 'Using alternate image path: "%1$s"', 'fixmyposts' ), $file_path );
					}
				}
			}

			// New condition since 1.2 - missing files will not cause errors.
			if ( strlen( $file_path ) > 0 && file_exists( $file_path ) ) {
				/*
				 * => ACTION: Create new metadata for the image.
				 *    For these functions we need "wp-admin/includes/image.php"
				 */
				wp_update_attachment_metadata(
					$post->ID,
					wp_generate_attachment_metadata( $post->ID, $file_path )
				);
			} else {
				$log_details[] = sprintf( __( 'Image ignored (file not found): "%1$s"', 'fixmyposts' ), $file_path );
			}
		}

		/**
		 * Allow the user to easily tap into the repair-loop.
		 */
		ob_start();
		do_action( 'fix-the-post', $post );
		$log_details[] = ob_get_clean();

		return trim( implode( "\n", $log_details ) );
	}


};


// Initialize the plugin (attach hooks, and so on)!
FixMyPosts::init();


