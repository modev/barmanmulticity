<?php
/**
 * WooCommerce Twilio SMS Notifications
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Twilio SMS Notifications to newer
 * versions in the future. If you wish to customize WooCommerce Twilio SMS Notifications for your
 * needs please refer to http://docs.woothemes.com/document/twilio-sms-notifications/ for more information.
 *
 * @package     WC-Twilio-SMS-Notifications/Admin
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2014, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Twilio SMS Admin class
 *
 * Loads admin settings page and adds related hooks / filters
 *
 * @since 1.0
 */
class WC_Twilio_SMS_Admin {


	/** @var string id of tab on WooCommerce Settings page */
	private $tab_id = 'twilio_sms';


	/**
	 * Setup admin class
	 *
	 * @since  1.0
	 */
	public function __construct() {

		/** General Admin Hooks */

		// Add SMS tab
		add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab'  ), 100 );

		// Show SMS settings page
		add_action( 'woocommerce_settings_tabs_twilio_sms', array( $this, 'display_settings' ) );

		// Save SMS settings page
		add_action( 'woocommerce_update_options_' . $this->tab_id, array( $this, 'process_settings' ) );

		// Add custom 'wc_twilio_sms_link' form field type
		add_action( 'woocommerce_admin_field_wc_twilio_sms_link', array( $this, 'add_link_field' ) );

		// add 'Twilio SMS Notifications' item to admin bar menu
		add_action( 'admin_bar_menu', array( $this, 'add_admin_bar_menu_item' ), 100 );

		/** Order Admin Hooks */

		// Add 'Send an SMS' meta-box on Order page to send SMS to customer
		add_action( 'add_meta_boxes', array( $this, 'add_order_meta_box' ) );
	}


	/**
	 * Add SMS tab to WooCommerce Settings after 'Email' tab
	 *
	 * @since 1.0
	 * @param array $settings_tabs tabs array sans 'SMS' tab
	 * @return array $settings_tabs now with 100% more 'SMS' tab!
	 */
	public function add_settings_tab( $settings_tabs ) {

		$new_settings_tabs = array();

		foreach ( $settings_tabs as $tab_id => $tab_title ) {

			$new_settings_tabs[ $tab_id ] = $tab_title;

			// Add our tab after 'Email' tab
			if ( 'email' == $tab_id ) {
				$new_settings_tabs[ $this->tab_id ] = __( 'SMS', WC_Twilio_SMS::TEXT_DOMAIN );
			}
		}

		return $new_settings_tabs;
	}


	/**
	 * Show SMS settings page
	 *
	 * @see woocommerce_admin_fields()
	 * @uses WC_Twilio_SMS_Admin::get_settings() to get settings array
	 * @uses WC_Twilio_SMS_Admin::display_send_test_sms_form() to output 'send test SMS' form
	 * @since 1.0
	 */
	public function display_settings() {

		// output settings
		woocommerce_admin_fields( $this->get_settings() );

		// add javascript
		ob_start();
		?>
			// Hide admin notification settings if unchecked
			$('#<?php echo 'wc_twilio_sms_enable_admin_sms'; ?>').on('change', function() {

				if( $(this).is(':checked') ) {
					$(this).closest('tr').nextUntil('p').show();
				} else {
					$(this).closest('tr').nextUntil('p').hide();
				}
			});

			// handle SMS test send
			$('a.<?php echo 'wc_twilio_sms_test_sms_button'; ?>').on('click', function () {

				var number  = $('input#<?php echo 'wc_twilio_sms_test_mobile_number'; ?>');
				var message = $('textarea#<?php echo 'wc_twilio_sms_test_message'; ?>');

				// make sure values are not empty
				if (( !number.val() ) || ( !number.val() )) {
					alert("<?php _e( 'Please make sure you have entered a mobile phone number and test message.', WC_Twilio_SMS::TEXT_DOMAIN ); ?>");
					return false;
				}

				// block UI
				number.closest('table').block({ message:null, overlayCSS:{ background:'#fff url(<?php echo SV_WC_Plugin_Compatibility::WC()->plugin_url(); ?>/assets/images/ajax-loader.gif) no-repeat center', opacity:0.6 } });

				// build data
				var data = {
					mobile_number	: number.val(),
					message			: message.val(),
					security		: '<?php echo wp_create_nonce( 'wc_twilio_sms_send_test_sms' ); ?>'
				};

				jQuery.post('<?php echo admin_url( 'admin-ajax.php?action=woocommerce_twilio_sms_send_test_sms' ); ?>', data, function (response) {

					// unblock UI
					number.closest('table').unblock();

					// clear posted values
					number.val('');
					message.val('');

					// Display Success or Failure message from response
					alert(response);

				});

				return false;

			});
		<?php
		SV_WC_Plugin_Compatibility::wc_enqueue_js( ob_get_clean() );
	}


	/**
	 * Add 'Send an SMS' meta-box to Orders page
	 *
	 * @since 1.0
	 */
	public function add_order_meta_box() {

		add_meta_box(
			'wc_twilio_sms_order_meta_box',
			__( 'Send SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
		 	array( $this, 'display_order_meta_box' ),
			'shop_order',
			'side',
			'default'
		);
	}


	/**
	 * Display the 'Send an SMS' meta-box on the Orders page
	 *
	 * @since 1.0
	 */
	public function display_order_meta_box() {
		global $theorder;

		$default_message = apply_filters( 'wc_twilio_sms_notifications_default_admin_sms_message', '' );
	?>
    	<p><textarea type="text" name="wc_twilio_sms_order_message" id="wc_twilio_sms_order_message" class="input-text" style="width: 100%;" rows="4" value="<?php echo esc_attr( $default_message ); ?>"></textarea></p>
    	<p><a class="button tips" id="wc_twilio_sms_order_send_message" data-tip="<?php _e( 'Send an SMS to the billing phone number for this order.', WC_Twilio_SMS::TEXT_DOMAIN ); ?>"><?php _e( 'Send SMS', WC_Twilio_SMS::TEXT_DOMAIN ); ?></a>
        <span id="wc_twilio_sms_order_message_char_count" style="color: green; float: right; font-size: 16px;">0</span></p>

	<?php ob_start(); ?>
		// character count
		$('#wc_twilio_sms_order_message').on('change keyup input', function() {

			$('#wc_twilio_sms_order_message_char_count').text( $(this).val().length );

			if( $(this).val().length > 160 ) {
				$('#wc_twilio_sms_order_message_char_count').css('color','red');
			}
		});

		// AJAX message send
		$( "a#wc_twilio_sms_order_send_message" ).click( function( e ) {

			var $section = $( "div#wc_twilio_sms_order_meta_box" ),
				$message = $( "textarea#wc_twilio_sms_order_message" );

			if ( $section.is( ".processing" ) ) return false;

			$section.addClass( "processing" ).block( { message: null, overlayCSS: { background: "#fff url( '<?php echo esc_url( SV_WC_Plugin_Compatibility::WC()->plugin_url() . '/assets/images/ajax-loader.gif' ); ?>' ) no-repeat center", backgroundSize: "16px 16px", opacity: 0.6 } } );

			var data = {
				action:    "wc_twilio_sms_send_order_sms",
				security:  "<?php echo wp_create_nonce( 'wc_twilio_sms_send_order_sms' ); ?>",
				order_id:  "<?php echo esc_js( $theorder->id ); ?>",
				message:   $message.val()
			};

			$.ajax( {
					type:     "POST",
					url:      ajaxurl,
					data:     data,
					success:  function( response ) {

					$section.removeClass( "processing" ).unblock();

					if ( response ) {

						$section.block( { message: response, timeout: 900 } );
						$message.val( '' );
					}
				},
				dataType: "html"
			} );
			return false;
		});
	<?php

		SV_WC_Plugin_Compatibility::wc_enqueue_js( ob_get_clean() );
	}


	/**
	 * Update options on SMS settings page
	 *
	 * @see woocommerce_update_options()
	 * @uses WC_Twilio_SMS_Admin::get_settings() to get settings array
	 * @since 1.0
	 */
	public function process_settings() {

		woocommerce_update_options( $this->get_settings() );
	}


	/**
	 * Build array of plugin settings in format needed to use WC admin settings API
	 *
	 * @see woocommerce_admin_fields()
	 * @see woocommerce_update_options()
	 * @since 1.0
	 * @returns array settings
	 */
	public static function get_settings() {

		return array(

			array(
				'name' => __( 'General Settings', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type' => 'title'
			),

			array(
				'id'       => 'wc_twilio_sms_checkout_optin_checkbox_label',
				'name'     => __( 'Opt-in Checkbox Label', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Label for the Opt-in checkbox on the Checkout page. Leave blank to disable the opt-in and force ALL customers to receive SMS updates.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width: 275px;',
				'default'  => __( 'Please send me order updates via text message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'text'
			),

			array(
				'id'       => 'wc_twilio_sms_checkout_optin_checkbox_default',
				'name'     => __( 'Opt-in Checkbox Default', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Default status for the Opt-in checkbox on the Checkout page.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'std'      => 'unchecked',
				'default'  => 'unchecked',
				'type'     => 'select',
				'options'  => array(
					'unchecked' => __( 'Unchecked', WC_Twilio_SMS::TEXT_DOMAIN ),
					'checked'   => __( 'Checked', WC_Twilio_SMS::TEXT_DOMAIN )
				)
			),

			array(
				'id'       => 'wc_twilio_sms_shorten_urls',
				'name'     => __( 'Shorten URLs', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Enable to automatically shorten links in SMS messages with Google URL Shortener (goo.gl)', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'  => 'yes',
				'type'     => 'checkbox'
			),

			array( 'type' => 'sectionend' ),

			array(
				'name' => __( 'Admin Notifications', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type' => 'title'
			),

			array(
				'id'      => 'wc_twilio_sms_enable_admin_sms',
				'name'    => __( 'Enable new order SMS admin notifications.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default' => 'no',
				'type'    => 'checkbox'
			),

			array(
				'id'       => 'wc_twilio_sms_admin_sms_recipients',
				'name'     => __( 'Admin Mobile Number', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Enter the mobile number (starting with the country code) where the New Order SMS should be sent. Send to multiple recipients by separating numbers with commas.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'  => '15558675309',
				'type'     => 'text'
			),

			array(
				'id'       => 'wc_twilio_sms_admin_sms_template',
				'name'     => __( 'Admin SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Use these tags to customize your message: %shop_name%, %order_id%, %order_amount%. Remember that SMS messages are limited to 160 characters.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'default'  => __( '%shop_name% : You have a new order (%order_id%) for %order_amount%!', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'textarea'
			),

			array( 'type' => 'sectionend' ),

			array(
				'name' => __( 'Customer Notifications', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type' => 'title'
			),

			array(
				'name'          => __( 'Send SMS Notifications for these statuses:', WC_Twilio_SMS::TEXT_DOMAIN ),
				'id'            => 'wc_twilio_sms_send_sms_pending',
				'desc'          => __( 'Pending', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => 'start'
			),

			array(
				'id'            => 'wc_twilio_sms_send_sms_on-hold',
				'desc'          => __( 'On-Hold', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => ''
			),

			array(
				'id'            => 'wc_twilio_sms_send_sms_processing',
				'desc'          => __( 'Processing', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => ''
			),

			array(
				'id'            => 'wc_twilio_sms_send_sms_completed',
				'desc'          => __( 'Completed', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => ''
			),

			array(
				'id'            => 'wc_twilio_sms_send_sms_cancelled',
				'desc'          => __( 'Cancelled', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => ''
			),

			array(
				'id'            => 'wc_twilio_sms_send_sms_refunded',
				'desc'          => __( 'Refunded', WC_Twilio_SMS::TEXT_DOMAIN ),
				'std'           => 'yes',
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => ''
			),

			array(
				'id'            => 'wc_twilio_sms_send_sms_failed',
				'desc'          => __( 'Failed', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'       => 'yes',
				'type'          => 'checkbox',
				'checkboxgroup' => 'end'
			),

			array(
				'id'       => 'wc_twilio_sms_default_sms_template',
				'name'     => __( 'Default Customer SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Use these tags to customize your message: %shop_name%, %order_id%, %order_amount%, %order_status%. Remember that SMS messages are limited to 160 characters.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'default'  => __( '%shop_name% : Your order (%order_id%) is now %order_status%.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'textarea'
			),

			array(
				'id'       => 'wc_twilio_sms_pending_sms_template',
				'name'     => __( 'Pending SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Add a custom SMS message for pending orders or leave blank to use the default message above.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'type'     => 'textarea'
			),

			array(
				'id'       => 'wc_twilio_sms_on-hold_sms_template',
				'name'     => __( 'On-Hold SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Add a custom SMS message for on-hold orders or leave blank to use the default message above.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'type'     => 'textarea'
			),

			array(
				'id'       => 'wc_twilio_sms_processing_sms_template',
				'name'     => __( 'Processing SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Add a custom SMS message for processing orders or leave blank to use the default message above.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'type'     => 'textarea'
			),

			array(
				'id'       => 'wc_twilio_sms_completed_sms_template',
				'name'     => __( 'Completed SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Add a custom SMS message for completed orders or leave blank to use the default message above.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'type'     => 'textarea'
			),

			array(
				'id'       => 'wc_twilio_sms_cancelled_sms_template',
				'name'     => __( 'Cancelled SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Add a custom SMS message for cancelled orders or leave blank to use the default message above.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'type'     => 'textarea'
			),

			array(
				'id'       => 'wc_twilio_sms_refunded_sms_template',
				'name'     => __( 'Refunded SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Add a custom SMS message for refunded orders or leave blank to use the default message above.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'type'     => 'textarea'
			),

			array(
				'id'       => 'wc_twilio_sms_failed_sms_template',
				'name'     => __( 'Failed SMS Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Add a custom SMS message for failed orders or leave blank to use the default message above.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'css'      => 'min-width:500px;',
				'type'     => 'textarea'
			),

			array( 'type' => 'sectionend' ),

			array(
				'name' => __( 'Twilio Settings', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type' => 'title'
			),

			array(
				'id'       => 'wc_twilio_sms_account_sid',
				'name'     => __( 'Account SID', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Log into your Twilio Account to find your Account SID.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'text'
			),

			array(
				'id'       => 'wc_twilio_sms_auth_token',
				'name'     => __( 'Auth Token', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Log into your Twilio Account to find your Auth Token.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'text'
			),

			array(
				'id'       => 'wc_twilio_sms_from_number',
				'name'     => __( 'From Number', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Enter the number to send SMS messages from. This must be a purchased number from Twilio.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'text'
			),

			array(
				'id'       => 'wc_twilio_sms_log_errors',
				'name'     => __( 'Log Errors', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Enable this to log Twilio API errors to the WooCommerce log. Use this if you are having issues sending SMS.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'default'  => 'no',
				'type'     => 'checkbox'
			),

			array( 'type' => 'sectionend' ),

			array(
				'name' => __( 'Send Test SMS', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type' => 'title'
			),

			array(
				'id'       => 'wc_twilio_sms_test_mobile_number',
				'name'     => __( 'Mobile Number', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Enter the mobile number (starting with the country code) where the test SMS should be send. Note that if you are using a trial Twilio account, this number must be verified first.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'text'
			),

			array(
				'id'       => 'wc_twilio_sms_test_message',
				'name'     => __( 'Message', WC_Twilio_SMS::TEXT_DOMAIN ),
				'desc_tip' => __( 'Enter the test message to be sent. Remember that SMS messages are limited to 160 characters.', WC_Twilio_SMS::TEXT_DOMAIN ),
				'type'     => 'textarea',
				'css'      => 'min-width: 500px;'
			),

			array(
				'name'  => __( 'Send', WC_Twilio_SMS::TEXT_DOMAIN ),
				'href'  => '#',
				'class' => 'wc_twilio_sms_test_sms_button' . ' button',
				'type'  => 'wc_twilio_sms_link'
			),

			array( 'type' => 'sectionend', 'id' => 'wc_twilio_sms_send_test_section' )

		);
	}


	/**
	 * Add custom woocommerce admin form field via woocommerce_admin_field_* action
	 *
	 * @since 1.0
	 * @param array $field associative array of field parameters
	 */
	public function add_link_field( $field ) {

		if ( isset( $field['name'] ) && isset( $field['class'] ) && isset( $field['href'] ) ) :

		?>
			<tr valign="top">
				<th scope="row" class="titledesc"></th>
				<td class="forminp">
					<a href="<?php echo esc_url( $field['href'] ); ?>" class="<?php echo esc_attr( $field['class'] ); ?>"><?php echo wp_filter_kses( $field['name'] ); ?></a>
				</td>
			</tr>
		<?php

		endif;
	}


	/**
	 * Add the 'Twilio SMS Notifications' admin menu bar item
	 *
	 * @since 1.1
	 */
	public function add_admin_bar_menu_item() {
		global $wp_admin_bar;

		// security check
		if ( ! is_admin_bar_showing() || ! current_user_can( 'manage_woocommerce' ) ) {
			return;
		}

		// add top-level menu
		$menu_args = array(
			'id'    => 'wc_twilio_sms_admin_bar_menu',
			'title' => __( 'Twilio SMS Notifications', WC_Twilio_SMS::TEXT_DOMAIN ),
			'href'  => false
		);

		// get SMS usage
		$sms_usage = $this->get_sms_usage();

		// set message
		if ( 0 == $sms_usage['count'] ) {
			$message = __( 'Your store has not sent any SMS messages today.', WC_Twilio_SMS::TEXT_DOMAIN );
		} else {
			$message = sprintf( _n( 'Your store has sent %d SMS message today at a cost of $%s', 'Your store has sent %d SMS messages today at a cost of $%s', $sms_usage['count'], WC_Twilio_SMS::TEXT_DOMAIN ),
								$sms_usage['count'], $sms_usage['cost'] );
		}

		// setup 'usage' item
		$sms_usage_item_args = array(
			'id' => 'wc_twilio_sms_sms_usage_item',
			'title' => $message,
			'href' => false,
			'parent' => 'wc_twilio_sms_admin_bar_menu'
		);

		// setup 'add funds' link
		$add_funds_item_args = array(
			'id'     => 'wc_twilio_sms_add_funds_item',
			'title'  => __( 'Add Funds to Your Twilio Account', WC_Twilio_SMS::TEXT_DOMAIN ),
			'href'   => 'https://www.twilio.com/user/billing',
			'meta'   => array( 'target' => '_blank' ),
			'parent' => 'wc_twilio_sms_admin_bar_menu'
		);

		// add menu + items
		$wp_admin_bar->add_menu( $menu_args );
		$wp_admin_bar->add_menu( $sms_usage_item_args );
		$wp_admin_bar->add_menu( $add_funds_item_args );
	}


	/**
	 * Get SMS usage for today via Twilio API and set as 15 minute transient
	 *
	 * @since 1.1
	 */
	private function get_sms_usage() {
		global $wc_twilio_sms;

		// get transient
		if ( false === ( $usage = get_transient( 'wc_twilio_sms_sms_usage' ) ) ) {

			// transient doesn't exist, fetch via Twilio API
			try {

				// get SMS usage
				$response = $wc_twilio_sms->get_api()->get_sms_usage();

				$usage = array(
					'count' => ( isset( $response['usage_records'][0]['count'] ) ) ? $response['usage_records'][0]['count'] : 0,
					'cost'  => ( isset( $response['usage_records'][0]['price'] ) ) ? $response['usage_records'][0]['price'] : 0
				);

				// set 15 minute transient
				set_transient( 'wc_twilio_sms_sms_usage', $usage, 60*15 );

				return $usage;

			} catch ( Exception $e ) {

				$wc_twilio_sms->log( $e->getMessage() );

				return array( 'count' => 0, 'cost' => '0.00' );
			}

		} else {

			return $usage;
		}
	}


} // end \WC_Twilio_SMS_Admin class
