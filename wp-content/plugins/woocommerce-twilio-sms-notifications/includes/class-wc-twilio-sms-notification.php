<?php
/**
 * WooCommerce Twilio SMS Notifications
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Twilio SMS Notifications to newer
 * versions in the future. If you wish to customize WooCommerce Twilio SMS Notifications for your
 * needs please refer to http://docs.woothemes.com/document/twilio-sms-notifications/ for more information.
 *
 * @package     WC-Twilio-SMS-Notifications/Notification
 * @author      SkyVerge
 * @copyright   Copyright (c) 2013-2014, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Twilio SMS Notification class
 *
 * Handle SMS sending Admin & Customer Notifications, as well as manual SMS messages from Order page
 *
 * @since 1.0
 */
class WC_Twilio_SMS_Notification  {

	/** @var \WC_Order order object for SMS sending  */
	private $order;


	/**
	 * Load new order object
	 *
	 * @since 1.0
	 * @param int $order_id the order ID
	 */
	public function __construct( $order_id ) {

		$this->order = new WC_Order( $order_id );

	}


	/**
	 * Send admin new order SMS notifications
	 *
	 * @since 1.0
	 */
	public function send_admin_notification() {
		global $wc_twilio_sms;

		// Check if sending admin SMS updates for new orders
		if ( 'yes' == get_option( 'wc_twilio_sms_enable_admin_sms' ) ) {

			// get message template
			$message = get_option( 'wc_twilio_sms_admin_sms_template', '' );

			// replace template variables
			$message = $this->replace_message_variables( $message );

			// shorten URLs if enabled
			if ( 'yes' == get_option( 'wc_twilio_sms_shorten_urls' ) ) {
				$message = $this->shorten_urls( $message );
			}

			// get admin phone number (s)
			$recipients = explode( ',', trim( get_option( 'wc_twilio_sms_admin_sms_recipients' ) ) );

			// send the SMS to each recipient
			if ( ! empty( $recipients ) ) {

				foreach ( $recipients as $recipient ) {

					try {

						$wc_twilio_sms->get_api()->send( $recipient, $message, false );

					} catch ( Exception $e ) {

						$wc_twilio_sms->log( $e->getMessage() );
					}
				}
			}

		}
	}


	/**
	 * Sends customer SMS notifications on order status changes
	 *
	 * @since  1.0
	 */
	public function send_automated_customer_notification() {

		// get checkbox opt-in label
		$optin = get_option( 'wc_twilio_sms_checkout_optin_checkbox_label', '' );

		// check if opt-in checkbox is enabled
		if ( ! empty( $optin ) ) {

			// get opt-in meta for order
			$optin = get_post_meta( $this->order->id, '_wc_twilio_sms_optin', true );

			// check if customer has opted-in
			if ( empty( $optin ) ) {
				// no meta set, so customer has not opted in
				return;
			}
		}

		// Check if sending SMS updates for this order's status
		if ( 'yes' == get_option( 'wc_twilio_sms_send_sms_' . $this->order->status ) ) {

			// get message template
			$message = get_option( 'wc_twilio_sms_' . $this->order->status . '_sms_template', '' );

			// use the default template if status-specific one is blank
			if ( empty( $message ) ) {
				$message = get_option( 'wc_twilio_sms_default_sms_template' );
			}

			// allow modification of message before variable replace (add additional variables, etc)
			$message = apply_filters( 'wc_twilio_sms_customer_sms_before_variable_replace', $message, $this->order );

			// replace template variables
			$message = $this->replace_message_variables( $message );

			// allow modification of message after variable replace
			$message = apply_filters( 'wc_twilio_sms_customer_sms_after_variable_replace', $message, $this->order );

			// allow modification of the "to" phone number
			$phone = apply_filters( 'wc_twilio_sms_customer_phone', $this->order->billing_phone, $this->order );

			// shorten URLs if enabled
			if ( 'yes' == get_option( 'wc_twilio_sms_shorten_urls' ) ) {
				$message = $this->shorten_urls( $message );
			}

			// send the SMS!
			$this->send_sms( $phone, $message );
		}

	}


	/**
	 * Sends SMS to customer from 'Send an SMS' metabox on Orders page
	 *
	 * @since 1.0
	 * @param string $message message to send customer
	 */
	public function send_manual_customer_notification( $message ) {

		// shorten URLs if enabled
		if ( 'yes' == get_option( 'wc_twilio_sms_shorten_urls' ) ) {
			$message = $this->shorten_urls( $message );
		}

		// send the SMS!
		$this->send_sms( $this->order->billing_phone, $message );
	}


	/**
	 * Create and send SMS message
	 *
	 * @since 1.0
	 * @param string $to
	 * @param string $message
	 * @param bool $customer_notification order note is added if true
	 */
	private function send_sms( $to, $message, $customer_notification = true ) {
		global $wc_twilio_sms;

		// Default status for SMS message, on error this is replaced with error message
		$status = __( 'Sent', WC_Twilio_SMS::TEXT_DOMAIN );

		// Timestamp of SMS is current time
		$sent_timestamp =  time();

		// error flag
		$error = false;

		try {

			// send the SMS via API
			$response = $wc_twilio_sms->get_api()->send( $to, $message, $this->order->billing_country );

			// use the timestamp from twilio if available
			$sent_timestamp = ( isset( $response['date_created'] ) ) ? strtotime( $response['date_created'] ) : $sent_timestamp;

			// use twilio formatted number if available
			$to = ( isset( $response['to'] ) ) ? $response['to'] : $to;

		} catch ( Exception $e ) {

			// Set status to error message
			$status = $e->getMessage();

			// set error flag
			$error = true;

			// log to PHP error log
			$wc_twilio_sms->log( $e->getMessage() );
		}

		// Add formatted order note
		if ( $customer_notification ) {
			$this->order->add_order_note( $this->format_order_note( $to, $sent_timestamp, $message, $status, $error ) );
		}
	}

	/**
	 * Extract URLs from SMS message and replace them with shorten URLs via callback
	 *
	 * @since  1.0
	 * @param string $message SMS message
	 * @return string SMS message with URLs shortened
	 */
	private function shorten_urls( $message ) {

		// regex pattern source : http://daringfireball.net/2010/07/improved_regex_for_matching_urls
		$pattern = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";

		// find each URL and replacing using callback
		$message = preg_replace_callback( $pattern, array( $this, 'shorten_url' ), $message );

		// return message with shortened URLs
		return $message;
	}

	/**
	 * Callback for shorten_urls() preg_replace
	 * By default, uses Google URL Shortener
	 *
	 * @since 1.0
	 * @param array $matches matches found via preg_replace
	 * @return string shortened url
	 */
	private function shorten_url( $matches ) {

		// get first match
		$url = reset( $matches );

		// shorten via google
		$shortened_url = $this->google_shorten_url( $url );

		// allow use of other URL shorteners
		return apply_filters( 'wc_twilio_sms_shorten_url', $shortened_url, $url );
	}


	/**
	 * Shortens a given URL via Google URL Shortener
	 *
	 * @link : https://developers.google.com/url-shortener/v1/getting_started
	 * @since  1.0
	 * @param string $url URL to shorten
	 * @return string shortened URL
	 */
	private function google_shorten_url( $url ) {
		global $wc_twilio_sms;

		// set wp_remote_post arguments
		$args = array(
			'method'      => 'POST',
			'timeout'     => '10',
			'redirection' => 0,
			'httpversion' => '1.0',
			'sslverify'   => 'false',
			'blocking'    => 'true',
			'headers'     => array(
				'Content-Type' => 'application/json'
			),
			'body'        => json_encode( array( 'longUrl' => $url ) ),
			'cookies'     => array()
		);

		// perform POST request
		$short_url = wp_remote_post( 'https://www.googleapis.com/urlshortener/v1/url', $args );

		// check for WP Error
		if ( is_wp_error( $short_url ) ) {

			$short_url = $url;

			$wc_twilio_sms->log( $short_url->get_error_message() );
		}

		// JSON decode response body into associative array
		if ( isset( $short_url['response']['code'] ) && 200 == $short_url['response']['code'] && isset( $short_url['body'] ) ) {
			$short_url = json_decode( $short_url['body'], true );
		}

		// if short url was decoded successfully, use it
		if ( isset( $short_url['id'] ) && ! empty( $short_url['id'] ) ) {
			$url = $short_url['id'];
		}

		return $url;
	}


	/**
	 * Replaces template variables in SMS message
	 *
	 * @since 1.0
	 * @param string $message raw SMS message to replace with variable info
	 * @return string message with variables replaced with indicated values
	 */
	private function replace_message_variables( $message ) {

		$replacements = array(
			'%shop_name%'    => get_bloginfo( 'name' ),
			'%order_id%'     => $this->order->get_order_number(),
			'%order_amount%' => $this->order->get_total(),
			'%order_status%' => ucfirst( $this->order->status )
		);

		return str_replace( array_keys( $replacements ), $replacements, $message );
	}


	/**
	 * Formats order note
	 *
	 * @since  1.0
	 * @param string $to number SMS message was sent to
	 * @param int $sent_timestamp integer timestamp for when message was sent
	 * @param string $message SMS message sent
	 * @param string $status order status
	 * @param bool $error true if there was an error sending SMS, false otherwise
	 * @return string HTML-formatted order note
	 */
	private function format_order_note( $to, $sent_timestamp, $message, $status, $error ) {
		global $wc_twilio_sms;

		try {

			// get datetime object from unix timestamp
			$datetime = new DateTime( "@{$sent_timestamp}", new DateTimeZone( 'UTC' ) );

			// change timezone to site timezone
			$datetime->setTimezone( new DateTimeZone( $this->get_wp_timezone_string() ) );

			$datetime_format = SV_WC_Plugin_Compatibility::is_wc_version_gte_2_1() ? wc_date_format() . ' ' . wc_time_format() : woocommerce_date_format() . ' ' . woocommerce_time_format();

			// return datetime localized to site date/time settings
			$formatted_datetime = date_i18n( $datetime_format, $sent_timestamp + $datetime->getOffset() );

		} catch ( Exception $e ) {

			// log error and set datetime for SMS to 'N/A'
			$wc_twilio_sms->log( $e->getMessage() );
			$formatted_datetime = __( 'N/A', WC_Twilio_SMS::TEXT_DOMAIN );
		}

		ob_start();
		?>
	  	<p><strong><?php _e( 'SMS Notification', WC_Twilio_SMS::TEXT_DOMAIN ); ?></strong></p>
		<p><strong><?php _e( 'To', WC_Twilio_SMS::TEXT_DOMAIN ); ?>: </strong><?php echo esc_html( $to ); ?></p>
		<p><strong><?php _e( 'Date Sent', WC_Twilio_SMS::TEXT_DOMAIN ); ?>: </strong><?php echo esc_html( $formatted_datetime ); ?></p>
		<p><strong><?php _e( 'Message', WC_Twilio_SMS::TEXT_DOMAIN ); ?>: </strong><?php echo esc_html( $message ); ?></p>
		<p><strong><?php _e( 'Status', WC_Twilio_SMS::TEXT_DOMAIN ); ?>: <span style="<?php echo ( $error ) ? 'color: red;' : 'color: green;'; ?>"><?php echo esc_html( $status ); ?></span></strong></p>
		<?php

		return ob_get_clean();
	}


	/**
	 * Returns the timezone string for a site, even if it's set to a UTC offset
	 *
	 * Adapted from http://www.php.net/manual/en/function.timezone-name-from-abbr.php#89155
	 *
	 * TODO: this can be removed when 2.1+ is required as wc_timezone_string() exists
	 *
	 * @since 1.1.2
	 * @return string valid PHP timezone string
	 */
	private function get_wp_timezone_string() {

		// if site timezone string exists, return it
		if ( $timezone = get_option( 'timezone_string' ) ) {
			return $timezone;
		}

		// get UTC offset, if it isn't set then return UTC
		if ( 0 === ( $utc_offset = get_option( 'gmt_offset', 0 ) ) ) {
			return 'UTC';
		}

		// adjust UTC offset from hours to seconds
		$utc_offset *= 3600;

		// attempt to guess the timezone string from the UTC offset
		$timezone = timezone_name_from_abbr( '', $utc_offset );

		// last try, guess timezone string manually
		if ( false === $timezone ) {

			$is_dst = date('I');

			foreach ( timezone_abbreviations_list() as $abbr ) {
				foreach ( $abbr as $city ) {
					if ( $city['dst'] == $is_dst && $city['offset'] == $utc_offset ) {
						return $city['timezone_id'];
					}
				}
			}
		}

		// fallback to UTC
		return 'UTC';
	}


} // end \WC_Twilio_SMS_Notification class
