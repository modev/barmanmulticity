<?php
$min_enableBuilder = false;
$min_builderPassword = 'admin';
$min_errorLogger = true;
$min_allowDebugFlag = true;
$min_cachePath = '/home/barman/public_html/wp-content/cache/cache';
$min_documentRoot = '';
$min_cacheFileLocking = true;
$min_serveOptions['bubbleCssImports'] = false;
$min_serveOptions['maxAge'] = 86400;
$min_serveOptions['minApp']['groupsOnly'] = false;
$min_symlinks = array();
$min_uploaderHoursBehind = 0;
$min_libPath = dirname(__FILE__) . '/lib';
ini_set('zlib.output_compression', '0');
// auto-generated on 2015-05-24 15:42:00
