<?php 


class HorusConf {

	/*ESTOS VALORES POR DEFECTO NO ESTAN VIGENTES PORFAVOR CONFIGURAR EN la interfaz administrativa de wordpress los valores reales en settings-> custom-global-variables */

    private static $url              = "http://horus.dev.konabackend.com"; //prod "https://api.pernod-ricard.io/pr-latam/v1"
    private static $touchpoint_token = 'd6d28f0d-2646-063c-5522-93eefee68df0'; //'67598e3f-5b17-3504-58c7-5881fd0c2786',
    private static $api_key          = 'u3khxjqtrznz4ybtwhp3nuf5';

    private static $interaccion_compra_producto_id     = '56cdda5f6fe2b09954110971';
    private static $interaccion_agregado_al_carrito_id = '57c898c5c1f7c73a51a9cef7';
    private static $interaccion_banner_home_id         = '57bb3d01c2d9831f3b14d8ca';
    private static $interaccion_ingreso_al_checkout    = '57c8993ec1f7c73a51a9cef8';

    
    public static function get_var($nombre) {
        if (!self::$initialized)
        self::initialize();

        return (isset(self::$$nombre))?self::$$nombre:null;
    } 
    
    
    public static function get_variables() {
		 $z = new ReflectionClass(__CLASS__);
		 $propiedades = $z->getStaticProperties();
		 unset($propiedades['initialized']);
		 
		 return array_keys($propiedades);
    }
    
        

    private static $initialized = false;
    

	public static function get_parameters_default() {
		self::initialize();
		
		return array(
			'url' => self::$url,
			'headers' => array(
				'X-TOUCHPOINT-TOKEN' => self::$touchpoint_token, 
				'api_key' => self::$api_key
			)
		);
	}    
    public static function check_properties_configured(){
		$configuradas = true;
		foreach(self::get_variables() as $propiedad){
		 if (isset($GLOBALS['cgv']) && (!isset($GLOBALS['cgv'][$propiedad]))){
				$configuradas = false;
		 }	
	    }	
	    
	    return $configuradas;
	}
	
    /** Si el modulo CGV no tiene aún definidas las variables para configurlas las agregamos, 
     * este solo paso vuele las propiedades de esta clase configurables en la interfaz administratiba de wordpress */
    public static function add_to_admin_configurable_properties(){

		foreach(self::get_variables() as $propiedad){
		 if (isset($GLOBALS['cgv']) && (!isset($GLOBALS['cgv'][$propiedad]))){
				$GLOBALS['cgv'][$propiedad] = null;
		 }	
	    }
		
	}

    public static function initialize() {
        if (self::$initialized)
            return;
	    self::$initialized = true;
	
		//Inicializando las variables  con valores administrables
		foreach(self::get_variables() as $propiedad){
		 if (isset($GLOBALS['cgv']) && (isset($GLOBALS['cgv'][$propiedad]))){
				self::$$propiedad = $GLOBALS['cgv'][$propiedad];
		 }	
	    }       
    }



}
?>
