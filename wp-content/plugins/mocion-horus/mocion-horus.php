<?php

/**
 * Plugin Name: mocion-horus
 * Description: Integacion de Horus a Barman.co.
 * Los valores necesarios de HORUS (como el dominio del server) NECESITAN ser configurados en la interfaz administrativa de wordpress  en settings-> custom-global-variables 
 * Author: Jhina Rivera (jhina.rivera@mocionsoft.com)
 */
include 'libraries/Requests/library/Requests.php';
include 'HorusConf.php';
include 'horus-ajax.php';
include 'horus-order-completed.php';
include 'shortcode_horus.php'; //Aún no se usa
//Inicializamos las variables de horus
add_action('wp_loaded', 'inicializar_horus');

function inicializar_horus() {
    static $ran = true;

    $token = get_horus_token();
    //error_log("USER TOKEN: ".$token);  

    HorusConf::initialize();
}

//Revisamos si el modulo custom-global-variables esta habilitado, este lo necesitamos
add_action('admin_init', 'check_horus_requirements');

function check_horus_requirements() {

    if (is_plugin_active('custom-global-variables/custom-global-variables.php')) {

        if (!HorusConf::check_properties_configured()) {

            HorusConf::add_to_admin_configurable_properties();
            add_action('admin_notices', 'error_cgv_properties_not_configured_notice');
        } else {
            
        }
    } else {
        add_action('admin_notices', 'error_cgv_plugin_not_enabled_notice');
    }
}

//Revisamos si las propiedades de horus ya fueron configuradas en el administrador 
function error_cgv_properties_not_configured_notice() {
    //print the message

    echo '<div class="notice notice-error">
       <p class="dashicons-before dashicons-admin-tools"> !! Las propiedades necesarias de horus no han sido configuradas. 
       En <a href="' + admin_url("options-general.php?page=custom-global-variables") + '">Config</a></p>
    </div>';
    //make sure to remove notice after its displayed so its only displayed when needed.
    //remove_action('admin_notices', 'my_admin_notice');
}

function error_cgv_plugin_not_enabled_notice() {
    //print the message

    echo '<div class="error notice-dismiss">
       <p class="dashicons-before dashicons-admin-tools"> !! Por favor habilitar el plugin CGV (custon global variables) 
       para que horus funcione correctamente. </p>
    </div>';
    //make sure to remove notice after its displayed so its only displayed when needed.
    //remove_action('admin_notices', 'my_admin_notice');
}

register_activation_hook(__FILE__, 'child_plugin_activate');

function child_plugin_activate() {

    // Require parent plugin
    if (!is_plugin_active('custom-global-variables/custom-global-variables.php') and current_user_can('activate_plugins')) {

        echo '<h3>' . __('Enable cgv plugin first  <a href="' . admin_url('plugins.php') . '">&laquo; Return to Plugins</a>', 'ap') . '</h3>';

        //Adding @ before will prevent XDebug output
        @trigger_error(__('Enable cgv plugin first  <a href="' . admin_url('plugins.php') . '">&laquo; Return to Plugins</a>', 'ap', 'ap'), E_USER_ERROR);
    }
}

add_action('woocommerce_before_checkout_form', 'entro_al_checkout');

function entro_al_checkout() {

    return;

    //invocar la interaccion de horus
    $parametros_conexion = configurar_parametros_conexion();

    $parametros = default_parametros();


    $interaccion_id = HorusConf::get_var('interaccion_ingreso_al_checkout');
    $url = $parametros_conexion["url"] . "/interactions/" . $interaccion_id;

    try {
        $response = Requests::post($url, $parametros_conexion["headers"], $parametros);
        $mensaje = $response->body;
    } catch (Exception $e) {
        $mensaje = "error: " . $e->getMessage();
    }

    return $response;
}

/**
 * ACTION LOGIN
 * @param type $creds
 * @return type
 */
function mocion_holus_login($creds = null) {
    return;
    Requests::register_autoloader();
    $parametros = HorusConf::get_parameters_default();
    //si existe la posición email en creds la guarda si no toma $_POST['log']
    $email = $creds["email"] ? $creds["email"] : $_POST['log'];
    $hash = $creds["hash"] ? $creds["hash"] : $_POST['pwd'];
    try {
        $response = Requests::post($parametros["url"] . "/consumers/login", $parametros["headers"], array('email' => $email, 'hash' => $hash));
    } catch (Exception $e) {

        return "No pudimos comunicarnos con el backend" . $e->getMessage();
    }
    $response = json_decode($response->body, true);
    if (isset($response['token'])) {
        //save_token_global($response['token']);
    }

    error_log("logued: " . json_encode($response));
    return $response;
}

add_filter('mocion_holus_login', 'mocion_holus_login');

/**
 * ACTION REGISTER 
 * @return type
 */
function mocion_holus_register() {
    return;
    Requests::register_autoloader();
    $parametros = HorusConf::get_parameters_default();

    $_POST["hash"] = $_POST["pass1"];

    $terms = false;
    if ($_POST["terms"] == "on") {
        $terms = true;
        $_POST["optInPernodRicard"] = $terms;
    }

    $derechos = false;
    if ($_POST["derechos"] == "on") {
        $derechos = true;
        $_POST["optInBrand"] = $derechos;
    }

    unset($_POST['action']);

    return;
    try {
        $response = Requests::post($parametros["url"] . "/consumers", $parametros["headers"], $_POST);
    } catch (Exception $e) {
        return "No pudimos comunicarnos con el backend" . $e->getMessage();
    }


    return json_decode($response->body, true);
}

add_filter('mocion_holus_register', 'mocion_holus_register');

/**
 * ACTION LOGOUT
 * @return type
 */
function mocion_holus_logout() {
    return;
    error_log("saliendonos");

    Requests::register_autoloader();
    $parametros = HorusConf::get_parameters_default();
    $parametros["headers"]['Cache-Control'] = 'no-cache';
    $parametros["headers"]['X-CUSTOMER-DIGITAL-EXTENSION-TOKEN'] = get_horus_token();
    try {
        $response = Requests::post($parametros["url"] . "/consumers/logout", $parametros["headers"], array());
    } catch (Exception $e) {
        return "No pudimos comunicarnos con el backend" . $e->getMessage();
    }
    remove_horus_token();
    $json = json_decode($response->body);
    return $json;
}

add_action('wp_logout', 'mocion_holus_logout');

/**
 * ACTION RESET
 * @param type $params
 * @return boolean
 */
function mocion_holus_reset($params) {
    return;
    Requests::register_autoloader();
    $parametros = HorusConf::get_parameters_default();
//
//    $token = get_token($params["user"]->user_email, $params["user"]->user_pass);
//    var_dump($token);
//
//    $parametros["headers"]['X-CUSTOMER-DIGITAL-EXTENSION-TOKEN'] = $token;
//    $parametros["headers"]['Cache-Control'] = 'no-cache';
//
//    unset($_POST['action']);
//    unset($_POST['token']);
//    $response = Requests::put($parametros["url"], $parametros["headers"], $_POST);
//    var_dump($_POST);
//    echo $response->body;

    $parametros["headers"]['Cache-Control'] = 'no-cache';

    try {
        $response = Requests::post($parametros["url"] . "/consumers/recovery-password", $parametros["headers"], array('email' => $params));
    } catch (Exception $e) {
        $response = "No pudimos comunicarnos con el backend" . $e->getMessage();
    }
    $funciono = false;
    if ($response->status_code && $response->status_code == "200") {
        $funciono = true;
    } else {
        $respuesta = json_decode($response->body, true);
        $mensaje = (isset($respuesta["message"])) ? $respuesta["message"] : "Invalid username or e-mail.";
        wc_add_notice(__($mensaje, 'woocommerce'), 'error');
        $funciono = false;
    }
    return $funciono;
}

add_filter('mocion_holus_reset', 'mocion_holus_reset');

/**
 * ACTION UPDATE 
 * @return type
 */
function mocion_holus_update() {

    return;
    Requests::register_autoloader();
    $parametros = HorusConf::get_parameters_default();

    $token = !empty($_POST['token']) ? $_POST['token'] : get_horus_token();

    if ($token != null) {
        $parametros["headers"]['X-CUSTOMER-DIGITAL-EXTENSION-TOKEN'] = $token;
        $parametros["headers"]['Cache-Control'] = 'no-cache';

        $_POST["firstName"] = $_POST["billing_first_name"];
        $_POST["lastName"] = $_POST["billing_last_name"];

        $_POST["identifyNumber"] = $_POST["billing_cedula"];
        $_POST["type"] = "CC";

        $phone = new stdClass();
        $phone->mobile = $_POST["billing_phone_2"];
        $phone->personal = $_POST["billing_phone"];

        $_POST["phone"] = json_encode($phone);

        $address = new stdClass();
        $address->line1 = $_POST["billing_address_1"] . " " . $_POST["billing_address_2"];
        $address->city = $_POST["billing_city"];
        $address->state = $_POST["billing_state"];

        $_POST["address"] = json_encode($address);

        // $_POST["email"] = $_POST["billing_email"]; 

        unset($_POST['action']);
        unset($_POST['token']);

        try {
            $response = Requests::put($parametros["url"] . "/consumers", $parametros["headers"], $_POST);
        } catch (Exception $e) {
            return "No pudimos comunicarnos con el backend" . $e->getMessage();
        }

        return json_decode($response->body, true);
    }
}

add_filter('mocion_holus_update', 'mocion_holus_update');

/**
 * 
 * @param type $email
 * @param type $pass
 * @return type
 */
function get_token($email, $pass = null) {
    return;
    if (isset($email)) {
        Requests::register_autoloader();
        $parametros = HorusConf::get_parameters_default();
        try {
            $response = Requests::post($parametros["url"] . "/consumers/login", $parametros["headers"], array('email' => $email, 'hash' => $pass));
        } catch (Exception $e) {
            return "No pudimos comunicarnos con el backend" . $e->getMessage();
        }
        $json = json_decode($response->body);
        return $json;
    }
}

/**
 * 
 * @param type $response
 */
function get_horus_token() {
    $token = null;
    $user = wp_get_current_user();
    if (isset($user->ID)) {
        $token = get_user_meta($user->ID, 'hours_token', true);
    }
    return $token;
}

function remove_horus_token($user = null) {
    if (!$user)
        $user = wp_get_current_user();

    if (isset($user->ID)) {
        delete_user_meta($user->ID, 'hours_token');
    }
}

function save_token_global($token, $user = null) {
    error_log("almacenando token: " . $token);
    if (!$user)
        $user = wp_get_current_user();

    if (isset($user->ID)) {
        delete_user_meta($user->ID, 'hours_token');
        error_log("almacenando token2: " . $token . " " . $user->ID);
        error_log("almacenado token3: " . add_user_meta($user->ID, 'hours_token', $token, true));
    }
}

add_filter('save_token_global', 'save_token_global', 10, 2);

/**
 * 
 */
function destroy_token_global() {
    // unset(get_horus_token());
}

add_filter('destroy_token_global', 'destroy_token_global');

/**
 * 
 * @param type $errors
 * @param type $sanitized_user_login
 * @param type $user_email
 * @return type
 */
function registration_errors($errors, $sanitized_user_login, $user_email) {

    if (empty($_POST['first_name']) || !empty($_POST['first_name']) && trim($_POST['first_name']) == '') {
        $errors->add('error', __('<strong>ERROR</strong>: You must include a first name.', 'mydomain'));
    }

    return $errors;
}

add_filter('registration_errors', 'registration_errors', 10, 3);

//add_filter('authenticate', 'demo_auth', 10, 3);

/**
 * 
 * @param type $user
 * @param type $username
 * @param type $password
 */
function demo_auth($user, $username, $password) {
//    return $user;
}
