<html>
    <head>
        <meta charset="UTF-8">
        <title>Requests PHP</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body ng-app='body' ng-controller='ctrl'>
        <form method="POST" action="request.php" style="width: 60%; margin: auto;">
            <input type="hidden" name='action' value='reset'>
            <div class="form-group">
                <input type="email" class="form-control" name="email" placeholder="email">
            </div>
            <button type='submit' class="btn btn-success btn-block">Reset</button>
        </form>
    </body>
</html>