=== WooCommerce Role based prices ===
Contributors: Inpsyde, Alex Frison, jjoeris
Tags: b2b, online shop, german, language, legal disclaimer, ecommerce, e-commerce, commerce, woothemes, wordpress ecommerce, affiliate, store, sales, sell, shop, shopping, cart, checkout, configurable, variable, widgets, reports, download, downloadable, digital, inventory, stock, reports, shipping, tax
Requires at least: 3.3
Tested up to: 3.6
Stable tag: 1.1.2

Woocommerce role based prices Plugin, special Prices for special Customer

== Description ==


= Features =



= Hinweise =


== Installation ==
= Requirements =
* WordPress 3.3*
* PHP 5.2*

= Installation =




== Screenshots ==


== Other Notes ==
= Acknowledgements =

Mike Jolley http://mikejolley.com/ supporting us with the WooCommerce core

= Licence =

 GPL Version 3

= Translations =


== Changelog ==

= 1.0 =
* First stable Release

=1.0.4=
- Fixed Tax loading bug
- improved overall stability
- Fixed wrong displayed prices

= 1.1 =
- Prices can now be reduced by percent instead of a fix price
- Added a menu icon
- Updated translation
- Prices shown in the backend don't change based on the user role anymore
- Each role can now be selected to include or exclude VAT
- Fixed verious pricing and display issues

= 1.1.1 =
- Fixed PHP Notices
- Fixed Variations
- Buy Button is now properly displayed for non logged in users
- Fixed verious small display issues

= 1.1.2 = 
- fixed translation
- fixed calculations for prices on total displays
- fixed tax calculaations for variation and grouped products
- fixed some display issues
