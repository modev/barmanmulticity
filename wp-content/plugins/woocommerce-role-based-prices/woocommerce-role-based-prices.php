<?php 
/**
 * Plugin Name:	Woocommerce Role Based Prices
 * Description:	Role based prices Plugin, which makes it possible to show different prices, for different users
 * Version:		1.1.2
 * Author:		Inpsyde GmbH
 * Author URI:	http://inpsyde.com
 * Licence:		GPLv3
 * Text Domain:	woocommerce-role-bases-prices
 * Domain Path:	/languages
 * Last Change: 28.08.2013 10:45
 *
 * @todo make tax calculation available for roles
 */

if ( ! class_exists( 'Woocommerce_role_based_prices' ) ) {
	
	if ( function_exists( 'add_filter' ) ) {
		add_filter( 'plugins_loaded' ,  array( 'Woocommerce_role_based_prices', 'get_instance' ) );
	}
	
	// initial Version Check
	// 
	register_activation_hook( __FILE__, array(  'Woocommerce_role_based_prices' , 'check_versions' ) ); 
	
	class Woocommerce_role_based_prices { 

		/**
		* The plugins textdomain
		*
		* @static
		* @access	public
		* @since	1.0
		* @var		string
		*/
		public static $textdomain = '';
		
		/**
		* The plugins plugins name
		*
		* @static
		* @access	public
		* @since	1.0
		* @var		string
		*/
		public static $plugin_name = '';
		
		/**
		* The plugins base name
		*
		* @static
		* @access	public
		* @since	1.0
		* @var		string
		*/
		public static $plugin_base_name = '';

		/**
		 *
		 * @static
		 * @access	public
		 * @since	1.0
		 * @var		string
		 */
		
		public static $plugin_url = NULL;

		/**
		 * The plugins textdomain path
		 *
		 * @static
		 * @access	public
		 * @since	1.0	
		 * @var		string
		 */
		public static $textdomainpath = '';

		/**
		 * Instance holder
		 *
		 * @static
		 * @access	private
		 * @since	1.0
		 * @var		NULL | Woocommerce_role_based_prices
		 */
		private static $instance = NULL;
		
		
		/**
		 * pricing capability for roles
		 *
		 * @static
		 * @access	private
		 * @since	1.0
		 * @var		string
		 */
		public static $price_capability  = 'Woocommerce_role_based_prices_price';
		
		/**
		 * tax capability for roles
		 *
		 * @static
		 * @access	private
		 * @since	1.0
		 * @var		string
		 */
		public static $tax_capability  = 'Woocommerce_role_based_prices_tax';
		
		
		/**
		 * TRUE if taxes should be calculated for 
		 * current user 
		 *
		 * @static
		 * @access	private
		 * @since	1.0
		 * @var		bool
		 */
		public static $current_user_is_taxable  = TRUE;
		
		/**
		 * TRUE if special prices should be calculated for 
		 * current user 
		 *
		 * @static
		 * @access	private
		 * @since	1.0
		 * @var		bool
		 */
		public static $current_user_is_pricable = FALSE;
		
		/**
		 * Main Role of the current user
		 *
		 * @static
		 * @access	private
		 * @since	1.0
		 * @var		string
		 */
		public static $current_user_main_role = NULL;
		
		/**
		 * Method for ensuring that only one instance of this object is used
		 *
		 * @static
		 * @access	public
		 * @since	1.0
		 * @return	Woocommerce_role_based_prices
		 */
		public static function get_instance() {

			if ( ! self::$instance )
				self::$instance = new self;
			return self::$instance;
		}

		/**
		 * Setting up some data, initialize translations and start the hooks
		 * 
		 * @access	public
		 * @since	1.0
		 * @uses	add_filter
		 * @return	void
		 */
		public function __construct () {
			
			// Textdomain
			self::$textdomain = $this->get_textdomain();
			
			// Textdomain Path
			self::$textdomainpath = $this->get_domain_path();
			
			// plugin name
			self::$plugin_name = $this->get_plugin_name();
			
			// plugin basename
			self::$plugin_base_name = $this->get_plugin_basename();
			
			// Initialize the localization
			$this->load_plugin_textdomain();
			
			// set the user based settings like role based pricing and taxes
			$this->set_user_based_settings();
			
			// ad the role prices to price tab of each product
			add_filter( 'woocommerce_product_write_panels', array( $this, 'add_role_prices_to_write_panels' ) );
			
			// add tab to product meta box
			add_filter( 'woocommerce_product_write_panel_tabs', array( $this, 'add_role_prices_tab_to_write_panels' ) );
			
			// add role manager
			add_filter( 'admin_menu', array( $this, 'extend_admin_menu' ), 99 );
			
			// save meta data on product write panel
			add_filter( 'woocommerce_process_product_meta', array( $this, 'save_process_product_meta' ), 10, 2 );

			add_filter( 'woocommerce_free_price_html', array( $this, 'prevent_empty_price' ), 10, 2 );

			add_filter( 'woocommerce_is_purchasable', array( $this, 'check_if_purchasable' ), 10 );

			require 'classes/woocommerce-role-based-prices-fixed-prices.php';

			$Woocommerce_role_based_prices_pricing = new Woocommerce_role_based_prices_fixed_prices();
			
			// require Auto Updater
			require_once 'classes/class-Woocommerce_role_based_prices_Auto_Update.php';
		}
		
		/**
		 * Get a value of the plugin header
		 *
		 * @since 1.0
		 * @uses get_plugin_data, ABSPATH
		 * @param string $value
		 * @return string The plugin header value
		 */
		protected function get_plugin_header( $value = 'TextDomain' ) {

			if ( ! function_exists( 'get_plugin_data' ) )
				require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

			$plugin_data = get_plugin_data( __FILE__ );
			$plugin_value = $plugin_data[ $value ];

			return $plugin_value;
		}

		/**
		 * Get the Textdomain
		 *
		 * @access	public
		 * @since	1.0
		 * @return	string The plugins textdomain
		 */
		public function get_textdomain() {

			return $this->get_plugin_header( 'TextDomain' );
		}

		/**
		 * Get the Textdomain Path where the language files are located
		 *
		 * @access	public
		 * @since	1.0
		 * @return	string The plugins textdomain path
		 */
		public function get_domain_path() {

			return $this->get_plugin_header( 'DomainPath' );
		}
		
		/**
		 * Get the plugin name
		 *
		 * @access	public
		 * @since	1.0
		 * @return	string The plugins name
		 */
		public function get_plugin_name() {
			// The Plugins Name
			return $this->get_plugin_header( 'Name' );
		}
		
		/**
		 * Get the plugins basename
		 *
		 * @access	public
		 * @since	1.0
		 * @return	string the plugins basename
		 */
		public function get_plugin_basename() {
			// The Plugins Basename
			return plugin_basename( __FILE__ );
		}
		
		/**
		 * Load the localization
		 *
		 * @since	1.0
		 * @access	public
		 * @uses	load_plugin_textdomain, plugin_basename
		 * @return	void
		 */
		public function load_plugin_textdomain() {

			load_plugin_textdomain( self::$textdomain, FALSE, dirname( plugin_basename( __FILE__ ) ) . self::$textdomainpath );
		}
		
		/**
		 * Check, if current installation fits all Versions, deactivate plugin, if it fails
		 * 
		 * @access public
		 * @since 1.0
		 * @uses version_compare, deactivate_plugins, wp_sprintf, is_plugin_inactive
		 * @return void
		 */
		public static function check_versions() {
			
			$Woocommerce_role_based_prices = Woocommerce_role_based_prices::get_instance();
			
			// check wp version
			if ( ! version_compare( $GLOBALS['wp_version'], '3.0', '>=' ) ) {
				deactivate_plugins( __FILE__ );
				die( 
					wp_sprintf( 
						'<strong>%s:</strong> ' . 
						__( 'Sorry, This plugin requires WordPress 3.0+', $Woocommerce_role_based_prices->get_textdomain() )
						, self::get_plugin_data( 'Name' )
					)
				);
			}
			
			// check php version
			if ( version_compare( PHP_VERSION, '5.2.0', '<' ) ) {
				deactivate_plugins( __FILE__ ); // Deactivate ourself
				die( 
					wp_sprintf(
						'<strong>%1s:</strong> ' . 
						__( 'Sorry, This plugin has taken a bold step in requiring PHP 5.0+, Your server is currently running PHP %2s, Please bug your host to upgrade to a recent version of PHP which is less bug-prone. At last count, <strong>over 80%% of WordPress installs are using PHP 5.2+</strong>.', $Woocommerce_role_based_prices->get_textdomain() )
						, self::get_plugin_data( 'Name' ), PHP_VERSION 
					)
				);
			}
			
			if ( is_plugin_inactive('woocommerce/woocommerce.php') ) {
				deactivate_plugins( __FILE__ ); // Deactivate ourself
				die( 
					__( 'The Plugin <strong>Woocommerce</strong> is not active, but mandatory to use this plugin.', $Woocommerce_role_based_prices->get_textdomain() )
				);
			}
		}
		
		/**
		 * Set pricing and tax settigs
		 * 
		 * @access public
		 * @author jj
		 * @since 1.0
		 * @return void
		 */
		public function set_user_based_settings() {
			
			$tmp_user = new WP_User( get_current_user_id() );
			
			self::$current_user_is_pricable = ( 0 < count( $tmp_user->roles ) && $tmp_user->has_cap( Woocommerce_role_based_prices::$price_capability ) );
			
			if( self::$current_user_is_pricable )
				self::$current_user_is_taxable = $tmp_user->has_cap( Woocommerce_role_based_prices::$tax_capability );	
				
			if( 0 < count( $tmp_user->roles )  )	
				self::$current_user_main_role = array_shift( $tmp_user->roles );
			else
				self::$current_user_main_role = '';
		}
		
		/**
		 * Adds role based pricing to the product write panels
		 * 
		 * @access public
		 * @author jj
		 * @since 1.0
		 * @return void
		 */
		public function add_role_prices_to_write_panels() {
			global $woocommerce_pricing, $post;
			
			$_product = get_product( $post->ID );


			echo '<div id="wc_b2b_role_based_prices" class="panel woocommerce_options_panel wc-metaboxes-wrapper" style="display: block;">';

			if( $_product->product_type == 'variable' ): ?>
				<div class="rbp-variation wc-metaboxes ui-sortable">
			<?php

				foreach( $_product->get_children( ) as $c => $child ): 
						$c_product = get_product( $child );
					
						$title = "";
						foreach( $c_product->variation_data as $data )
							$title .= $data . ' ';

					?>

					
					<div id="woocommerce-pricing-rules-wrap" class="rbp-variation wc-metabox closed" rel="<?php echo $c; ?>">
						<h3>
							<div class="handlediv" title="Hin- und herschalten"></div>
							<strong class="attribute_name">Variation: <?php echo $title; ?></strong>
						</h3>


						<table cellpadding="0" cellspacing="0" class="rbp-variation-data wc-metabox-content">
									<tbody>
										<tr>
											<td class="attribute_name">
												<?php 
												 foreach ( $this->get_pricing_roles() as $role_id => $role ) 
													 $this->get_role_price_input( $role_id, $role, $title );
															   
												 ?>   
											</td>
										</tr>
									</tbody>
								</table>

						</div>   
					
				<?php endforeach; ?>
			</div>
			<?php else: ?>
				
					<div id="woocommerce-pricing-rules-wrap">
						<?php 
						 foreach ( $this->get_pricing_roles() as $role_id => $role ) 
							 $this->get_role_price_input( $role_id, $role );
									   
						 ?>   
						</div>   
					<div class="clear"></div>
				
			<?php

			endif;
			echo '</div>';
		}
		 
		/**
		 * Get input fields for prices assigned to roles
		 * 
		 * @access public
		 * @param string $role_id string representation of role
		 * @param object $role WP_Roles
		 * @author jj, ap 
		 * @since 1.0
		 * @return string html with price for roles
		 */
		public function get_role_price_input( $role_id, $role, $variation = NULL ) {
			
			if( is_null( $variation ) ): ?>
			
				<p class="form-field _regular_price_per_unit_field ">
					<label for="<?php echo '_b2b_price_for_' . $role_id; ?>"><?php echo $role[ 'name' ] ?></label>
					<input type="text" class="short" name="<?php echo'_b2b_price_for_' . $role_id ?>" id="<?php echo'_b2b_price_for_' . $role_id ?>" value="<?php echo get_post_meta( get_the_ID(), '_b2b_price_for_' . $role_id, True ); ?>" placeholder=""> 
					<span>
						<select name="<?php echo '_rpb_price_type_' . $role_id; ?>" class="short">
							<option value="currency" <?php selected( get_post_meta( get_the_ID(), '_rpb_price_type_' . $role_id, TRUE ), 'currency' ); ?>><?php echo get_woocommerce_currency_symbol(); ?></option>
							<option value="percent"  <?php selected( get_post_meta( get_the_ID(), '_rpb_price_type_' . $role_id, TRUE ), 'percent' ); ?>><?php _e( 'Percent', $this->get_textdomain() ); ?></option>
						</select>
					</span>
				</p>
			<?php

			else:
				$variation = str_replace(' ', '_', trim( $variation ) ); ?>

				<p class="form-field _regular_price_per_unit_field ">
					<label for="<?php echo '_b2b_price_for_' . $role_id; ?>"><?php echo $role[ 'name' ] ?></label>
					<input type="text" class="short" name="<?php echo'_b2b_price_for_' . $variation . '_' . $role_id ?>" id="<?php echo'_b2b_price_for_' . $role_id ?>" value="<?php echo get_post_meta( get_the_ID(), '_b2b_price_for_' . $variation . '_' . $role_id, True ); ?>" placeholder=""> 
					<span>
						<select name="<?php echo '_rpb_price_type_' . $variation . _ . $role_id; ?>" class="short" style="width: 20%!important; margin-left:10px;">
							<option value="currency" <?php selected( get_post_meta( get_the_ID(), '_rpb_price_type_' . $variation . '_' . $role_id, TRUE ), 'currency' ); ?>><?php echo get_woocommerce_currency_symbol(); ?></option>
							<option value="percent"  <?php selected( get_post_meta( get_the_ID(), '_rpb_price_type_' . $variation . '_' . $role_id, TRUE ), 'percent' ); ?>><?php _e( 'Percent', $this->get_textdomain() ); ?></option>
						</select>
					</span>
				</p>

			<?php
			endif;

		}
		
		
		/**
		 * Save the special prices for roles
		 * 
		 * @access public
		 * @author jj 
		 * @since 1.0
		 * @param int $post_id
		 * @param object $post
		 */
		public function save_process_product_meta( $post_id, $post ) {

			foreach ( $_POST as $field => $val ) {
				if ( substr( $field, 0, 11 ) == "_b2b_price_" || substr( $field, 0, 16 ) == "_rpb_price_type_" ) {
					if ( isset( $val ) )
						update_post_meta( $post_id, $field, stripslashes( $val ) );
				}
			}
		}
			
		/**
		 * Add Menu for role editing
		 * 
		 * @access public
		 * @author jj 
		 * @since 1.0
		 */
		public function extend_admin_menu() {
			// check for managing right
			$show_in_menu = current_user_can( 'manage_woocommerce' ) ? 'woocommerce' : false;
			$menu_title =  __( 'Pricing Roles', $this->get_textdomain() );
			add_submenu_page( $show_in_menu, $menu_title, $menu_title, 'manage_woocommerce', 'Woocommerce_role_based_prices', array( $this, 'b2b_role_manager' ) );
		}
		
		 /**
		 * Role editing
		 * 
		 * @access public
		 * @author jj 
		 * @since 1.0
		 */
		public function b2b_role_manager() {
			global $wp_roles;
			
			if ( ! isset( $wp_roles ) ) {
				$wp_roles = new WP_Roles();
			}
			
			if( isset( $_POST[ '_wpnonce' ] ) && wp_verify_nonce( $_POST[ '_wpnonce' ] ) && isset( $_POST[ 'is_role_priceable' ] ) ) {
				
				if( isset( $_POST[ 'add_new_role' ] ) && isset( $_POST[ 'new_role' ] ) && ! empty( $_POST[ 'new_role' ] ) ) {
					// Customer role
					add_role( 'wc_b2b_' . sanitize_title( $_POST[ 'new_role' ] ), $_POST[ 'new_role' ], array(
						'read'						=> TRUE,
						'edit_posts'				=> FALSE,
						'delete_posts'				=> FALSE
					) );
					// set if role is pricable
					if( isset( $_POST[ 'is_role_priceable' ][ 'new_role' ] ) )
						$wp_roles->add_cap( 'wc_b2b_' . sanitize_title( $_POST[ 'new_role' ] ), self::$price_capability );
						
					// set if role is taxable
					if( isset( $_POST[ 'is_role_taxable' ][ 'new_role' ] ) )
						$wp_roles->add_cap( 'wc_b2b_' . sanitize_title( $_POST[ 'new_role' ] ), self::$tax_capability );
				}
				
				foreach ( $wp_roles->roles as $role_id => $role) {
					
					// remove role(s)
					if( isset( $_POST[ 'delete_role_' . $role_id ] ) ) {
						if(  0 === strpos( $role_id, 'wc_b2b_' ) ) {
							remove_role( $role_id );
						}
					}

					// add 'tax included' cap
					if( !isset( $_POST[ 'incl_vat_role_' . $role_id ] ) ) {
						$wp_roles->add_cap( $role_id, 'rbp_role_includes_tax' );
					} else {
						$wp_roles->remove_cap( $role_id, 'rbp_role_includes_tax' );
					}


					// save roles
					if( isset( $_POST[ 'save_roles' ] ) ) {
						
						// save if role is a pricable role
						if( in_array( $role_id, array_keys( $_POST[ 'is_role_priceable' ] ) ) )
							$wp_roles->add_cap( $role_id, self::$price_capability );
						else 
							$wp_roles->remove_cap( $role_id, self::$price_capability );
						
						// save if role is taxable
						if( isset( $_POST[ 'is_role_taxable' ] ) && in_array( $role_id, array_keys( $_POST[ 'is_role_taxable' ] ) ) )
							$wp_roles->add_cap( $role_id, self::$tax_capability );
						else 
							$wp_roles->remove_cap( $role_id, self::$tax_capability );
					}
				}
			}
			?>
			<div class="wrap woocommerce">
			
				<div id="icon-woocommerce" class="icon32 icon32-posts-shop_coupon"><br /></div>
				<h2><?php echo __( 'Pricing Roles', $this->get_textdomain() ); ?></h2>
				
				
				<div id="poststuff" class="metabox-holder has-right-sidebar">
				
					<div id="side-info-column" class="inner-sidebar">
						<div id="side-sortables" class="meta-box-sortables ui-sortable">
							<div id="woocommerce-inpsyde" class="postbox">
								<h3 class="hndle"><span><?php _e( 'Powered by', $this->get_textdomain() ); ?></span></h3>
								<div class="inside">
									<p style="text-align: center;"><a href="http://inpsyde.com"><img src="<?php echo plugin_dir_url( __FILE__ ) . 'images/inpsyde_logo.png' ?>" style="border: 7px solid #fff;" /></a></p>
									<p><?php _e( 'This plugin is powered by <a href="http://inpsyde.com">Inpsyde.com</a> - Your expert for WordPress, BuddyPress and bbPress.', $this->get_textdomain() ); ?></p>
								</div>
							</div>
						</div>
					</div>
					
					<div id="post-body">
						<div id="post-body-content">
							<div id="normal-sortables" class="meta-box-sortables ui-sortable">
							
								<div id="poststuff" class="woocommerce-roles-wrap">
									<form method="post">
										<?php wp_nonce_field(); ?>
										<table class="widefat">
											<thead>
												<th class="manage-column column-cb check-column"></th>
												<th>
													<?php  _e( 'Role', $this->get_textdomain() ); ?>
												</th>
												<?php /*
												<th>
													<?php  _e( 'Calculate Tax', $this->get_textdomain() ); ?>
												</th>
												*/?>
												<th>
													<?php  _e( 'Delete', $this->get_textdomain() ); ?>
												</th>

												<th>
													<?php  _e( 'without VAT', $this->get_textdomain() ); ?>
												</th>

											</thead>
											
											<tfoot>
												<th class="manage-column column-cb check-column"></th>
												<th>
													<?php  _e( 'Role', $this->get_textdomain() ); ?>
												</th>
												<?php /*
												<th>
													<?php  _e( 'Calculate Tax', $this->get_textdomain() ); ?>
												</th>
												*/?>
												<th>
													<input type="submit" class="button-primary" name="save_roles" value="<?php _e( 'Save Changes', self::get_textdomain() ) ?>" style="float: right; text-decoration: none;font-size: 12px!important; line-height: 13px; padding: 3px 8px; cursor: pointer; font-family: sans-serif; border-width: 1px; border-style: solid; -webkit-border-radius: 11px; border-radius: 11px; -moz-box-sizing: content-box; -webkit-box-sizing: content-box; box-sizing: content-box;" />
												</th>
											</tfoot>
											
											<tbody>
												<?php foreach ( $wp_roles->roles as $role_id => $role ) : ?>
													<?php 
													if ( ! isset( $role[ 'capabilities' ][ self::$price_capability ] ) )
														$role[ 'capabilities' ][ self::$price_capability ] = false;
				
													if ( ! isset( $role[ 'capabilities' ][ self::$tax_capability ] ) )
														$role[ 'capabilities' ][ self::$tax_capability ] = false;
													?>
													<tr>
														<td> 
															<input type="checkbox" class="checkbox" id="role_<?php echo $role_id; ?>" name="is_role_priceable[<?php echo $role_id; ?>]" <?php checked( $role[ 'capabilities' ][ self::$price_capability ], 1 ); ?> />
														</td>
														<td>
															<?php echo __( $role[ 'name' ], $this->get_textdomain() ); ?>
														</td>
														<?php /*
														<td> 
															<input type="checkbox" class="checkbox" id="role_tax_<?php echo $role_id; ?>" name="is_role_taxable[<?php echo $role_id; ?>]" <?php checked( $role[ 'capabilities' ][ self::$tax_capability ], 1 ); ?> />
														</td>
														*/ ?>
														 <td>
														 	<?php if(  0 === strpos( $role_id, 'wc_b2b_' ) ) : ?>
																<input type="submit" class="button-secondary" name="delete_role_<?php echo $role_id; ?>" value="<?php _e( 'delete', $this->get_textdomain() ); ?>" />
															<?php else : ?>
																<?php _e( 'Can\'t be deleted', $this->get_textdomain() ); ?>
															<?php endif; ?>
														</td>

														<td>
															<input type="checkbox" name="incl_vat_role_<?php echo $role_id; ?>" value="true" <?php checked( @$role[ 'capabilities' ][ 'rbp_role_includes_tax' ] != 1 ); ?> />
														</td>

													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</form>
								</div>
								<br class="clear" />
								
								<div id="new_role_form" class="postbox">
									<h3 class="hndle"><span><?php _e( 'Add New Role', $this->get_textdomain() ); ?></span></h3>
									<div class="inside">
										<form method="post">
											<?php wp_nonce_field(); ?>
											<table class="form-table">
												<tbody>
													<tr valign="top">
														<th scope="row">
															<label for="new_role"><?php _e( 'Active', $this->get_textdomain() ); ?>:</label>
														</th>
														<td>
															<input type="checkbox" class="checkbox" id="new_role" name="is_role_priceable[new_role]" value="1" <?php checked( 1, 1 ); ?> />
														</td>
													</tr>
													<tr valign="top">
														<th scope="row">
															<label for="new_role"><?php _e( 'Role Name', $this->get_textdomain() ); ?>:</label>
														</th>
														<td>
															<input type="text" id="new_role" name="new_role" /> 
															<input type="submit" class="button-primary" name="add_new_role" value="<?php _e( 'Add new Role', $this->get_textdomain() ) ?>" style="float: right;" />
														</td>
													</tr>
												</tbody>
											</table>
										</form>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				
				</div>

				
			</div>
			<?php
		}
		
		/**
		 * Roles, which are defined to have special prices, when logged in
		 * 
		 * @access public
		 * @author jj 
		 * @since 1.0
		 * @return array $all_roles array of WP_Roles
		 */
		public function get_pricing_roles() {
		   global $wp_roles;
		   
		   if ( !isset( $wp_roles ) ) 
				$wp_roles = new WP_Roles();
		   
		   return array_filter( $wp_roles->roles, array( $this, 'is_pricable' ) );
		}
		
		/**
		 * filter roles which are selected for pricing
		 * 
		 * @access public
		 * @author jj 
		 * @since 1.0
		 * @return array $filtered array of WP_Roles->roles
		 */
		public function is_pricable( $array ) {
			
			if ( isset( $array[ 'capabilities' ][ self::$price_capability ] ) )
				return $array[ 'capabilities' ][ self::$price_capability ];
			else
				return FALSE;
		}
		
		/**
		 * Adds the role based pricing tab to each product write panel
		 *
		 * @since	1.0
		 * @author	jj
		 * @access	public
		 * @return	void
		 */
		public function add_role_prices_tab_to_write_panels() {
			
			$product_write_panel_tabs = '<li><a style="background: #f1f1f1 url(' . plugins_url( '/images/icon.png', __FILE__ ) . ') no-repeat 5px 5px; padding: 5px 5px 5px 28px;" href="#wc_b2b_role_based_prices">' . __( 'Role based Prices', $this->get_textdomain() ) . '</a></li>';
			echo apply_filters( 'Woocommerce_role_based_prices_product_write_panel_tabs' , $product_write_panel_tabs );
		}


		/**
		 * Allows the regular price to be empty and still show the role based price
		 *
		 * @since	1.0.4
		 * @author	dw
		 * @access	public
		 * @return	price
		 */
		public function prevent_empty_price( $default, $_product ) {

			if ( self::$current_user_is_pricable ) {

				$Woocommerce_role_based_prices_pricing = new Woocommerce_role_based_prices_fixed_prices();

				return woocommerce_price( $Woocommerce_role_based_prices_pricing->get_rolebased_price( $_product ) );
			} else {

				return $default;
			}
		}


		/**
		 * Just check if a product is purchasable
		 *
		 * @since	1.0.4
		 * @author	dw
		 * @access	public
		 * @return	bool
		 */
		public function check_if_purchasable() {

			if ( self::$current_user_is_pricable || ! is_user_logged_in() )
				return true;
			else
				return false;
		}
	}

}
?>