<?php
/**
 * B2B price calculations
 *
 * @todo make tax calculation available for roles
 *
 * @author jj
 * @since 0.1
 */

class Woocommerce_role_based_prices_fixed_prices {

	public function __construct() {

		// b2b prices are making only sense when logged in and is pricable
		if( ! is_user_logged_in() || ! Woocommerce_role_based_prices::$current_user_is_pricable )
			return;

		// Start a PHP session
		if ( !session_id() )
			session_start();

		// add_filter( 'woocommerce_cart_item_price_html',				array( $this, 'on_display_cart_item_price_html' ), 10, 3);
		add_filter( 'woocommerce_grouped_price_html',				array( $this, 'on_price_html' ), 10, 2);
		add_filter( 'woocommerce_variable_price_html',				array( $this, 'on_price_html' ), 10, 2);
		add_filter( 'woocommerce_sale_price_html',					array( $this, 'on_price_html' ), 10, 2);
		add_filter( 'woocommerce_price_html',						array( $this, 'on_price_html' ), 10, 2);
		add_filter( 'woocommerce_empty_price_html',					array( $this, 'on_price_html' ), 10, 2);
		add_filter( 'woocommerce_before_calculate_totals', 			array( $this, 'on_calculate_totals' ) );
		add_filter( 'woocommerce_de_print_including_tax',			array( $this, 'enable_print_including_tax' ) );
		add_filter( 'woocommerce_countries_ex_tax_or_vat',			array( $this, 'remove_exlude_tax_message' ) );
		add_filter( 'woocommerce_sale_flash',						array( $this, 'remove_sale_flash' ) );

		add_filter( 'woocommerce_available_variation', 				array( $this, 'update_variations' ), 10, 3 );
	}

	/**
	 * Displaying item price as html
	 *
	 * @access public
	 * @author jj
	 * @since 0.1
	 * @param string $html
	 * @param array $cart_item
	 * @param string $cart_item_key
	 */
	public function on_display_cart_item_price_html( $html, $cart_item, $cart_item_key ) {

		if( $cart_item[ 'data' ]->product_type == 'variation' )
			return woocommerce_price( $this->get_variation_price( $cart_item[ 'data' ] ) );
		else
			return woocommerce_price( $this->get_rolebased_price( $cart_item[ 'data' ] ) );
	}

	/**
	 * Show price in html
	 *
	 * @access public
	 * @author jj
	 * @since 0.1
	 * @param string $html
	 * @param object $_product
	 */
	public function on_price_html( $html, $_product) {
		if( is_null( $this->get_rolebased_price( $_product ) ) )
			return __( 'Please pick a variation to show the price.', 'woocommerce-role-bases-prices' );

		return woocommerce_price( $this->get_rolebased_price( $_product ) );
	}



	public function update_variations( $arr, $_product, $variation ) {

		$price_field = '_b2b_price_for_' . implode('_', $arr[ 'attributes' ] ) . '_' . Woocommerce_role_based_prices::$current_user_main_role;
		$type_field = '_rpb_price_type_' . implode('_', $arr[ 'attributes' ] ) . '_' . Woocommerce_role_based_prices::$current_user_main_role;

		$role_based_price = get_post_meta( $_product->id, $price_field, TRUE );
		$type = get_post_meta( $_product->id, $type_field, TRUE );


		if( ! empty( $role_based_price ) ) {

			// Never show role based prices within backend
			if ( is_admin() ) {
				return $arr;

			// Show percentage
			} elseif ( $type == 'percent' ) {
				$arr[ 'price_html' ] = $this->get_html_price( ( $variation->get_price() / 100 ) * $role_based_price );

			// Show default role based price
			} else {
				$arr[ 'price_html' ] = $this->get_html_price( $role_based_price );
			}
		}

 		return $arr;
	}


	public function get_variation_price( $_product, $_include_tax = true ) {
		$price_field = '_b2b_price_for_' . implode('_', $_product->variation_data ) . '_' . Woocommerce_role_based_prices::$current_user_main_role;
		$type_field = '_rpb_price_type_' . implode('_', $_product->variation_data ) . '_' . Woocommerce_role_based_prices::$current_user_main_role;

		$role_based_price = get_post_meta( $_product->id, $price_field, TRUE );
		$type = get_post_meta( $_product->id, $type_field, TRUE );

		if( ! empty( $role_based_price ) ) {

			// Show percentage
			if ( $type == 'percent' ) {

				$role_based_price = ( $_product->get_price() / 100 ) * $role_based_price;

				if ( $_include_tax )
					return $this->get_price_with_tax( $_product, $role_based_price );
				else
					return $role_based_price;

			// Show default role based price
			} else {

				if ( $_include_tax )
					return $this->get_price_with_tax( $_product, $role_based_price );
				else
					return $role_based_price;
			}
		}

		return $_product->get_price();

	}


	public function get_html_price( $price, $with_tax = FALSE ) {

		return '<span class="price"><span class="amount">' . woocommerce_price( $price ) . '</span></span>';
	}

	/**
	 *  adjust price on calculatiing totals
	 *
	 * @access public
	 * @todo check for variation price
	 * @author jj
	 * @since 0.1
	 * @param object $_cart
	 */
	public function on_calculate_totals( $_cart ) {

			global $woocommerce;

			if ( sizeof( $_cart->cart_contents ) > 0 ) {
				foreach ( $_cart->cart_contents as $cart_item ) {
					if( $cart_item[ 'data' ]->product_type == 'variation' )
						$cart_item[ 'data' ]->price = $this->get_variation_price( $cart_item[ 'data' ], false );
					else
						$cart_item[ 'data' ]->price = $this->get_rolebased_price( $cart_item[ 'data' ], false );
				}
			}
	 }


	 /**
	  * get the rolebased price
	  *
	  * @access public
	  * @author jj
	  * @since 0.1
	  * @param object $_product
	  */
	 public function get_rolebased_price( $_product, $_include_tax = true ) {

	 	$role_has_tax = ( apply_filters( 'woocommerce_de_print_including_tax', true ) );

	 	if( $_product->product_type == 'variable' ) return NULL;

	 	if( Woocommerce_role_based_prices::$current_user_is_pricable ) {

			$user_role = Woocommerce_role_based_prices::$current_user_main_role;

			if( $user_role == '' ) return $_product->get_price();

			$field = '_b2b_price_for_' . $user_role;
			$role_based_price = get_post_meta( $_product->id, $field, TRUE );

			// Convert price from 100,00 -> 100.00
			// 	therefore functions like woocommerce_price() can format
			// 	the values correctly
			$role_based_price = str_replace( ',', '.', $role_based_price );

			$type = get_post_meta( $_product->id, '_rpb_price_type_' . $user_role, TRUE );

			if( ! empty( $role_based_price ) ) {

				// Never show role based prices within backend
				if ( is_admin() && !( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
					return $_product->get_price();

				// Show percentage
				} elseif ( $type == 'percent' ) {

					$role_based_price = ( ( $_product->get_price() / 100 ) * $role_based_price );

					if ( $_include_tax )
						return $this->get_price_with_tax( $_product, $role_based_price );
					else
						return $role_based_price;

				// Show default role based price
				} else {
					if ( $_include_tax )
						return $this->get_price_with_tax( $_product, $role_based_price );
					else
						return $role_based_price;
				}
			} else {

				// Fallback to default price
				$role_based_price = $_product->get_price();
			}
	 	} else {
			return $_product->get_price();
	 	}


	 	// Check if a tax gets added or subtracted
	 	//  and return the resulting price
	 	if ( $_include_tax )
 			return $this->get_price_with_tax( $_product, $role_based_price );
 		else
 			return $role_based_price;
	 }

	 /**
	  * Enable print incl. Tax of german extension
	  *
	  * @access public
	  * @author jj
	  * @since 0.1
	  * @return bool print tax or not
	  */
	 public function enable_print_including_tax() {
		if ( current_user_can( 'rbp_role_includes_tax' ) ) {
			return true;
		} else {
			return false;
		}
	 	// disabled until tax ist activated
		//return Woocommerce_role_based_prices::$current_user_is_taxable;
	 }

	 /**
	  * Disable print excl. Tax
	  *
	  * @access public
	  * @author jj
	  * @since 0.1
	  * @return string message
	  */
	 public function remove_exlude_tax_message( $message ) {

	 	if( ! Woocommerce_role_based_prices::$current_user_is_taxable ) {
	 		return '';
	 	}
	 	return $message;
	 }

	 /**
	  * Disable sale flash on special price users
	  *
	  * @access public
	  * @author jj
	  * @since 0.1
	  * @return string message
	  */
	 public function remove_sale_flash( $message ) {

	 	if( Woocommerce_role_based_prices::$current_user_is_pricable ) {
	 		return '';
	 	}
	 	return $message;
	 }

	 /**
	  * add tax to a price
	  *
	  * @access public
	  * @author jj
	  * @since 0.1
	  *
	  * @param object $_product
	  * @param string $price
	  * @return float $price
	  */
	 public function get_price_with_tax( $_product, $price ) {

	 	if ( is_admin() && !( defined( 'DOING_AJAX' ) && DOING_AJAX ) )
	 		return $price;

	 	$role_has_tax = ( apply_filters( 'woocommerce_de_print_including_tax', true ) );
	 	$include_tax = ( get_option( 'woocommerce_prices_include_tax' ) == 'yes' );

	 	// Subtract taxes if role would have them automatically
		if ( $_product->is_taxable() && $include_tax && !$role_has_tax ) {

			$_tax = new WC_Tax();
			$tax_rates 		= $_tax->get_shop_base_rate( $_product->tax_class );
			$taxes 			= $_tax->calc_tax( $price, $tax_rates, false );
			$tax_amount		= $_tax->get_tax_total( $taxes );
			$price 			= round( $price - $tax_amount, 2);

		// Add taxes
		} else if ( $_product->is_taxable() && !$include_tax && $role_has_tax ) {

			$_tax = new WC_Tax();
			$tax_rates 		= $_tax->get_shop_base_rate( $_product->tax_class );
			$taxes 			= $_tax->calc_tax( $price, $tax_rates, false );
			$tax_amount		= $_tax->get_tax_total( $taxes );
			$price 			= round( $price + $tax_amount, 2);
		}

		return $price;
	 }

	 /**
	  * Just a dummy method for codestyling localization
	  */
	 public function dummyLangMethod() {
		__( 'Administrator', $this->textdomain() );
		__( 'Editor', $this->textdomain() );
		__( 'Author', $this->textdomain() );
		__( 'Contributor', $this->textdomain() );
		__( 'Subscriber', $this->textdomain() );
		__( 'Customer', $this->textdomain() );
		__( 'Shop Manager', $this->textdomain() );
	 }
}

?>