<?php
/*
Plugin Name: WooCommerce Review for Discount
Plugin URI: http://woothemes.com/woocommerce
Description: Provide discounts to incentivize users who submit reviews for specific or any products
Version: 1.5.2
Author: 75nineteen Media
Author URI: http://www.75nineteen.com/woocommerce/review-for-discount
Requires at least: 3.3.1

Copyright: © 2012 75nineteen Media.
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
    require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '67ae2070dd8d3f3624925857efda6117', '18671' );

if (is_woocommerce_active()) {

    /**
     * Localisation
     **/
    load_plugin_textdomain( 'wc_review_discount', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    class SFN_ReviewDiscount {
        private $db_version = '1.5.2';

        public function __construct() {
            // install
            register_activation_hook(__FILE__, array($this, 'install'));

            // menu
            add_action('admin_menu', array($this, 'menu'), 20);

            // settings styles and scripts
            add_action( 'admin_enqueue_scripts', array($this, 'settings_scripts'));

            // process settings
            add_action( 'admin_post_sfn_rd_new', array($this, 'create'));
            add_action( 'admin_post_sfn_rd_edit', array($this, 'edit'));
            add_action( 'admin_post_sfn_rd_email', array($this, 'update_email'));

            // comment posted
            add_action('comment_post', array($this, 'comment_posted'));
            add_action('comment_unapproved_to_approved', array($this, 'comment_updated'));

            //add_filter('woocommerce_coupon_code', array($this, 'format_coupon_code'));

            // Upgrade
            if ( get_option( 'wrd_db_version', 0 ) < $this->db_version )
                add_action( 'plugins_loaded', array( $this, 'install' ) );

            if ( get_option( 'wrd_expiry_fix', false) === false ) {
                add_action( 'plugins_loaded', array( $this, 'fix_expiration' ), 25 );
            }
        }

        function format_coupon_code( $code ) {
            return strtoupper($code);
        }

        function install() {
            global $wpdb;
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            // default email settings
            $email = get_option('wrd_email_settings', array());

            if (empty($email)) {
                    $email = array(
                    'subject'   => __('Your store coupon', 'wc_review_discount'),
                    'message'   => __("Thank you for your review. Please use the coupon code below to get discounts from our store:", 'wc_review_discount') . "\n\n{code}"
                );
                update_option('wrd_email_settings', $email);

            }

            $wpdb->hide_errors();

            $table = $wpdb->prefix . 'wrd_discounts';
            $sql = "CREATE TABLE $table (
id bigint(20) NOT NULL AUTO_INCREMENT,
type varchar(25) NOT NULL,
amount double(12,2) NOT NULL DEFAULT 0.00,
individual INT(1) NOT NULL DEFAULT 0,
before_tax INT(1) NOT NULL DEFAULT 0,
free_shipping INT(1) NOT NULL DEFAULT 0,
send_mode varchar(25) NOT NULL,
send_to_verified INT (1) NOT NULL DEFAULT 0,
product_ids TEXT NOT NULL,
category_ids TEXT NOT NULL,
usage_limit INT(1) NOT NULL DEFAULT 0,
expiry_value INT(3) NOT NULL DEFAULT 0,
expiry_type varchar(25) NOT NULL DEFAULT '',
usage_count BIGINT(20) NOT NULL DEFAULT 0,
all_products varchar(20) NOT NULL DEFAULT 'no',
unique_email INT(1) NOT NULL DEFAULT 0,
date_added DATETIME NOT NULL,
KEY type (type),
PRIMARY KEY  (id)
)";
            dbDelta($sql);

            $table = $wpdb->prefix . 'wrd_sent_coupons';
            $sql = "CREATE TABLE $table (
comment_id bigint(20) NOT NULL,
discount_id bigint(20) NOT NULL,
coupon_id bigint(20) NOT NULL,
author_email varchar(255) NOT NULL,
KEY comment_id (comment_id),
KEY discount_id (discount_id),
KEY coupon_id (coupon_id)
)";
            dbDelta($sql);

            if ( get_option('wrd_db_update_usage_limit', false) === false ) {
                $wpdb->query("UPDATE {$wpdb->prefix}wrd_discounts SET `usage_limit` = `limit`");
            }
            update_option('wrd_db_update_usage_limit', true);

            update_option( 'wrd_db_version', $this->db_version );
        }

        function menu() {
            add_submenu_page('woocommerce', __('Review for Discount', 'wc_review_discount'),  __('Review for Discount', 'wc_review_discount') , 'manage_woocommerce', 'wc-review-discount', array($this, 'settings'));
        }

        function settings() {
            global $wpdb, $woocommerce;

            $tab = (isset($_GET['tab'])) ? $_GET['tab'] : 'discounts';

            if ($tab == 'discounts') {
                // load the discounts
                $discounts = $this->getDiscounts();

                include dirname(__FILE__) .'/settings.php';
            } elseif ($tab == 'new') {
                $products = $wpdb->get_results("SELECT * FROM `". $wpdb->prefix ."posts` WHERE `post_type` = 'product' AND `post_status` = 'publish' ORDER BY `post_title` ASC");
                $cats       = get_terms('product_cat');

                $discount = array(
                    'id'            => 0,
                    'type'          => 'fixed_cart',
                    'amount'        => '',
                    'send_mode'     => 'immediately',
                    'before_tax'    => 0,
                    'individual'    => 0,
                    'free_shipping' => 0,
                    'limit'         => 0,
                    'verified'      => 0,
                    'all_products'  => '',
                    'products'      => array(),
                    'categories'    => array(),
                    'expiry_value'  => 0,
                    'expiry_type'   => '',
                    'unique_email'  => 0
                );

                $wrd_new = sfn_session_get( 'wrd_new' );
                if ( $wrd_new ) {
                    $discount = array_merge( $discount, $wrd_new );
                }

                include dirname(__FILE__) .'/form.php';
            } elseif ($tab == 'email') {
                $emailSettings = get_option('wrd_email_settings', array());

                include dirname(__FILE__) .'/email-settings.php';
            } elseif ($tab == 'delete') {
                $id = (int)$_GET['id'];

                // delete
                $wpdb->query( $wpdb->prepare("DELETE FROM `{$wpdb->prefix}wrd_sent_coupons` WHERE `discount_id` = %d", $id) );
                $wpdb->query( $wpdb->prepare("DELETE FROM `{$wpdb->prefix}wrd_discounts` WHERE `id` = %d", $id) );

                wp_redirect('admin.php?page=wc-review-discount&tab=discounts&deleted=true');
                exit;
            } elseif ($tab == 'edit') {
                $products = $wpdb->get_results("SELECT * FROM `". $wpdb->prefix ."posts` WHERE `post_type` = 'product' AND `post_status` = 'publish' ORDER BY `post_title` ASC");
                $cats       = get_terms('product_cat');

                $row        = $wpdb->get_row( $wpdb->prepare("SELECT * FROM `{$wpdb->prefix}wrd_discounts` WHERE `id` = %d", $_GET['id']) );

                $discount = array(
                    'id'            => $row->id,
                    'type'          => $row->type,
                    'amount'        => $row->amount,
                    'send_mode'     => $row->send_mode,
                    'all_products'  => $row->all_products,
                    'verified'      => $row->send_to_verified,
                    'before_tax'    => $row->before_tax,
                    'individual'    => $row->individual,
                    'free_shipping' => $row->free_shipping,
                    'limit'         => $row->usage_limit,
                    'products'      => (!empty($row->product_ids)) ? unserialize($row->product_ids) : array(),
                    'categories'    => (!empty($row->category_ids)) ? unserialize($row->category_ids) : array(),
                    'expiry_value'  => $row->expiry_value,
                    'expiry_type'   => $row->expiry_type,
                    'unique_email'  => $row->unique_email
                );

                $wrd_edit = sfn_session_get( 'wrd_edit' );

                if ( $wrd_edit ) {
                    $discount = array_merge( $discount, $wrd_edit );
                }

                include dirname(__FILE__) .'/form.php';
            }


        }

        function settings_scripts() {
            if ( !function_exists('wc_add_notice') ) {
                woocommerce_admin_scripts();
            } else {
                $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
                wp_register_script( 'ajax-chosen', WC()->plugin_url() . '/assets/js/chosen/ajax-chosen.jquery' . $suffix . '.js', array('jquery', 'chosen'), WC()->version );
                wp_register_script( 'chosen', WC()->plugin_url() . '/assets/js/chosen/chosen.jquery' . $suffix . '.js', array('jquery'), WC()->version );
            }

            wp_enqueue_script( 'woocommerce_admin' );
            wp_enqueue_script('farbtastic');
            wp_enqueue_script( 'ajax-chosen' );
            wp_enqueue_script( 'chosen' );
            wp_enqueue_script( 'jquery-ui-sortable' );

            wp_enqueue_style('chosen', plugins_url() .'/woocommerce/assets/css/chosen.css');

            ?>
            <style type="text/css">
            .chzn-choices li.search-field .default {
                width: auto !important;
            }

            .chzn-container-multi .chzn-choices .search-field input { height: auto !important; }
            </style>
            <?php
        }

        function update_email() {
            $default = get_option('wrd_email_settings', array());

            $post = array_map('stripslashes_deep', $_POST);

            $email = array(
                'subject'   => (isset($post['subject'])) ? $post['subject'] : null,
                'message'   => (isset($post['message'])) ? $post['message'] : null
            );

            $settings = array_merge($default, $email);

            update_option('wrd_email_settings', $settings);
            wp_redirect('admin.php?page=wc-review-discount&tab=email&updated=true');
            exit;
        }

        function create() {
            $data = array(
                'type'              => $_POST['type'],
                'amount'            => $_POST['amount'],
                'send_mode'         => $_POST['sending_mode'],
                'send_to_verified'  => 0,
                'all_products'      => (isset($_POST['all_products'])) ? $_POST['all_products'] : 'no'
            );

            if (isset($_POST['send_to_verified'])) {
                $data['send_to_verified']  = ($_POST['send_to_verified'] == 1) ? 1 : 0;
            }

            if (isset($_POST['before_tax']) && $_POST['before_tax'] == 'yes') {
                $data['before_tax'] = 1;
            } else {
                $data['before_tax'] = 0;
            }

            if (isset($_POST['individual_use']) && $_POST['individual_use'] == 'yes') {
                $data['individual'] = 1;
            } else {
                $data['individual'] = 0;
            }

            if (isset($_POST['free_shipping']) && $_POST['free_shipping'] == 'yes') {
                $data['free_shipping'] = 1;
            } else {
                $data['free_shipping'] = 0;
            }

            if (isset($_POST['limit']) && $_POST['limit'] == 1) {
                $data['usage_limit'] = 1;
            } else {
                $data['usage_limit'] = 0;
            }

            if (isset($_POST['unique_email']) && $_POST['unique_email'] == 1) {
                $data['unique_email'] = 1;
            } else {
                $data['unique_email'] = 0;
            }

            if (isset($_POST['product_ids']) && !empty($_POST['product_ids'])) {
                $data['products'] = array();
                foreach ($_POST['product_ids'] as $pId) {
                    $data['products'][] = $pId;
                }
            }

            if (isset($_POST['product_cats']) && !empty($_POST['product_cats'])) {
                $data['categories'] = array();
                foreach ($_POST['product_cats'] as $cId) {
                    $data['categories'][] = $cId;
                }
            }

            if (!empty($_POST['expiry_value']) && !empty($_POST['expiry_type'])) {
                $data['expiry_value']   = $_POST['expiry_value'];
                $data['expiry_type']    = $_POST['expiry_type'];
            } else {
                $data['expiry_value']   = 0;
                $data['expiry_type']    = '';
            }

            if ( $data['all_products'] == 'yes' ) {
                $data['products'] = array();
                $data['categories'] = array();
            }

            $stat = $this->newDiscount($data);

            if (is_wp_error($stat)) {
                sfn_session_set( 'wrd_new', $data );
                wp_redirect( 'admin.php?page=wc-review-discount&tab=new&error='. urlencode($stat->get_error_message()) );
            } else {
                wp_redirect('admin.php?page=wc-review-discount&created=1');
            }

            exit;
        }

        function edit() {
            $id = $_POST['id'];
            $data = array(
                'type'              => $_POST['type'],
                'amount'            => $_POST['amount'],
                'send_mode'         => $_POST['sending_mode'],
                'send_to_verified'  => 0,
                'all_products'      => (isset($_POST['all_products'])) ? $_POST['all_products'] : 'no'
            );

            if (isset($_POST['send_to_verified'])) {
                $data['send_to_verified']  = ($_POST['send_to_verified'] == 1) ? 1 : 0;
            }

            if (isset($_POST['before_tax']) && $_POST['before_tax'] == 'yes') {
                $data['before_tax'] = 1;
            } else {
                $data['before_tax'] = 0;
            }

            if (isset($_POST['individual_use']) && $_POST['individual_use'] == 'yes') {
                $data['individual'] = 1;
            } else {
                $data['individual'] = 0;
            }

            if (isset($_POST['free_shipping']) && $_POST['free_shipping'] == 'yes') {
                $data['free_shipping'] = 1;
            } else {
                $data['free_shipping'] = 0;
            }

            if (isset($_POST['limit']) && $_POST['limit'] == 1) {
                $data['usage_limit'] = 1;
            } else {
                $data['usage_limit'] = 0;
            }

            if (isset($_POST['unique_email']) && $_POST['unique_email'] == 1) {
                $data['unique_email'] = 1;
            } else {
                $data['unique_email'] = 0;
            }

            $data['product_ids'] = array();
            if (isset($_POST['product_ids']) && !empty($_POST['product_ids'])) {

                foreach ($_POST['product_ids'] as $pId) {
                    $data['product_ids'][] = $pId;
                }
            }

            if (!empty($data['product_ids'])) {
                $data['product_ids'] = serialize($data['product_ids']);
            } else {
                $data['product_ids'] = '';
            }

            $data['category_ids'] = array();
            if (isset($_POST['product_cats']) && !empty($_POST['product_cats'])) {

                foreach ($_POST['product_cats'] as $cId) {
                    $data['category_ids'][] = $cId;
                }
            }

            if (!empty($data['category_ids'])) {
                $data['category_ids'] = serialize($data['category_ids']);
            } else {
                $data['category_ids'] = '';
            }

            if (!empty($_POST['expiry_value']) && !empty($_POST['expiry_type'])) {
                $data['expiry_value']   = $_POST['expiry_value'];
                $data['expiry_type']    = $_POST['expiry_type'];
            } else {
                $data['expiry_value']   = 0;
                $data['expiry_type']    = '';
            }

            if ( $data['all_products'] == 'yes' ) {
                $data['product_ids'] = '';
                $data['category_ids'] = '';
            }

            $stat = $this->editDiscount($id, $data);

            if (is_wp_error($stat)) {
                $data['products'] = (isset($data['product_ids']) && !empty($data['product_ids'])) ? unserialize($data['product_ids']) : array();
                unset($data['product_ids']);

                $data['categories'] = (isset($data['product_cats']) && !empty($data['product_cats'])) ? unserialize($data['product_cats']) : array();
                unset($data['product_cats']);

                sfn_session_set( 'wrd_edit', $data );
                wp_redirect( 'admin.php?page=wc-review-discount&tab=edit&id='. $id .'&error='. urlencode($stat->get_error_message()) );
            } else {
                wp_redirect('admin.php?page=wc-review-discount&updated=1');
            }

            exit;
        }

        function comment_updated($id) {
            $this->comment_posted($id, true);
        }

        function comment_posted($commentId, $approved = null) {
            global $wpdb, $woocommerce;

            // we first load the comment to see if it has been approved or not
            $comment = get_comment($commentId);

            $postId         = $comment->comment_post_ID;
            $product        = sfn_get_product($postId);
            $categories     = get_the_terms($postId, 'product_cat');
            $discount       = false;
            $found          = array();
            $sent           = false;

            if (! $product || !$product->exists()) return;

            $cats = array();
            // get the category IDs
            if ($categories) foreach ($categories as $category) {
                $cats[] = $category->term_id;
            }

            // check if the product id matches any of our discounts
            $discounts = $this->getDiscounts();

            foreach ($discounts as $disc) {
                if ( $disc->all_products == 'yes' ) {
                    if ($disc->send_mode == 'immediately' || $disc->send_mode == 'approved' && ($comment->comment_approved == 1 || $approved === true) ) {
                        $found[] = $disc;
                        break;
                    }
                } elseif (empty($disc->category_ids) && empty($disc->product_ids)) {
                    //$discount = $disc;
                    //break;
                    continue;
                } else {
                    // if the category matches, we create and send the coupon
                    $discCats = (empty($disc->category_ids)) ? array() : unserialize($disc->category_ids);
                    foreach ($cats as $catId) {
                        if (in_array($catId, $discCats)) {
                            // check if comment has been approved or if we do not require comments to be approved
                            if ($disc->send_mode == 'immediately' || $disc->send_mode == 'approved' && ($comment->comment_approved == 1 || $approved === true) ) {
                                $found[] = $disc;
                                break 2;
                            }
                        }
                    }

                    // search for product IDs
                    $discProds = (empty($disc->product_ids)) ? array() : unserialize($disc->product_ids);
                    if (in_array($postId, $discProds)) {
                        // check if comment has been approved or if we do not require comments to be approved
                        if ($disc->send_mode == 'immediately' || $disc->send_mode == 'approved' && ($comment->comment_approved == 1 || $approved === true) ) {
                            $found[] = $disc;
                            break;
                        }
                    }
                }
            }

            // if valid, create coupon and send it to the registered email address
            if (! empty($found) ) {
                foreach ( $found as $disc ) {
                    // check if coupon is set to only be sent once per email
                    $sent_num = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}wrd_sent_coupons WHERE discount_id = %d AND author_email = %s", $disc->id, $comment->comment_author_email) );
                    
                    if ($disc->unique_email && $sent_num > 0) continue;

                    // if coupon for the same product has already been sent to this customer, do nothing.
                    if ($this->isSent($comment->comment_ID, $disc->id, $comment->comment_author_email)) continue;
                    if ($this->isSentToEmail($comment->comment_ID, $comment->comment_author_email)) continue;

                    // check if this requires that the reviewer be a verified owner of the product
                    if (function_exists('woocommerce_customer_bought_product') && $disc->send_to_verified == 1) {
                        if (! woocommerce_customer_bought_product($comment->comment_author_email, $comment->user_id, $postId) ) return;
                    }

                    // create the coupon
                    $now    = time();
                    $code   = $this->generateCouponCode();
                    $coupon_array = array(
                        'post_title'    => $code,
                        'post_author'   => 1,
                        'post_date'     => date("Y-m-d H:i:s", $now),
                        'post_status'   => 'publish',
                        'comment_status'=> 'closed',
                        'ping_status'   => 'closed',
                        'post_name'     => $code,
                        'post_parent'   => 0,
                        'menu_order'    => 0,
                        'post_type'     => 'shop_coupon'
                    );
                    $coupon_id = wp_insert_post($coupon_array);
                    $wpdb->query("UPDATE {$wpdb->prefix}posts SET post_status = 'publish' WHERE ID = $coupon_id");

                    $product_ids = '';
                    if (!empty($disc->product_ids)) {
                        $pids = unserialize($disc->product_ids);
                        foreach ($pids as $pid) {
                            $product_ids .= $pid .',';
                        }
                        $product_ids = rtrim($product_ids, ',');
                    }

                    $category_ids = '';
                    if (!empty($disc->category_ids)) {
                        $cids = unserialize($disc->category_ids);
                        foreach ($cids as $cid) {
                            $category_ids .= $cid.',';
                        }
                        $category_ids = rtrim($category_ids, ',');
                    }

                    $expiry = '';
                    if ($disc->expiry_value > 0 && !empty($disc->expiry_type)) {
                        $exp = $disc->expiry_value .' '. $disc->expiry_type;
                        $ts = strtotime(current_time('mysql') . " +$exp");

                        if ($ts !== false) {
                            $expiry = date('Y-m-d', $ts);
                        }
                    }

                    $validProducts = '';
                    $validCategories = '';

                    if ($disc->all_products == 'no' && $disc->usage_limit == 1) {
                        if ( !empty($disc->product_ids) ) {
                            $validProducts = implode(',', unserialize($disc->product_ids));
                        }

                        if ( !empty($disc->category_ids) ) {
                            $validCategories = implode(',', unserialize($disc->category_ids));
                        }
                    }

                    update_post_meta($coupon_id, 'discount_type', $disc->type);
                    update_post_meta($coupon_id, 'coupon_amount', $disc->amount);
                    update_post_meta($coupon_id, 'individual_use', ($disc->individual == 0) ? 'no' : 'yes');
                    update_post_meta($coupon_id, 'product_ids', $validProducts);
                    update_post_meta($coupon_id, 'exclude_product_ids', '');
                    update_post_meta($coupon_id, 'usage_limit', 1);
                    update_post_meta($coupon_id, 'expiry_date', $expiry);
                    update_post_meta($coupon_id, 'apply_before_tax', ($disc->before_tax == 0) ? 'no' : 'yes');
                    update_post_meta($coupon_id, 'free_shipping', ($disc->free_shipping == 0) ? 'no' : 'yes');
                    update_post_meta($coupon_id, 'product_categories', $validCategories);
                    update_post_meta($coupon_id, 'exclude_product_categories', '');
                    update_post_meta($coupon_id, 'minimum_amount', '');

                    $email  = $comment->comment_author_email;
                    $insert = array(
                        'comment_id'    => $comment->comment_ID,
                        'discount_id'   => $disc->id,
                        'coupon_id'     => $coupon_id,
                        'author_email'  => $email
                    );
                    $wpdb->insert( $wpdb->prefix .'wrd_sent_coupons', $insert );

                    $mailer = $woocommerce->mailer();

                    $emailSettings  = get_option('wrd_email_settings');
                    $email_heading  = $emailSettings['subject'];
                    $message        = wpautop($emailSettings['message']);

                    // replace variables
                    $product_name   = $product->get_title();
                    $valid          = '';
                    $amount         = $disc->amount;

                    if (floor($disc->amount) == $disc->amount) {
                        // remove 0s
                        $amount = number_format($disc->amount);
                    }

                    switch ($disc->type) {
                        case 'fixed_cart':
                        case 'fixed_product':
                            $amount = woocommerce_price($amount);
                            break;

                        case 'percent':
                        case 'percent_product':
                            $amount = $amount .'%';
                            break;
                    }

                    $vars   = array('{store_name}', '{code}', '{discount_amount}', '{product_name}', '{valid_products}');
                    $reps   = array(get_bloginfo('name'), $code, $amount, $product_name, $valid);
                    $subject= str_replace($vars, $reps, $emailSettings['subject']);
                    $message= str_replace($vars, $reps, $message);

                    $message = $mailer->wrap_message( $subject, $message );
                    $mailer->send($email, $subject, $message);

                    break;
                }
            }
        }

        function generateCouponCode() {
            global $wpdb;

            $chars = 'abcdefghijklmnopqrstuvwxyz01234567890';
            do {
                $code = '';
                for ($x = 0; $x < 8; $x++) {
                    $code .= $chars[ rand(0, strlen($chars)-1) ];
                }

                $check = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM `{$wpdb->prefix}posts` WHERE `post_title` = %s AND `post_type` = 'shop_coupon'", $code) );

                if ($check == 0) break;
            } while (true);

            return $code;
        }

        function isSent($commentId, $discountId, $authorEmail) {
            global $wpdb;

            return (bool)$wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM `{$wpdb->prefix}wrd_sent_coupons` WHERE `comment_id` = %d AND `discount_id` = %d AND `author_email` = %s", $commentId, $discountId, $authorEmail) );
        }

        function isSentToEmail($commentId, $authorEmail) {
            global $wpdb;

            $comment = get_comment($commentId);
            $postId  = $comment->comment_post_ID;

            // get all comments for this post
            $comments   = get_comments('post_id='. $postId);

            if ( is_array($comments) ) {
                foreach ( $comments as $cmt ) {
                    if ( $cmt->comment_ID != $commentId && $cmt->comment_author_email == $authorEmail ) {
                        return true;
                    }
                }
            }

            return false;
        }

        function newDiscount($data) {
            global $wpdb;

            $all = $wpdb->get_results("SELECT `product_ids`, `category_ids` FROM {$wpdb->prefix}wrd_discounts");
            foreach ($all as $discount) {
                if (!empty($discount->product_ids)) {
                    $ids = unserialize($discount->product_ids);

                    if (isset($data['products']) && !empty($data['products'])) {
                        foreach ($data['products'] as $pid) {
                            if (in_array($pid, $ids)) {
                                return new WP_Error('wrd_error', sprintf(__('You already have a discount for %s.', 'wc_review_discount'), get_the_title($pid)));
                            }
                        }
                    }
                }

                if (!empty($discount->category_ids)) {
                    $ids = unserialize($discount->category_ids);

                    if (isset($data['categories']) && !empty($data['categories'])) {
                        foreach ($data['categories'] as $cid) {
                            if (in_array($cid, $ids)) {
                                $term = get_term_by( 'id', $cid, 'product_cat' );
                                return new WP_Error('wrd_error', sprintf(__('You already have a discount for the category: %s.', 'wc_review_discount'), $term->name));
                            }
                        }
                    }
                }
            }

            if (isset($data['products'])) {
                if (empty($data['products'])) {
                    $data['products'] = '';
                } else {
                    $data['products'] = serialize($data['products']);
                }
            } else {
                $data['products'] = '';
            }

            if (isset($data['categories'])) {
                if (empty($data['categories'])) {
                    $data['categories'] = '';
                } else {
                    $data['categories'] = serialize($data['categories']);
                }
            } else {
                $data['categories'] = '';
            }

            $now = time();
            $wpdb->query("INSERT INTO `{$wpdb->prefix}wrd_discounts` (
                `type`, `amount`, `individual`, `before_tax`, `send_mode`, `send_to_verified`, `product_ids`, `category_ids`, `usage_limit`, `expiry_value`, `expiry_type`, `unique_email`, `date_added`
            ) VALUES (
                '". $data['type'] ."', '". $data['amount'] ."', '". $data['individual'] ."', '". $data['before_tax'] ."', '". $data['send_mode'] ."', '". $data['send_to_verified'] ."',
                '". $data['products'] ."', '". $data['categories'] ."', '". $data['usage_limit'] ."', '". $data['expiry_value'] ."', '". $data['expiry_type'] ."', '". $data['unique_email'] ."', '". date("Y-m-d H:i:s", $now) ."'
            )");

            return true;
        }

        function editDiscount($id, $data) {
            global $wpdb;

            $all = $wpdb->get_results("SELECT `product_ids`, `category_ids` FROM {$wpdb->prefix}wrd_discounts WHERE `id` != $id");
            foreach ($all as $discount) {
                if (!empty($discount->product_ids)) {
                    $ids = unserialize($discount->product_ids);

                    if (isset($data['product_ids']) && !empty($data['product_ids'])) {
                        $products = unserialize($data['product_ids']);
                        foreach ($products as $pid) {
                            if (in_array($pid, $ids)) {
                                return new WP_Error('wrd_error', sprintf(__('You already have a discount for %s.', 'wc_review_discount'), get_the_title($pid)));
                            }
                        }
                    }
                }

                if (!empty($discount->category_ids)) {
                    $ids = unserialize($discount->category_ids);

                    if (isset($data['category_ids']) && !empty($data['category_ids'])) {
                        $categories = unserialize($data['category_ids']);
                        foreach ($categories as $cid) {
                            if (in_array($cid, $ids)) {
                                $term = get_term_by( 'id', $cid, 'product_cat' );
                                return new WP_Error('wrd_error', sprintf(__('You already have a discount for the category: %s.', 'wc_review_discount'), $term->name));
                            }
                        }
                    }
                }
            }

            $wpdb->update("{$wpdb->prefix}wrd_discounts", $data, array('id' => $id));
        }

        function getDiscounts() {
            global $wpdb;

            $rows = array();
            $results = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}wrd_discounts` ORDER BY `date_added` ASC");

            foreach ($results as $row) {
                $row->sent = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM `{$wpdb->prefix}wrd_sent_coupons` WHERE `discount_id` = %d", $row->id) );
                $rows[] = $row;
            }

            return $rows;
        }

        function fix_expiration() {
            global $wpdb;

            $discounts = $wpdb->get_results("SELECT `s`.`coupon_id`, `d`.`expiry_value`, `d`.`expiry_type` FROM {$wpdb->prefix}wrd_sent_coupons `s`, {$wpdb->prefix}wrd_discounts `d` WHERE `s`.`discount_id` = `d`.`id`");

            if ( count($discounts) > 0 ) {
                foreach ( $discounts as $disc ) {
                    if ($disc->expiry_value > 0 && !empty($disc->expiry_type)) {
                        $coupon = get_post( $disc->coupon_id );
                        $exp = $disc->expiry_value .' '. $disc->expiry_type;
                        $ts = strtotime( $coupon->post_date . " +$exp");

                        if ($ts !== false) {
                            $expiry = date('Y-m-d', $ts);
                            update_post_meta( $disc->coupon_id, 'expiry_date', $expiry );
                        }
                    }
                }
            }

            update_option( 'wrd_expiry_fix', true);
        }
    }

    $sfnReviewDiscount = new SFN_ReviewDiscount();

    if (! function_exists('sfn_get_product') ) {
        function sfn_get_product( $id ) {
            if ( function_exists('get_product') ) {
                return get_product( $id );
            } else {
                return new WC_Product( $id );
            }
        }
    }

    if (! function_exists('sfn_session_set') ) {
        function sfn_session_set( $name, $value ) {
            global $woocommerce;

            if ( isset($woocommerce->session) ) {
                $woocommerce->session->$name = $value;
            } else {
                $_SESSION[ $name ] = $value;
            }
        }
    }

    if (! function_exists('sfn_session_get') ) {
        function sfn_session_get( $name ) {
            global $woocommerce;

            if ( isset($woocommerce->session) ) {
                return (isset($woocommerce->session->$name)) ? $woocommerce->session->$name : null;
            } else {
                return (isset($_SESSION[ $name ])) ? $_SESSION[ $name ] : null;
            }
        }
    }

    if (! function_exists('sfn_session_unset') ) {
        function sfn_session_unset( $name ) {
            global $woocommerce;

            if ( isset($woocommerce->session) ) {
                unset($woocommerce->session->$name);
            } else {
                unset($_SESSION[$name]);
            }
        }
    }
}
