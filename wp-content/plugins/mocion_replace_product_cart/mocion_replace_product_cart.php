<?php

/**
  Plugin Name: Mocion Replace product cart
  Description: Replace product cart
  Version: 0.1
  Author: Jhina Rivera
 */
add_action('woocommerce_shop_popup_item', 'woocommerce_template_loop_replace_to_cart', 12);

function woocommerce_template_loop_replace_to_cart($producto_original = null) {

    if ($producto_original) {

        $product = new WC_product($producto_original);

        $defaults = array(
            'quantity' => 1,
            'producto_original' => $producto_original,
            'class' => implode(' ', array_filter(array(
                'button',
                'product_type_' . $product->product_type,
                $product->is_purchasable() && $product->is_in_stock() ? 'replace_to_cart_button' : '',
                $product->supports('ajax_replace_to_cart') ? 'ajax_replace_to_cart' : 'ajax_replace_to_cart'
            )))
        );

        wc_get_template('loop/replace-to-cart.php', $defaults);
    }
}

add_action('wp_enqueue_scripts', 'replace_product_enqueue_scripts');

function replace_product_enqueue_scripts() {

// 	wp_enqueue_script( 'popupoferta', plugins_url( '/popupOferta.js', __FILE__ ), array('jquery'), '1.0', true );


    wp_enqueue_script('popupoferta', plugins_url('/js/popupOferta.js', __FILE__), array(), filemtime(dirname(__FILE__)), true);

    wp_localize_script('popupoferta', 'popupoferta', array(
        'ajax_url' => admin_url('admin-ajax.php')
    ));
}

add_action('wp_ajax_replace_product_from_cart', 'replace_product_from_cart');
add_action('wp_ajax_nopriv_replace_product_from_cart', 'replace_product_from_cart');

function replace_product_from_cart() {

    if (!isset($_POST["prod_to_remove"]))
        return;

    $prod_to_remove = (int) $_POST["prod_to_remove"];

    var_dump($prod_to_remove);

    global $woocommerce;
    $cart = $woocommerce->cart;

    foreach ($cart->get_cart() as $cart_item_key => $cart_item) {

        $producto = $cart_item['data'];

        // Get the Variation or Product ID
        $prod_id = isset($producto->id) ? $producto->id : 0;

        if ($prod_to_remove == $prod_id) {

            /////////////////////////////////////////////////////////
            //////////// REMOVE PRODUCTO A CARRITO /////////////////
            /////////////////////////////////////////////////////////

            WC()->cart->remove_cart_item($cart_item_key);

            /////////////////////////////////////////////////////////
            //////////// AGREGAR PRODUCTO A CARRITO /////////////////
            /////////////////////////////////////////////////////////

            $producto_original = new WC_product($prod_to_remove);

            $crosssells = $producto_original->get_cross_sells();
            WC()->cart->add_to_cart(reset($crosssells), 1);
        }
    }

    wp_die();
}


add_action('wp_footer', 'popupOferta'); 
function popupOferta() { 

    echo '<div id="container_oferta">
        <div class="container_oferta_content">
            <header class="banner headerclass " role="banner">
                <div class="container">
                    <div id="logo" class="logocase">
                        <img src="'.get_home_url().'/wp-content/uploads/2014/10/logotipo.png"
                                alt="Barman Club | Licor a domicilio">
                        <p class="kad_tagline belowlogo-text"></p>
                    </div> <!-- Close #logo -->
                    <div class="label-oferta">OFERTA DEL MES</div> 
                    <span class="cerrar-popup">X</span>
                </div> <!-- Close Container -->
            </header>
            <div id="content" class="container"></div>     
        </div>
    </div>'; 
}