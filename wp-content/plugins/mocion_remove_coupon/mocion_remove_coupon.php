<?php

/**
  Plugin Name: Mocion Alter link Remove Coupon
  Description: Reemplazar el simbolo de # en la url de eliminar cupon
  Version: 0.1
  Author: Jhina Rivera
 */

add_filter( 'woocommerce_cart_totals_coupon_html', 'encode_number_symbol_in_coupon', 10, 1 , 2);

function encode_number_symbol_in_coupon($value, $coupon=null){
    
    return (str_replace("remove_coupon=#", "remove_coupon=!!!" , $value));
    
}

add_filter( 'woocommerce_coupon_code', 'decode_number_symbol_in_coupon', 10, 1 );

function decode_number_symbol_in_coupon($coupon_code) {
    
     return (str_replace("!!!", "#" , $coupon_code));
}

