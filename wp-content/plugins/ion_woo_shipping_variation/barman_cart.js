jQuery(document).ready(function($) {
    

jQuery(document).on('change', '.cart_product_variation',function() {
    var variation_id = jQuery(this).val();
   
    var product_id   = jQuery(this).attr( 'data-product_id' );

    var data = {
        'action': 'change_product_variation',
        'variation_id':variation_id,
        'posicion':jQuery(this).parents('.cart_item').index(),
        'product_id':product_id
    };
    
	// We can also pass the url value separately from ajaxurl for front end AJAX implementations
	jQuery.post(barmancart.ajax_url, data, function(response) {
                console.log(response);
		jQuery( 'body' ).trigger( 'update_checkout' );
	});    
       
});    
    

});


