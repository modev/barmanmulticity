<?php
/**
 * Detectar o establecer ciudad del usuario de manera global, para ser usada como referencia en las operaciones
 * de los productos que dependen de la ciudad en la que uno se encuentre
 */

add_action('wp_ajax_nueva_ciudad', 'prefix_ajax_nueva_ciudad');
add_action('wp_ajax_nopriv_nueva_ciudad', 'prefix_ajax_nueva_ciudad');
add_shortcode('mocion-ciudades-dropdown', 'mocion_ciudades_dropdown_shortcode');
add_action('woocommerce_init', 'initsesion');

function initsesion () {
    //Esto es necesario o no nos reconoce un usuario
    $wc = WC();
    //if (isset($wc) && $wc->session && !WC()->session->has_session())
    //WC()->session->set_customer_session_cookie(true);
}


// [mocion-ciudades-dropdown]
function mocion_ciudades_dropdown_shortcode($atts) {

	
    if (WC() && WC()->session && !WC()->session->has_session())
    WC()->session->set_customer_session_cookie(true);
    
    $atributos = shortcode_atts( array(
        'reload' => true,
        'show-title' => false,
    ), $atts );    

    $args = array(
        'post_type' => 'ciudades',
        'post_status' => 'publish'
    );

    $ciudades = new WP_Query($args);
    if (!$ciudades->have_posts())
        return "";


    $ciudad = WC()->customer->get_shipping_city();

    if (!$ciudad) {
        $ciudad = mocion_ciudad_por_defecto();
    }

    wp_enqueue_script('citiesdropdown', plugins_url('citiesdropdown.js', __FILE__), array(), null, true);

    set_query_var('atributos', $atributos);
    set_query_var('ciudades', $ciudades);
    set_query_var('ciudad', $ciudad);
    ob_start();
    load_template(dirname(__FILE__) . '/citiesdropdown.template.php',false);
    $contenido = ob_get_contents();
    ob_end_clean();
    return $contenido;
}

function prefix_ajax_nueva_ciudad() {
    //Esto es necesario o no nos reconoce un usuario
     if (WC() && WC()->session && !WC()->session->has_session())
     WC()->session->set_customer_session_cookie(true);
	
    $customer = new WC_Customer();
    $ciudad = sanitize_text_field($_POST['ciudad']);

    actualizar_ciudad($ciudad, $customer);
    wp_die();
}

function actualizar_ciudad($city, $customer) {

    $customer->set_city($city);
    $customer->set_shipping_city($city);
    $customer->save_data();

    $current_user = wp_get_current_user();
    update_user_meta($current_user->ID, 'billing_city', $city);
    update_user_meta($current_user->ID, 'shipping_city', $city);
}

/*
 * Cuando el usuario entra por primera vez y por alguna razon no selecciona una ciudad 
 * 
 */

function mocion_ciudad_por_defecto() {

    if (!WC()->session)
        return "";

    //WC()->session->set_customer_session_cookie(true);
    
    $customer = WC()->customer;
    if (!WC()->customer) {
        $customer = new WC_Customer();
    }

    $s_city = WC()->customer->get_shipping_city();

    if (!$s_city) {
        $args = array(
            'post_type' => 'ciudades',
            'post_status' => 'publish',
            'posts_per_page' => 1
        );
        $ciudades = new WP_Query($args);
        $ciudades->the_post();

        if ($ciudades->post && $ciudades->post->post_title){
            $s_city =  $ciudades->post->post_title;
        }
        actualizar_ciudad($s_city, $customer);
    }

    return $s_city;
}
