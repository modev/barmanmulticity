<?php if ($ciudades->have_posts()) :?>
    <form id="seleccion-ciudades" class="seleccion-ciudades">
    <div class="field">
        <?php if ($atributos['show-title']):?>  <label>Seleccione su ciudad:</label><?php  endif;?>
        <div class="ui selection dropdown" data-ajaxurl="<?php echo admin_url('admin-ajax.php'); ?>">
            <i class="marker icon"></i> <span class="nombre-seleccion"><?php echo $ciudad ?></span> <i class="dropdown icon"></i>
            <div class="menu transition">
                <?php
                
                    while ($ciudades->have_posts()) : $ciudades->the_post();
                        $selected = ($ciudad == get_the_title()) ? 'true' : 'false';
                        $selected_item_class = ($ciudad == get_the_title()) ? 'item-selected-city' : '';
                        ?>
                        <div class="item <?php echo $selected_item_class; ?>" data-reload="<?php echo $atributos['reload']?>" data-value="<?php the_title(); ?>" data-selected="<?php echo $selected; ?>">
                            <?php the_title(); ?>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</form>
<?php endif; ?>

