<?php

if (!defined('ABSPATH')) {
	exit;
}

if (!class_exists('WC_Email_New_Order_By_Coupon')) :

// // include_once(WP_PLUGIN_DIR . '/woocommerce/includes/emails/class-wc-email.php' );
//include_once(WP_PLUGIN_DIR . '/woocommerce/includes/emails/class-wc-email-new-order.php');
// '';


	/**
	 * New Order Email.
	 *
	 * An email sent to the admin when a new order is received/paid for.
	 *
	 * @class       WC_Email_New_Order
	 * @version     2.0.0
	 * @package     WooCommerce/Classes/Emails
	 * @author      WooThemes
	 * @extends     WC_Email
	 */


	/**
	 * cliente
	 * distribuidor
	 * adminsitrador
	 * cupon
	 * promocion
	 */
	class WC_Email_New_Order_By_Coupon {


		/**
		 * Constructor.
		 */
		public function __construct() {
			$this->id = 'new_order';
			$this->title = __('New order', 'woocommerce');
			$this->description = __('New order emails are sent to chosen recipient(s) when a new order is received.', 'woocommerce');
			$this->heading = __('New customer order', 'woocommerce');
			$this->subject = __('[{site_title}] New customer order ({order_number}) - {order_date}', 'woocommerce');
			$this->template_html = 'emails/admin-new-order.php';
			$this->template_plain = 'emails/plain/admin-new-order.php';
		}

		public function get_order_details($order_id) {
			return wc_get_order($order_id);
		}

		/**
		 * Trigger.
		 *
		 * @param int $order_id
		 */
		public function trigger($order_id) {


			$this->object = wc_get_order($order_id);
			$this->subject = __('[' . get_bloginfo('name') . '] New customer order (' . $this->object->get_order_number() . ') - ' . date_i18n(wc_date_format(), strtotime($this->object->order_date)), 'woocommerce');

			/**
			 * @todo lógica para envíar correos
			 *
			 * 1. Cargar la entidad del metodo de envío
			 * 2. {Distribuidor} Conseguir la entidad relacionada
			 * 3. Conseguir el correo asociado a la entidad {distribuidor}
			 * 4. conseguir cupón con correo sobreescrito
			 * 5. if
			 *    6. Si hay correo sobreescrito enviar solo a ese
			 *    7. si no hay enviar al del método de envío
			 *
			 *
			 */

			/**
			 * @step 1
			 */
			$order = $this->get_order_details($order_id);
			$shipping_items = $order->get_items('shipping');

			/**
			 * @step 2
			 */
			$shippingMethodId = (int)reset($shipping_items)['method_id'];
			$distId = get('distribuidor', 1, 1, $shippingMethodId);

			/**
			 * @step 3
			 */
			$correo = get('correo', 1, 1, $distId);
			$default_to = $this->get_default_recipients();

			/**
			 * @step 4
			 */
			$coupons_used = $this->object->get_used_coupons();
			if (!empty($coupons_used)) {
				$cupon = $this->getRecipientsByCoupons($coupons_used);
			}

			/**
			 * Validar si tiene promociones y sobreescribir el distribuidor
			 */
			$correosPorPromocion = $this->getCorreosPorPromocion($coupons_used);

			/**
			 * @step 5
			 */
			if (isset($cupon)) {

				$to = implode(",", array($default_to, $cupon));
			} else {
				$to = implode(",", array($default_to, $correo));
			}

			if (isset($correosPorPromocion)) {
				$to = $correosPorPromocion;
			}


			error_log("correos" . $to);

			/**
			 * @test
			 * $this->asd();
			 * return;
			 */
			if ($to) {

				global $woocommerce;
				$mailer = $woocommerce->mailer();
				$mailer->send($to, $this->subject, $this->get_content_html());
			} else {
				error_log("Está cargando los otros");
				/**
				 * Old logic
				 */
				$recipientes_cupones = "";
				$default_to = $this->get_default_recipients();
				//Get recipients by shipping
				$shiping_recipients = $this->get_recipient_by_shipping();
				//Coupons used
				$coupons_used = $this->object->get_used_coupons();
				if (!empty($coupons_used)) {
					$recipientes_cupones = $this->getRecipientsByCoupons($coupons_used);
				}
				$to = implode(",", array($default_to, $shiping_recipients, $recipientes_cupones));
				error_log("correos" . $to);
				if (in_array("barmancupon", $coupons_used)) {
					$email_correo = array("gbahamon@teletrade.com.co, domicilios@teletrade.com.co");
					$to = implode(",", array($default_to, $email_correo));

				}
				if (!$to) {
					return;
				}

				global $woocommerce;
				$mailer = $woocommerce->mailer();
				$mailer->send($to, $this->subject, $this->get_content_html());
			}
		}

		public function getCorreosPorPromocion($cupones) {
			//promocion_cupon_field
			$correos = array();
			$id = '';
			$args = array(
				'posts_per_page' => -1,
				'post_type' => 'shop_coupon',
				'orderby' => 'title',
			);
			$query = new WP_Query($args);
			if ($query->have_posts()) {
				while ($query->have_posts()) {
					$query->the_post();
					if (in_array(get_the_title(), $cupones)) {
						$id = get_the_ID();
					}
				}
				wp_reset_postdata();
			}

			//id de la promoción
			$promoId = get('promocion_cupon_field', 1, 1, $id);
			//id del distribuidor
			$distId = get('correo', 1, 1, $promoId);
			//correo
			$correo = get('correo', 1, 1, $distId);

			return $correo;
		}

		public
		function get_default_recipients() {

			$default_recipients = "";
			$WCEmailController = new WC_Emails(FALSE, FALSE);
			$WCEmailHandlers = $WCEmailController->get_emails();
			if (isset($WCEmailHandlers['WC_Email_New_Order'])) {
				$default_recipients = $WCEmailHandlers['WC_Email_New_Order']->get_recipient();
			}

			return $default_recipients;
		}


		/**
		 * Get content html.
		 *
		 * @access public
		 * @return string
		 */
		public
		function get_content_html() {

			return wc_get_template_html($this->template_html, array(
				'order' => $this->object,
				'email_heading' => $this->heading,
				'sent_to_admin' => true,
				'plain_text' => false,
				'email' => $this
			));
		}


		/*
		El personal de ecolicores se encarga siempre del envio con cupones
					Adicionalmente buscamos los correos configurados en la promocion para tambien retornarlos
					*/

		/**
		 * @param $coupons_used
		 * @return
		 * @todo
		 * 1. Cargar los cupones
		 * 2. Conseguir el id de los cupones usados
		 * 3. Si está sobreescrito devolver el del último cupón aplicado
		 */
		private function getRecipientsByCoupons($coupons_used) {

			$correos = array();
			$id = '';
			$args = array(
				'posts_per_page' => -1,
				'post_type' => 'shop_coupon',
				'orderby' => 'title',
			);
			$query = new WP_Query($args);
			if ($query->have_posts()) {
				while ($query->have_posts()) {
					$query->the_post();
					if (in_array(get_the_title(), $coupons_used)) {
						$id = get_the_ID();
					}
				}
				wp_reset_postdata();
			}

			$distId = get('correo', 1, 1, $id);

			$correo = get('correo', 1, 1, $distId);

			return $correo;
		}

		private
		function get_recipient_by_shipping() {

			// 		pedido sin cupon

			/*
			* Contacto: Edwin Girlado,
							  Teléfono: 3174677444
							  Correo: pedidos@grupogiraldo.co
							 */
			$city = WC()->customer->get_city();

			switch ($city) {
				case "Cali":
					//pedidos@grupogiraldo.co";
					$email_correo = "mateo-1709@hotmail.com, eddgiraldogiraldo@gmail.com";
					break;
				case "Medellín":
					$email_correo = "contacto@licoresmedellin.com, domicilios@licoresmedellin.com";
					break;
				case "Bogotá":
					$email_correo = "gbahamon@teletrade.com.co, domicilios@teletrade.com.co";
					break;
				//Por si algo nos fallo
				default:
					$email_correo = "juan.lopez@mocionsoft.com, esteben.guio@mocionsoft.com, barman@mocionsoft.com";
					break;
			}
			return $email_correo;
		}

	}
endif;
return new WC_Email_New_Order_By_Coupon();