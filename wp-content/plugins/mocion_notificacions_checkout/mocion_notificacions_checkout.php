<?php
/**
 * Plugin Name: Notificacions checkout by Email Coupon
 * Description: Notifica al correo del cupon y promocion cuando se realiza una venta
 * Version: 1.0
 * Author: Mocion
 */
add_action("woocommerce_checkout_order_processed", "my_awesome_publication_notification");

include("class-wc-email-new-order-by-coupon.php");

function my_awesome_publication_notification($order_id, $checkout = null) {
    $email = new WC_Email_New_Order_By_Coupon();
    $email->trigger($order_id);
    
}

function example_callback( $example ) {
    // Maybe modify $example in some way.
    return $example;
}
add_filter( 'telefono_distribuidor', 'definir_telefono_distribuidor' );

function definir_telefono_distribuidor(){
	$city = WC()->customer->get_city(); 
    $telefono = "";
	switch($city){
		case "Cali":
			$telefono = "3174677444";      
		break;
		case "Medellín":
			$telefono = "(574) 4482522 / 3002739202";
		break;
		case "Bogotá":
			$telefono = "4926307";
		break;
		default: break;

	}	
	return $telefono;
}

add_filter( 'ciudad_distribuidor', 'definir_ciudad_distribuidor' );

function definir_ciudad_distribuidor($city){
	$city = WC()->customer->get_city();
	return $city;
}

