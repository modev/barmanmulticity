=== WooCommerce Local Pickup Plus ===
Author: skyverge
Tags: woocommerce
Requires at least: 3.5
Tested up to: 3.7.1
Requires WooCommerce at least: 1.6.6
Tested WooCommerce up to: 2.0.20

A shipping plugin for WooCommerce that allows the store operator to define local pickup locations, which the customer can then choose from when making a purchase.

See http://docs.woothemes.com/document/local-pickup-plus/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-shipping-local-pickup-plus' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress

