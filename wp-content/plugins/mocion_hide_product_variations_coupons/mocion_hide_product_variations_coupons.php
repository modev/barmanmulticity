<?php
/**
 * Plugin Name: mocion_hide_product_variations_coupons
 * Description: Quita las variaciones de la lista de productos al crear un cupón
 * Version: 1.0
 * Author: MocionG
 */

add_filter('woocommerce_json_search_found_products', 'products_remove_variations_coupon_admin');

function products_remove_variations_coupon_admin($found_products) {

	foreach ($found_products as $key => $p) {
		$type = get_post_type($key);
		if ($type == "product_variation") {
			unset($found_products[$key]);
		}
	}
	return $found_products;
}
