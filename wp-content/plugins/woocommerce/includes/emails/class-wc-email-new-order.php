<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Email_New_Order' ) ) :

/**
 * New Order Email.
 *
 * An email sent to the admin when a new order is received/paid for.
 *
 * @class       WC_Email_New_Order
 * @version     2.0.0
 * @package     WooCommerce/Classes/Emails
 * @author      WooThemes
 * @extends     WC_Email
 */
class WC_Email_New_Order extends WC_Email {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id               = 'new_order';
		$this->title            = __( 'New order', 'woocommerce' );
		$this->description      = __( 'New order emails are sent to chosen recipient(s) when a new order is received.', 'woocommerce' );
		$this->heading          = __( 'New customer order', 'woocommerce' );
		$this->subject          = __( '[{site_title}] New customer order ({order_number}) - {order_date}', 'woocommerce' );
		$this->template_html    = 'emails/admin-new-order.php';
		$this->template_plain   = 'emails/plain/admin-new-order.php';

		// Triggers for this email
		add_action( 'woocommerce_order_status_pending_to_processing_notification', array( $this, 'trigger' ) );
		add_action( 'woocommerce_order_status_pending_to_completed_notification', array( $this, 'trigger' ) );
		add_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $this, 'trigger' ) );
		add_action( 'woocommerce_order_status_failed_to_processing_notification', array( $this, 'trigger' ) );
		add_action( 'woocommerce_order_status_failed_to_completed_notification', array( $this, 'trigger' ) );
		add_action( 'woocommerce_order_status_failed_to_on-hold_notification', array( $this, 'trigger' ) );

		// Call parent constructor
		parent::__construct();

		// Other settings
		$this->recipient = $this->get_option( 'recipient', get_option( 'admin_email' ) );
	}

	/**
	 * Trigger.
	 *
	 * @param int $order_id
	 */
	public function trigger( $order_id ) {
		if ( $order_id ) {
			$this->object                  = wc_get_order( $order_id );
			$this->find['order-date']      = '{order_date}';
			$this->find['order-number']    = '{order_number}';
			$this->replace['order-date']   = date_i18n( wc_date_format(), strtotime( $this->object->order_date ) );
			$this->replace['order-number'] = $this->object->get_order_number();
		}

		if ( ! $this->is_enabled() || ! $this->get_recipient() ) {
			return;
		}
                
                $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
                
                var_dump($this->get_recipient());

                /** SOLUCION TEMPORAL VIEJA PARA EL ENVIO DE CORREOS A DIFERENTE PERSONA SI TIENEN CUPONES **/
                
                    /** MOCION * */
                      // se adiciona los correos de las promociones de los cupones que se utilizaron en la orden
                      // para tambien enviarles una notificacion de la venta

//                      $coupons_used = $this->object->get_used_coupons();
//
//                      //	var_dump($coupons_used); die;
//
//                      $inicial = $this->get_recipient();
//
//                      if (empty($coupons_used)) {
//                          /*
//                           * Contacto: Edwin Girlado, 
//Teléfono: 3174677444
//Correo: pedidos@grupogiraldo.co
//                           */
//                            $city = WC()->customer->get_city();
//                             if ($city == "Cali"){
//                                 $email_correo = "pedidos@grupogiraldo.co";
//                                 
//                             }else{
//                                 $email_correo = "gbahamon@teletrade.com.co, domicilios@teletrade.com.co";
//                             }
//                             $to = $inicial . " , " . $email_correo;
//
//
//                      } else {
//
//                          $coupons = array();
//                          foreach ($coupons_used as $key => $value) {
//                              $args = array(
//                                  's' => $value,
//                                  'post_type' => 'shop_coupon',
//                                  'post_status' => 'publish',
//                              );
//                              $coupons[$value] = get_posts($args);
//                          }
//
//                          $promocions = array();
//                          foreach ($coupons as $coupon) {
//                              $promocions[$coupon[0]->post_title] = array(
//                                  'promocion_cupon_field' => get_post_meta($coupon[0]->ID, 'promocion_cupon_field', true),
//                              );
//                          }
//
//                          $correos = array();
//                          foreach ($promocions as $promocion) {
//                              if ($promocion["promocion_cupon_field"] != null) {
//                                  $correos[] = get_post_meta($promocion["promocion_cupon_field"], 'correo', true);
//                              }
//                          }
//                          if (empty($correos)){
//                          $separado = "algo@ecolicores";
//                          }else{
//                              $separado = implode(",", $correos);
//                          }
//                          $to = $inicial . " , " . $separado; 
//                          
//                      }
//
//                      if (!$this->is_enabled() || !$this->get_recipient()) {
//                          return;
//                      }
//
//                      // si el usuario de la orden es el administrador o el usuario jhina.rivera@mocionsoft.com
//                      // esto es para pruebas
//                      if($this->object->user_id == 1 || $this->object->user_id == 916) {
//                          $to = "juan.lopez@mocionsoft.com";
//                      }            
                
                /** FIN SOLUCION TEMPORAL **/                
//		$this->send( $to, $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
	}

	/**
	 * Get content html.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html( $this->template_html, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => true,
			'plain_text'    => false,
			'email'			=> $this
		) );
	}

	/**
	 * Get content plain.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html( $this->template_plain, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => true,
			'plain_text'    => true,
			'email'			=> $this
		) );
	}

	/**
	 * Initialise settings form fields.
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'         => __( 'Enable/Disable', 'woocommerce' ),
				'type'          => 'checkbox',
				'label'         => __( 'Enable this email notification', 'woocommerce' ),
				'default'       => 'yes'
			),
			'recipient' => array(
				'title'         => __( 'Recipient(s)', 'woocommerce' ),
				'type'          => 'text',
				'description'   => sprintf( __( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', 'woocommerce' ), esc_attr( get_option('admin_email') ) ),
				'placeholder'   => '',
				'default'       => '',
				'desc_tip'      => true
			),
			'subject' => array(
				'title'         => __( 'Subject', 'woocommerce' ),
				'type'          => 'text',
				'description'   => sprintf( __( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', 'woocommerce' ), $this->subject ),
				'placeholder'   => '',
				'default'       => '',
				'desc_tip'      => true
			),
			'heading' => array(
				'title'         => __( 'Email Heading', 'woocommerce' ),
				'type'          => 'text',
				'description'   => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.', 'woocommerce' ), $this->heading ),
				'placeholder'   => '',
				'default'       => '',
				'desc_tip'      => true
			),
			'email_type' => array(
				'title'         => __( 'Email type', 'woocommerce' ),
				'type'          => 'select',
				'description'   => __( 'Choose which format of email to send.', 'woocommerce' ),
				'default'       => 'html',
				'class'         => 'email_type wc-enhanced-select',
				'options'       => $this->get_email_type_options(),
				'desc_tip'      => true
			)
		);
	}
}

endif;

return new WC_Email_New_Order();
