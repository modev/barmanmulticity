<?php
/**
 * Plugin Name: Filtro de promociones
 * Plugin URI: http://filter_promocion_page.com/
 * Description: Filtro de promociones para la agina de administracion de cupones
 * Version: 1.0 or whatever version of the plugin (pretty self explanatory)
 * Author: Plugin Author's Name
 * Author URI: Author's website
 * License: A "Slug" license name e.g. GPL12
 */
add_filter('parse_query', 'promocion_posts_filter');
add_action('restrict_manage_posts', 'promocion_posts_filter_restrict_manage_posts');

function promocion_posts_filter($query) {
    global $pagenow;
    if (is_admin() && ($pagenow == 'edit.php' || $pagenow == 'tools.php') && isset($_GET['FILTER_PROMOCION']) && $_GET['FILTER_PROMOCION'] != '') {
        $query->query_vars['meta_key'] = "promocion_cupon_field";
        $query->query_vars['meta_value'] = $_GET['FILTER_PROMOCION'];
    }
}

function promocion_posts_filter_restrict_manage_posts() {
    global $post_type;
    if ($post_type == "shop_coupon") {
        global $wpdb;
        $sql = 'SELECT DISTINCT ID, post_title FROM ' . $wpdb->posts . ' where post_type = "taxonomy_grupo" AND post_status = "publish"';
        $promociones = $wpdb->get_results($sql, ARRAY_N);
        ?>
        <select name="FILTER_PROMOCION">
            <option value=""><?php _e('Filtrar por promoción', 'promocionapf'); ?></option>
        <?php
        $current = isset($_GET['FILTER_PROMOCION']) ? $_GET['FILTER_PROMOCION'] : '';

        foreach ($promociones as $promocion) {
            if (substr($promocion[0], 0, 1) != "_") {
                printf
                        (
                        '<option value="%s"%s>%s</option>', $promocion[0], $promocion[0] == $current ? ' selected="selected"' : '', $promocion[1]
                );
            }

            // var_dump($promocion);
        }
        ?>
        </select>

            <?php
        }
    }
    